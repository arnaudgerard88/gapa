import { IDomaine } from '@/shared/model/domaine.model';
import { IChapitre } from '@/shared/model/chapitre.model';
import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';

export interface ICahier {
  id?: number;
  nom?: string;
  dateMaj?: Date;
  domaine?: IDomaine;
  chapitres?: IChapitre[];
  responsabiliteCahiers?: IResponsabiliteCahier[];
}

export class Cahier implements ICahier {
  constructor(
    public id?: number,
    public nom?: string,
    public dateMaj?: Date,
    public domaine?: IDomaine,
    public chapitres?: IChapitre[],
    public responsabiliteCahiers?: IResponsabiliteCahier[]
  ) {}
}
