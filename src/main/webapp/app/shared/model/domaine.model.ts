import { IAnomalie } from '@/shared/model/anomalie.model';
import { ICahier } from '@/shared/model/cahier.model';
import { IApplication } from '@/shared/model/application.model';

export interface IDomaine {
  id?: number;
  nom?: string;
  anomalies?: IAnomalie[];
  cahier?: ICahier;
  application?: IApplication;
}

export class Domaine implements IDomaine {
  constructor(
    public id?: number,
    public nom?: string,
    public anomalies?: IAnomalie[],
    public cahier?: ICahier,
    public application?: IApplication
  ) {}
}
