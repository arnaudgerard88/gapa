import { IDomaine } from '@/shared/model/domaine.model';
import { IVersion } from '@/shared/model/version.model';
import { IUtilisateur } from '@/shared/model/utilisateur.model';
import { IEnvironnement } from '@/shared/model/environnement.model';

export interface IApplication {
  id?: number;
  nom?: string;
  email?: string;
  dateAjout?: Date;
  domaines?: IDomaine[];
  versions?: IVersion[];
  utilisateurs?: IUtilisateur[];
  environnements?: IEnvironnement[];
}

export class Application implements IApplication {
  constructor(
    public id?: number,
    public nom?: string,
    public email?: string,
    public dateAjout?: Date,
    public domaines?: IDomaine[],
    public versions?: IVersion[],
    public utilisateurs?: IUtilisateur[],
    public environnements?: IEnvironnement[]
  ) {}
}
