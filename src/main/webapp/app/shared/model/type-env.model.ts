import { IEnvironnement } from '@/shared/model/environnement.model';

export interface ITypeEnv {
  id?: number;
  nom?: string;
  environnements?: IEnvironnement[];
}

export class TypeEnv implements ITypeEnv {
  constructor(public id?: number, public nom?: string, public environnements?: IEnvironnement[]) {}
}
