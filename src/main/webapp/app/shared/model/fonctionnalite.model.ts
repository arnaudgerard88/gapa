import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';
import { IChapitre } from '@/shared/model/chapitre.model';

export interface IFonctionnalite {
  id?: number;
  nom?: string;
  description?: string;
  estActive?: number;
  suiviCampagnes?: ISuiviCampagne[];
  chapitre?: IChapitre;
}

export class Fonctionnalite implements IFonctionnalite {
  constructor(
    public id?: number,
    public nom?: string,
    public description?: string,
    public estActive?: number,
    public suiviCampagnes?: ISuiviCampagne[],
    public chapitre?: IChapitre
  ) {}
}
