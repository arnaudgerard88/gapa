import { IUtilisateur } from '@/shared/model/utilisateur.model';
import { ICahier } from '@/shared/model/cahier.model';

export interface IResponsabiliteCahier {
  id?: number;
  date?: Date;
  utilisateur?: IUtilisateur;
  cahier?: ICahier;
}

export class ResponsabiliteCahier implements IResponsabiliteCahier {
  constructor(public id?: number, public date?: Date, public utilisateur?: IUtilisateur, public cahier?: ICahier) {}
}
