import { IAnomalie } from '@/shared/model/anomalie.model';
import { IDeploiement } from '@/shared/model/deploiement.model';
import { ICorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';
import { IApplication } from '@/shared/model/application.model';

export interface IVersion {
  id?: number;
  nom?: string;
  dateMiseProd?: Date;
  anomalies?: IAnomalie[];
  deploiements?: IDeploiement[];
  correctionPlannifiees?: ICorrectionPlannifiee[];
  application?: IApplication;
}

export class Version implements IVersion {
  constructor(
    public id?: number,
    public nom?: string,
    public dateMiseProd?: Date,
    public anomalies?: IAnomalie[],
    public deploiements?: IDeploiement[],
    public correctionPlannifiees?: ICorrectionPlannifiee[],
    public application?: IApplication
  ) {}
}
