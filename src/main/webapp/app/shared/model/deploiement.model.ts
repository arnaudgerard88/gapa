import { IVersion } from '@/shared/model/version.model';
import { IEnvironnement } from '@/shared/model/environnement.model';

export interface IDeploiement {
  id?: number;
  estActif?: number;
  date?: Date;
  version?: IVersion;
  environnement?: IEnvironnement;
}

export class Deploiement implements IDeploiement {
  constructor(
    public id?: number,
    public estActif?: number,
    public date?: Date,
    public version?: IVersion,
    public environnement?: IEnvironnement
  ) {}
}
