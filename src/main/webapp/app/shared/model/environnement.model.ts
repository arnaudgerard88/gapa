import { ICampagne } from '@/shared/model/campagne.model';
import { IDeploiement } from '@/shared/model/deploiement.model';
import { IApplication } from '@/shared/model/application.model';
import { ITypeEnv } from '@/shared/model/type-env.model';

export interface IEnvironnement {
  id?: number;
  dateAjout?: Date;
  estActif?: boolean;
  campagnes?: ICampagne[];
  deploiements?: IDeploiement[];
  application?: IApplication;
  typeEnv?: ITypeEnv;
}

export class Environnement implements IEnvironnement {
  constructor(
    public id?: number,
    public dateAjout?: Date,
    public estActif?: boolean,
    public campagnes?: ICampagne[],
    public deploiements?: IDeploiement[],
    public application?: IApplication,
    public typeEnv?: ITypeEnv
  ) {
    this.estActif = this.estActif || false;
  }
}
