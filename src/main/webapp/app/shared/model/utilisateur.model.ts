import { IAnomalie } from '@/shared/model/anomalie.model';
import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';
import { IApplication } from '@/shared/model/application.model';

export interface IUtilisateur {
  id?: number;
  nom?: string;
  prenom?: string;
  email?: string;
  anomalies?: IAnomalie[];
  responsabiliteCahiers?: IResponsabiliteCahier[];
  application?: IApplication;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public email?: string,
    public anomalies?: IAnomalie[],
    public responsabiliteCahiers?: IResponsabiliteCahier[],
    public application?: IApplication
  ) {}
}
