import { IAnomalie } from '@/shared/model/anomalie.model';
import { IEtablissement } from '@/shared/model/etablissement.model';

export interface IAnomalieEtablissement {
  id?: number;
  date?: Date;
  anomalie?: IAnomalie;
  etablissement?: IEtablissement;
}

export class AnomalieEtablissement implements IAnomalieEtablissement {
  constructor(public id?: number, public date?: Date, public anomalie?: IAnomalie, public etablissement?: IEtablissement) {}
}
