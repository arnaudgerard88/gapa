import { IFonctionnalite } from '@/shared/model/fonctionnalite.model';
import { ICahier } from '@/shared/model/cahier.model';

export interface IChapitre {
  id?: number;
  nom?: string;
  dateMaj?: Date;
  fonctionnalites?: IFonctionnalite[];
  cahier?: ICahier;
}

export class Chapitre implements IChapitre {
  constructor(
    public id?: number,
    public nom?: string,
    public dateMaj?: Date,
    public fonctionnalites?: IFonctionnalite[],
    public cahier?: ICahier
  ) {}
}
