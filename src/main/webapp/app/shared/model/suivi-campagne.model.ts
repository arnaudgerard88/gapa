import { IAnomalie } from '@/shared/model/anomalie.model';
import { IFonctionnalite } from '@/shared/model/fonctionnalite.model';
import { ICampagne } from '@/shared/model/campagne.model';

export interface ISuiviCampagne {
  id?: number;
  etat?: string;
  date?: Date;
  anomalies?: IAnomalie[];
  fonctionnalite?: IFonctionnalite;
  campagne?: ICampagne;
}

export class SuiviCampagne implements ISuiviCampagne {
  constructor(
    public id?: number,
    public etat?: string,
    public date?: Date,
    public anomalies?: IAnomalie[],
    public fonctionnalite?: IFonctionnalite,
    public campagne?: ICampagne
  ) {}
}
