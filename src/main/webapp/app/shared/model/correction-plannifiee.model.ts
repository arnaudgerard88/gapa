import { IVersion } from '@/shared/model/version.model';
import { IAnomalie } from '@/shared/model/anomalie.model';

export interface ICorrectionPlannifiee {
  id?: number;
  date?: Date;
  version?: IVersion;
  anomalie?: IAnomalie;
}

export class CorrectionPlannifiee implements ICorrectionPlannifiee {
  constructor(public id?: number, public date?: Date, public version?: IVersion, public anomalie?: IAnomalie) {}
}
