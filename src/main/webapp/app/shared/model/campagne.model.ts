import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';
import { IEnvironnement } from '@/shared/model/environnement.model';

export interface ICampagne {
  id?: number;
  nom?: string;
  dateDebut?: Date;
  dateFin?: Date;
  suiviCampagnes?: ISuiviCampagne[];
  environnement?: IEnvironnement;
}

export class Campagne implements ICampagne {
  constructor(
    public id?: number,
    public nom?: string,
    public dateDebut?: Date,
    public dateFin?: Date,
    public suiviCampagnes?: ISuiviCampagne[],
    public environnement?: IEnvironnement
  ) {}
}
