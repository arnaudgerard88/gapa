import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';

export interface IEtablissement {
  id?: number;
  nomEtab?: string;
  anomalieEtablissements?: IAnomalieEtablissement[];
}

export class Etablissement implements IEtablissement {
  constructor(public id?: number, public nomEtab?: string, public anomalieEtablissements?: IAnomalieEtablissement[]) {}
}
