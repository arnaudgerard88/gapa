import { IAnomalie } from '@/shared/model/anomalie.model';
import { ICorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';
import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';
import { IUtilisateur } from '@/shared/model/utilisateur.model';
import { IVersion } from '@/shared/model/version.model';
import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';
import { IDomaine } from '@/shared/model/domaine.model';

export interface IAnomalie {
  id?: number;
  numInterne?: string;
  numExterne?: string;
  titre?: string;
  description?: string;
  statut?: string;
  criticite?: string;
  dateDeclaration?: Date;
  dateCloture?: Date;
  anomalies?: IAnomalie[];
  correctionPlannifiees?: ICorrectionPlannifiee[];
  anomalieEtablissements?: IAnomalieEtablissement[];
  utilisateur?: IUtilisateur;
  version?: IVersion;
  anomalie?: IAnomalie;
  suiviCampagne?: ISuiviCampagne;
  domaine?: IDomaine;
}

export class Anomalie implements IAnomalie {
  constructor(
    public id?: number,
    public numInterne?: string,
    public numExterne?: string,
    public titre?: string,
    public description?: string,
    public statut?: string,
    public criticite?: string,
    public dateDeclaration?: Date,
    public dateCloture?: Date,
    public anomalies?: IAnomalie[],
    public correctionPlannifiees?: ICorrectionPlannifiee[],
    public anomalieEtablissements?: IAnomalieEtablissement[],
    public utilisateur?: IUtilisateur,
    public version?: IVersion,
    public anomalie?: IAnomalie,
    public suiviCampagne?: ISuiviCampagne,
    public domaine?: IDomaine
  ) {}
}
