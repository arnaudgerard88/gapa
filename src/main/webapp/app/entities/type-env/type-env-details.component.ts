import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITypeEnv } from '@/shared/model/type-env.model';
import TypeEnvService from './type-env.service';

@Component
export default class TypeEnvDetails extends Vue {
  @Inject('typeEnvService') private typeEnvService: () => TypeEnvService;
  public typeEnv: ITypeEnv = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.typeEnvId) {
        vm.retrieveTypeEnv(to.params.typeEnvId);
      }
    });
  }

  public retrieveTypeEnv(typeEnvId) {
    this.typeEnvService()
      .find(typeEnvId)
      .then(res => {
        this.typeEnv = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
