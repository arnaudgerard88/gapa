import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import EnvironnementService from '../environnement/environnement.service';
import { IEnvironnement } from '@/shared/model/environnement.model';

import AlertService from '@/shared/alert/alert.service';
import { ITypeEnv, TypeEnv } from '@/shared/model/type-env.model';
import TypeEnvService from './type-env.service';

const validations: any = {
  typeEnv: {
    nom: {}
  }
};

@Component({
  validations
})
export default class TypeEnvUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('typeEnvService') private typeEnvService: () => TypeEnvService;
  public typeEnv: ITypeEnv = new TypeEnv();

  @Inject('environnementService') private environnementService: () => EnvironnementService;

  public environnements: IEnvironnement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.typeEnvId) {
        vm.retrieveTypeEnv(to.params.typeEnvId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.typeEnv.id) {
      this.typeEnvService()
        .update(this.typeEnv)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A TypeEnv is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.typeEnvService()
        .create(this.typeEnv)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A TypeEnv is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveTypeEnv(typeEnvId): void {
    this.typeEnvService()
      .find(typeEnvId)
      .then(res => {
        this.typeEnv = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.environnementService()
      .retrieve()
      .then(res => {
        this.environnements = res.data;
      });
  }
}
