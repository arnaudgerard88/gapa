import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import FonctionnaliteService from '../fonctionnalite/fonctionnalite.service';
import { IFonctionnalite } from '@/shared/model/fonctionnalite.model';

import CahierService from '../cahier/cahier.service';
import { ICahier } from '@/shared/model/cahier.model';

import AlertService from '@/shared/alert/alert.service';
import { IChapitre, Chapitre } from '@/shared/model/chapitre.model';
import ChapitreService from './chapitre.service';

const validations: any = {
  chapitre: {
    nom: {},
    dateMaj: {}
  }
};

@Component({
  validations
})
export default class ChapitreUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('chapitreService') private chapitreService: () => ChapitreService;
  public chapitre: IChapitre = new Chapitre();

  @Inject('fonctionnaliteService') private fonctionnaliteService: () => FonctionnaliteService;

  public fonctionnalites: IFonctionnalite[] = [];

  @Inject('cahierService') private cahierService: () => CahierService;

  public cahiers: ICahier[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.chapitreId) {
        vm.retrieveChapitre(to.params.chapitreId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.chapitre.id) {
      this.chapitreService()
        .update(this.chapitre)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Chapitre is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.chapitreService()
        .create(this.chapitre)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Chapitre is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.chapitre[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.chapitre[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.chapitre[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.chapitre[field] = null;
    }
  }

  public retrieveChapitre(chapitreId): void {
    this.chapitreService()
      .find(chapitreId)
      .then(res => {
        res.dateMaj = new Date(res.dateMaj);
        this.chapitre = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.fonctionnaliteService()
      .retrieve()
      .then(res => {
        this.fonctionnalites = res.data;
      });
    this.cahierService()
      .retrieve()
      .then(res => {
        this.cahiers = res.data;
      });
  }
}
