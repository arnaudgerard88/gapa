import { Component, Vue, Inject } from 'vue-property-decorator';

import { IChapitre } from '@/shared/model/chapitre.model';
import ChapitreService from './chapitre.service';

@Component
export default class ChapitreDetails extends Vue {
  @Inject('chapitreService') private chapitreService: () => ChapitreService;
  public chapitre: IChapitre = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.chapitreId) {
        vm.retrieveChapitre(to.params.chapitreId);
      }
    });
  }

  public retrieveChapitre(chapitreId) {
    this.chapitreService()
      .find(chapitreId)
      .then(res => {
        this.chapitre = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
