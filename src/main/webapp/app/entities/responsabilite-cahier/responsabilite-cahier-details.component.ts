import { Component, Vue, Inject } from 'vue-property-decorator';

import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';
import ResponsabiliteCahierService from './responsabilite-cahier.service';

@Component
export default class ResponsabiliteCahierDetails extends Vue {
  @Inject('responsabiliteCahierService') private responsabiliteCahierService: () => ResponsabiliteCahierService;
  public responsabiliteCahier: IResponsabiliteCahier = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.responsabiliteCahierId) {
        vm.retrieveResponsabiliteCahier(to.params.responsabiliteCahierId);
      }
    });
  }

  public retrieveResponsabiliteCahier(responsabiliteCahierId) {
    this.responsabiliteCahierService()
      .find(responsabiliteCahierId)
      .then(res => {
        this.responsabiliteCahier = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
