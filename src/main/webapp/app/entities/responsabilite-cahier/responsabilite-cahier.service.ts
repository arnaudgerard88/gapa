import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';

const baseApiUrl = 'api/responsabilite-cahiers';

export default class ResponsabiliteCahierService {
  public find(id: number): Promise<IResponsabiliteCahier> {
    return new Promise<IResponsabiliteCahier>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(function(res) {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(function(res) {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IResponsabiliteCahier): Promise<IResponsabiliteCahier> {
    return new Promise<IResponsabiliteCahier>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IResponsabiliteCahier): Promise<IResponsabiliteCahier> {
    return new Promise<IResponsabiliteCahier>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
