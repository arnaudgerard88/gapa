import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import UtilisateurService from '../utilisateur/utilisateur.service';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

import CahierService from '../cahier/cahier.service';
import { ICahier } from '@/shared/model/cahier.model';

import AlertService from '@/shared/alert/alert.service';
import { IResponsabiliteCahier, ResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';
import ResponsabiliteCahierService from './responsabilite-cahier.service';

const validations: any = {
  responsabiliteCahier: {
    date: {}
  }
};

@Component({
  validations
})
export default class ResponsabiliteCahierUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('responsabiliteCahierService') private responsabiliteCahierService: () => ResponsabiliteCahierService;
  public responsabiliteCahier: IResponsabiliteCahier = new ResponsabiliteCahier();

  @Inject('utilisateurService') private utilisateurService: () => UtilisateurService;

  public utilisateurs: IUtilisateur[] = [];

  @Inject('cahierService') private cahierService: () => CahierService;

  public cahiers: ICahier[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.responsabiliteCahierId) {
        vm.retrieveResponsabiliteCahier(to.params.responsabiliteCahierId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.responsabiliteCahier.id) {
      this.responsabiliteCahierService()
        .update(this.responsabiliteCahier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A ResponsabiliteCahier is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.responsabiliteCahierService()
        .create(this.responsabiliteCahier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A ResponsabiliteCahier is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.responsabiliteCahier[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.responsabiliteCahier[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.responsabiliteCahier[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.responsabiliteCahier[field] = null;
    }
  }

  public retrieveResponsabiliteCahier(responsabiliteCahierId): void {
    this.responsabiliteCahierService()
      .find(responsabiliteCahierId)
      .then(res => {
        res.date = new Date(res.date);
        this.responsabiliteCahier = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.utilisateurService()
      .retrieve()
      .then(res => {
        this.utilisateurs = res.data;
      });
    this.cahierService()
      .retrieve()
      .then(res => {
        this.cahiers = res.data;
      });
  }
}
