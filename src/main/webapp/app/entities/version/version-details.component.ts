import { Component, Vue, Inject } from 'vue-property-decorator';

import { IVersion } from '@/shared/model/version.model';
import VersionService from './version.service';

@Component
export default class VersionDetails extends Vue {
  @Inject('versionService') private versionService: () => VersionService;
  public version: IVersion = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.versionId) {
        vm.retrieveVersion(to.params.versionId);
      }
    });
  }

  public retrieveVersion(versionId) {
    this.versionService()
      .find(versionId)
      .then(res => {
        this.version = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
