import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import DeploiementService from '../deploiement/deploiement.service';
import { IDeploiement } from '@/shared/model/deploiement.model';

import CorrectionPlannifieeService from '../correction-plannifiee/correction-plannifiee.service';
import { ICorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';

import ApplicationService from '../application/application.service';
import { IApplication } from '@/shared/model/application.model';

import AlertService from '@/shared/alert/alert.service';
import { IVersion, Version } from '@/shared/model/version.model';
import VersionService from './version.service';

const validations: any = {
  version: {
    nom: {},
    dateMiseProd: {}
  }
};

@Component({
  validations
})
export default class VersionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('versionService') private versionService: () => VersionService;
  public version: IVersion = new Version();

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];

  @Inject('deploiementService') private deploiementService: () => DeploiementService;

  public deploiements: IDeploiement[] = [];

  @Inject('correctionPlannifieeService') private correctionPlannifieeService: () => CorrectionPlannifieeService;

  public correctionPlannifiees: ICorrectionPlannifiee[] = [];

  @Inject('applicationService') private applicationService: () => ApplicationService;

  public applications: IApplication[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.versionId) {
        vm.retrieveVersion(to.params.versionId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.version.id) {
      this.versionService()
        .update(this.version)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Version is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.versionService()
        .create(this.version)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Version is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.version[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.version[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.version[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.version[field] = null;
    }
  }

  public retrieveVersion(versionId): void {
    this.versionService()
      .find(versionId)
      .then(res => {
        res.dateMiseProd = new Date(res.dateMiseProd);
        this.version = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.deploiementService()
      .retrieve()
      .then(res => {
        this.deploiements = res.data;
      });
    this.correctionPlannifieeService()
      .retrieve()
      .then(res => {
        this.correctionPlannifiees = res.data;
      });
    this.applicationService()
      .retrieve()
      .then(res => {
        this.applications = res.data;
      });
  }
}
