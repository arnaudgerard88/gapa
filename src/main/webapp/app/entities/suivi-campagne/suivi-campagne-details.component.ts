import { Component, Vue, Inject } from 'vue-property-decorator';

import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';
import SuiviCampagneService from './suivi-campagne.service';

@Component
export default class SuiviCampagneDetails extends Vue {
  @Inject('suiviCampagneService') private suiviCampagneService: () => SuiviCampagneService;
  public suiviCampagne: ISuiviCampagne = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.suiviCampagneId) {
        vm.retrieveSuiviCampagne(to.params.suiviCampagneId);
      }
    });
  }

  public retrieveSuiviCampagne(suiviCampagneId) {
    this.suiviCampagneService()
      .find(suiviCampagneId)
      .then(res => {
        this.suiviCampagne = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
