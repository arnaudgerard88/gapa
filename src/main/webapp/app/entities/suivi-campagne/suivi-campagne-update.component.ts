import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import FonctionnaliteService from '../fonctionnalite/fonctionnalite.service';
import { IFonctionnalite } from '@/shared/model/fonctionnalite.model';

import CampagneService from '../campagne/campagne.service';
import { ICampagne } from '@/shared/model/campagne.model';

import AlertService from '@/shared/alert/alert.service';
import { ISuiviCampagne, SuiviCampagne } from '@/shared/model/suivi-campagne.model';
import SuiviCampagneService from './suivi-campagne.service';

const validations: any = {
  suiviCampagne: {
    etat: {},
    date: {}
  }
};

@Component({
  validations
})
export default class SuiviCampagneUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('suiviCampagneService') private suiviCampagneService: () => SuiviCampagneService;
  public suiviCampagne: ISuiviCampagne = new SuiviCampagne();

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];

  @Inject('fonctionnaliteService') private fonctionnaliteService: () => FonctionnaliteService;

  public fonctionnalites: IFonctionnalite[] = [];

  @Inject('campagneService') private campagneService: () => CampagneService;

  public campagnes: ICampagne[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.suiviCampagneId) {
        vm.retrieveSuiviCampagne(to.params.suiviCampagneId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.suiviCampagne.id) {
      this.suiviCampagneService()
        .update(this.suiviCampagne)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A SuiviCampagne is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.suiviCampagneService()
        .create(this.suiviCampagne)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A SuiviCampagne is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.suiviCampagne[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.suiviCampagne[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.suiviCampagne[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.suiviCampagne[field] = null;
    }
  }

  public retrieveSuiviCampagne(suiviCampagneId): void {
    this.suiviCampagneService()
      .find(suiviCampagneId)
      .then(res => {
        res.date = new Date(res.date);
        this.suiviCampagne = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.fonctionnaliteService()
      .retrieve()
      .then(res => {
        this.fonctionnalites = res.data;
      });
    this.campagneService()
      .retrieve()
      .then(res => {
        this.campagnes = res.data;
      });
  }
}
