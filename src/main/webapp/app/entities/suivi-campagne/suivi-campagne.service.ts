import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';

const baseApiUrl = 'api/suivi-campagnes';

export default class SuiviCampagneService {
  public find(id: number): Promise<ISuiviCampagne> {
    return new Promise<ISuiviCampagne>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(function(res) {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(function(res) {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: ISuiviCampagne): Promise<ISuiviCampagne> {
    return new Promise<ISuiviCampagne>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: ISuiviCampagne): Promise<ISuiviCampagne> {
    return new Promise<ISuiviCampagne>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(function(res) {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
