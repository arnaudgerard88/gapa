import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';
import CorrectionPlannifieeService from './correction-plannifiee.service';

@Component
export default class CorrectionPlannifieeDetails extends Vue {
  @Inject('correctionPlannifieeService') private correctionPlannifieeService: () => CorrectionPlannifieeService;
  public correctionPlannifiee: ICorrectionPlannifiee = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.correctionPlannifieeId) {
        vm.retrieveCorrectionPlannifiee(to.params.correctionPlannifieeId);
      }
    });
  }

  public retrieveCorrectionPlannifiee(correctionPlannifieeId) {
    this.correctionPlannifieeService()
      .find(correctionPlannifieeId)
      .then(res => {
        this.correctionPlannifiee = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
