import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import VersionService from '../version/version.service';
import { IVersion } from '@/shared/model/version.model';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import AlertService from '@/shared/alert/alert.service';
import { ICorrectionPlannifiee, CorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';
import CorrectionPlannifieeService from './correction-plannifiee.service';

const validations: any = {
  correctionPlannifiee: {
    date: {}
  }
};

@Component({
  validations
})
export default class CorrectionPlannifieeUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('correctionPlannifieeService') private correctionPlannifieeService: () => CorrectionPlannifieeService;
  public correctionPlannifiee: ICorrectionPlannifiee = new CorrectionPlannifiee();

  @Inject('versionService') private versionService: () => VersionService;

  public versions: IVersion[] = [];

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.correctionPlannifieeId) {
        vm.retrieveCorrectionPlannifiee(to.params.correctionPlannifieeId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.correctionPlannifiee.id) {
      this.correctionPlannifieeService()
        .update(this.correctionPlannifiee)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A CorrectionPlannifiee is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.correctionPlannifieeService()
        .create(this.correctionPlannifiee)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A CorrectionPlannifiee is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.correctionPlannifiee[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.correctionPlannifiee[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.correctionPlannifiee[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.correctionPlannifiee[field] = null;
    }
  }

  public retrieveCorrectionPlannifiee(correctionPlannifieeId): void {
    this.correctionPlannifieeService()
      .find(correctionPlannifieeId)
      .then(res => {
        res.date = new Date(res.date);
        this.correctionPlannifiee = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.versionService()
      .retrieve()
      .then(res => {
        this.versions = res.data;
      });
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
  }
}
