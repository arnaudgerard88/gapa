import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import SuiviCampagneService from '../suivi-campagne/suivi-campagne.service';
import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';

import EnvironnementService from '../environnement/environnement.service';
import { IEnvironnement } from '@/shared/model/environnement.model';

import AlertService from '@/shared/alert/alert.service';
import { ICampagne, Campagne } from '@/shared/model/campagne.model';
import CampagneService from './campagne.service';

const validations: any = {
  campagne: {
    nom: {},
    dateDebut: {},
    dateFin: {}
  }
};

@Component({
  validations
})
export default class CampagneUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('campagneService') private campagneService: () => CampagneService;
  public campagne: ICampagne = new Campagne();

  @Inject('suiviCampagneService') private suiviCampagneService: () => SuiviCampagneService;

  public suiviCampagnes: ISuiviCampagne[] = [];

  @Inject('environnementService') private environnementService: () => EnvironnementService;

  public environnements: IEnvironnement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.campagneId) {
        vm.retrieveCampagne(to.params.campagneId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.campagne.id) {
      this.campagneService()
        .update(this.campagne)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Campagne is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.campagneService()
        .create(this.campagne)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Campagne is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.campagne[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.campagne[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.campagne[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.campagne[field] = null;
    }
  }

  public retrieveCampagne(campagneId): void {
    this.campagneService()
      .find(campagneId)
      .then(res => {
        res.dateDebut = new Date(res.dateDebut);
        res.dateFin = new Date(res.dateFin);
        this.campagne = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.suiviCampagneService()
      .retrieve()
      .then(res => {
        this.suiviCampagnes = res.data;
      });
    this.environnementService()
      .retrieve()
      .then(res => {
        this.environnements = res.data;
      });
  }
}
