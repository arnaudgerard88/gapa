import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICampagne } from '@/shared/model/campagne.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import CampagneService from './campagne.service';

@Component
export default class Campagne extends mixins(Vue2Filters.mixin, AlertMixin) {
  @Inject('campagneService') private campagneService: () => CampagneService;
  private removeId: number = null;
  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;

  public campagnes: ICampagne[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCampagnes();
  }

  public clear(): void {
    this.page = 1;
    this.retrieveAllCampagnes();
  }

  public retrieveAllCampagnes(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    this.campagneService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.campagnes = res.data;
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: ICampagne): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCampagne(): void {
    this.campagneService()
      .delete(this.removeId)
      .then(() => {
        const message = 'A Campagne is deleted with identifier ' + this.removeId;
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllCampagnes();
        this.closeDialog();
      });
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  public loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  public transition(): void {
    this.retrieveAllCampagnes();
  }

  public changeOrder(propOrder): void {
    this.propOrder = propOrder;
    this.reverse = !this.reverse;
    this.transition();
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
