import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICampagne } from '@/shared/model/campagne.model';
import CampagneService from './campagne.service';

@Component
export default class CampagneDetails extends Vue {
  @Inject('campagneService') private campagneService: () => CampagneService;
  public campagne: ICampagne = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.campagneId) {
        vm.retrieveCampagne(to.params.campagneId);
      }
    });
  }

  public retrieveCampagne(campagneId) {
    this.campagneService()
      .find(campagneId)
      .then(res => {
        this.campagne = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
