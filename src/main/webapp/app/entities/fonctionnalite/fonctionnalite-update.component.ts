import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import SuiviCampagneService from '../suivi-campagne/suivi-campagne.service';
import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';

import ChapitreService from '../chapitre/chapitre.service';
import { IChapitre } from '@/shared/model/chapitre.model';

import AlertService from '@/shared/alert/alert.service';
import { IFonctionnalite, Fonctionnalite } from '@/shared/model/fonctionnalite.model';
import FonctionnaliteService from './fonctionnalite.service';

const validations: any = {
  fonctionnalite: {
    nom: {},
    description: {},
    estActive: {}
  }
};

@Component({
  validations
})
export default class FonctionnaliteUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('fonctionnaliteService') private fonctionnaliteService: () => FonctionnaliteService;
  public fonctionnalite: IFonctionnalite = new Fonctionnalite();

  @Inject('suiviCampagneService') private suiviCampagneService: () => SuiviCampagneService;

  public suiviCampagnes: ISuiviCampagne[] = [];

  @Inject('chapitreService') private chapitreService: () => ChapitreService;

  public chapitres: IChapitre[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fonctionnaliteId) {
        vm.retrieveFonctionnalite(to.params.fonctionnaliteId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.fonctionnalite.id) {
      this.fonctionnaliteService()
        .update(this.fonctionnalite)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Fonctionnalite is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.fonctionnaliteService()
        .create(this.fonctionnalite)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Fonctionnalite is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveFonctionnalite(fonctionnaliteId): void {
    this.fonctionnaliteService()
      .find(fonctionnaliteId)
      .then(res => {
        this.fonctionnalite = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.suiviCampagneService()
      .retrieve()
      .then(res => {
        this.suiviCampagnes = res.data;
      });
    this.chapitreService()
      .retrieve()
      .then(res => {
        this.chapitres = res.data;
      });
  }
}
