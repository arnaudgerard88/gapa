import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFonctionnalite } from '@/shared/model/fonctionnalite.model';
import FonctionnaliteService from './fonctionnalite.service';

@Component
export default class FonctionnaliteDetails extends Vue {
  @Inject('fonctionnaliteService') private fonctionnaliteService: () => FonctionnaliteService;
  public fonctionnalite: IFonctionnalite = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fonctionnaliteId) {
        vm.retrieveFonctionnalite(to.params.fonctionnaliteId);
      }
    });
  }

  public retrieveFonctionnalite(fonctionnaliteId) {
    this.fonctionnaliteService()
      .find(fonctionnaliteId)
      .then(res => {
        this.fonctionnalite = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
