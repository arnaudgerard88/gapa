import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import CorrectionPlannifieeService from '../correction-plannifiee/correction-plannifiee.service';
import { ICorrectionPlannifiee } from '@/shared/model/correction-plannifiee.model';

import AnomalieEtablissementService from '../anomalie-etablissement/anomalie-etablissement.service';
import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';

import UtilisateurService from '../utilisateur/utilisateur.service';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

import VersionService from '../version/version.service';
import { IVersion } from '@/shared/model/version.model';

import SuiviCampagneService from '../suivi-campagne/suivi-campagne.service';
import { ISuiviCampagne } from '@/shared/model/suivi-campagne.model';

import DomaineService from '../domaine/domaine.service';
import { IDomaine } from '@/shared/model/domaine.model';

import AlertService from '@/shared/alert/alert.service';
import { IAnomalie, Anomalie } from '@/shared/model/anomalie.model';
import AnomalieService from './anomalie.service';

const validations: any = {
  anomalie: {
    numInterne: {},
    numExterne: {},
    titre: {},
    description: {},
    statut: {},
    criticite: {},
    dateDeclaration: {},
    dateCloture: {}
  }
};

@Component({
  validations
})
export default class AnomalieUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('anomalieService') private anomalieService: () => AnomalieService;
  public anomalie: IAnomalie = new Anomalie();

  public anomalies: IAnomalie[] = [];

  @Inject('correctionPlannifieeService') private correctionPlannifieeService: () => CorrectionPlannifieeService;

  public correctionPlannifiees: ICorrectionPlannifiee[] = [];

  @Inject('anomalieEtablissementService') private anomalieEtablissementService: () => AnomalieEtablissementService;

  public anomalieEtablissements: IAnomalieEtablissement[] = [];

  @Inject('utilisateurService') private utilisateurService: () => UtilisateurService;

  public utilisateurs: IUtilisateur[] = [];

  @Inject('versionService') private versionService: () => VersionService;

  public versions: IVersion[] = [];

  @Inject('suiviCampagneService') private suiviCampagneService: () => SuiviCampagneService;

  public suiviCampagnes: ISuiviCampagne[] = [];

  @Inject('domaineService') private domaineService: () => DomaineService;

  public domaines: IDomaine[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.anomalieId) {
        vm.retrieveAnomalie(to.params.anomalieId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.anomalie.id) {
      this.anomalieService()
        .update(this.anomalie)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Anomalie is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.anomalieService()
        .create(this.anomalie)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Anomalie is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.anomalie[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.anomalie[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.anomalie[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.anomalie[field] = null;
    }
  }

  public retrieveAnomalie(anomalieId): void {
    this.anomalieService()
      .find(anomalieId)
      .then(res => {
        res.dateDeclaration = new Date(res.dateDeclaration);
        res.dateCloture = new Date(res.dateCloture);
        this.anomalie = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.correctionPlannifieeService()
      .retrieve()
      .then(res => {
        this.correctionPlannifiees = res.data;
      });
    this.anomalieEtablissementService()
      .retrieve()
      .then(res => {
        this.anomalieEtablissements = res.data;
      });
    this.utilisateurService()
      .retrieve()
      .then(res => {
        this.utilisateurs = res.data;
      });
    this.versionService()
      .retrieve()
      .then(res => {
        this.versions = res.data;
      });
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.suiviCampagneService()
      .retrieve()
      .then(res => {
        this.suiviCampagnes = res.data;
      });
    this.domaineService()
      .retrieve()
      .then(res => {
        this.domaines = res.data;
      });
  }
}
