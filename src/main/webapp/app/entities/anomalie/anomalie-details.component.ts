import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAnomalie } from '@/shared/model/anomalie.model';
import AnomalieService from './anomalie.service';

@Component
export default class AnomalieDetails extends Vue {
  @Inject('anomalieService') private anomalieService: () => AnomalieService;
  public anomalie: IAnomalie = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.anomalieId) {
        vm.retrieveAnomalie(to.params.anomalieId);
      }
    });
  }

  public retrieveAnomalie(anomalieId) {
    this.anomalieService()
      .find(anomalieId)
      .then(res => {
        this.anomalie = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
