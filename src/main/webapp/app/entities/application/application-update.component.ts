import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import DomaineService from '../domaine/domaine.service';
import { IDomaine } from '@/shared/model/domaine.model';

import VersionService from '../version/version.service';
import { IVersion } from '@/shared/model/version.model';

import UtilisateurService from '../utilisateur/utilisateur.service';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

import EnvironnementService from '../environnement/environnement.service';
import { IEnvironnement } from '@/shared/model/environnement.model';

import AlertService from '@/shared/alert/alert.service';
import { IApplication, Application } from '@/shared/model/application.model';
import ApplicationService from './application.service';

const validations: any = {
  application: {
    nom: {},
    email: {},
    dateAjout: {}
  }
};

@Component({
  validations
})
export default class ApplicationUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('applicationService') private applicationService: () => ApplicationService;
  public application: IApplication = new Application();

  @Inject('domaineService') private domaineService: () => DomaineService;

  public domaines: IDomaine[] = [];

  @Inject('versionService') private versionService: () => VersionService;

  public versions: IVersion[] = [];

  @Inject('utilisateurService') private utilisateurService: () => UtilisateurService;

  public utilisateurs: IUtilisateur[] = [];

  @Inject('environnementService') private environnementService: () => EnvironnementService;

  public environnements: IEnvironnement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.applicationId) {
        vm.retrieveApplication(to.params.applicationId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.application.id) {
      this.applicationService()
        .update(this.application)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Application is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.applicationService()
        .create(this.application)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Application is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.application[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.application[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.application[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.application[field] = null;
    }
  }

  public retrieveApplication(applicationId): void {
    this.applicationService()
      .find(applicationId)
      .then(res => {
        res.dateAjout = new Date(res.dateAjout);
        this.application = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.domaineService()
      .retrieve()
      .then(res => {
        this.domaines = res.data;
      });
    this.versionService()
      .retrieve()
      .then(res => {
        this.versions = res.data;
      });
    this.utilisateurService()
      .retrieve()
      .then(res => {
        this.utilisateurs = res.data;
      });
    this.environnementService()
      .retrieve()
      .then(res => {
        this.environnements = res.data;
      });
  }
}
