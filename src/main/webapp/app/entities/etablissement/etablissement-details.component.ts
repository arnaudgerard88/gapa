import { Component, Vue, Inject } from 'vue-property-decorator';

import { IEtablissement } from '@/shared/model/etablissement.model';
import EtablissementService from './etablissement.service';

@Component
export default class EtablissementDetails extends Vue {
  @Inject('etablissementService') private etablissementService: () => EtablissementService;
  public etablissement: IEtablissement = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.etablissementId) {
        vm.retrieveEtablissement(to.params.etablissementId);
      }
    });
  }

  public retrieveEtablissement(etablissementId) {
    this.etablissementService()
      .find(etablissementId)
      .then(res => {
        this.etablissement = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
