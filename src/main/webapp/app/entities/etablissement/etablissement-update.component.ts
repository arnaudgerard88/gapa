import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AnomalieEtablissementService from '../anomalie-etablissement/anomalie-etablissement.service';
import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';

import AlertService from '@/shared/alert/alert.service';
import { IEtablissement, Etablissement } from '@/shared/model/etablissement.model';
import EtablissementService from './etablissement.service';

const validations: any = {
  etablissement: {
    nomEtab: {}
  }
};

@Component({
  validations
})
export default class EtablissementUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('etablissementService') private etablissementService: () => EtablissementService;
  public etablissement: IEtablissement = new Etablissement();

  @Inject('anomalieEtablissementService') private anomalieEtablissementService: () => AnomalieEtablissementService;

  public anomalieEtablissements: IAnomalieEtablissement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.etablissementId) {
        vm.retrieveEtablissement(to.params.etablissementId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.etablissement.id) {
      this.etablissementService()
        .update(this.etablissement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Etablissement is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.etablissementService()
        .create(this.etablissement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Etablissement is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveEtablissement(etablissementId): void {
    this.etablissementService()
      .find(etablissementId)
      .then(res => {
        this.etablissement = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieEtablissementService()
      .retrieve()
      .then(res => {
        this.anomalieEtablissements = res.data;
      });
  }
}
