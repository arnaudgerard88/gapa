import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import DomaineService from '../domaine/domaine.service';
import { IDomaine } from '@/shared/model/domaine.model';

import ChapitreService from '../chapitre/chapitre.service';
import { IChapitre } from '@/shared/model/chapitre.model';

import ResponsabiliteCahierService from '../responsabilite-cahier/responsabilite-cahier.service';
import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';

import AlertService from '@/shared/alert/alert.service';
import { ICahier, Cahier } from '@/shared/model/cahier.model';
import CahierService from './cahier.service';

const validations: any = {
  cahier: {
    nom: {},
    dateMaj: {}
  }
};

@Component({
  validations
})
export default class CahierUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('cahierService') private cahierService: () => CahierService;
  public cahier: ICahier = new Cahier();

  @Inject('domaineService') private domaineService: () => DomaineService;

  public domaines: IDomaine[] = [];

  @Inject('chapitreService') private chapitreService: () => ChapitreService;

  public chapitres: IChapitre[] = [];

  @Inject('responsabiliteCahierService') private responsabiliteCahierService: () => ResponsabiliteCahierService;

  public responsabiliteCahiers: IResponsabiliteCahier[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cahierId) {
        vm.retrieveCahier(to.params.cahierId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.cahier.id) {
      this.cahierService()
        .update(this.cahier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Cahier is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.cahierService()
        .create(this.cahier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Cahier is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.cahier[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.cahier[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.cahier[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.cahier[field] = null;
    }
  }

  public retrieveCahier(cahierId): void {
    this.cahierService()
      .find(cahierId)
      .then(res => {
        res.dateMaj = new Date(res.dateMaj);
        this.cahier = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.domaineService()
      .retrieve()
      .then(res => {
        this.domaines = res.data;
      });
    this.chapitreService()
      .retrieve()
      .then(res => {
        this.chapitres = res.data;
      });
    this.responsabiliteCahierService()
      .retrieve()
      .then(res => {
        this.responsabiliteCahiers = res.data;
      });
  }
}
