import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICahier } from '@/shared/model/cahier.model';
import CahierService from './cahier.service';

@Component
export default class CahierDetails extends Vue {
  @Inject('cahierService') private cahierService: () => CahierService;
  public cahier: ICahier = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cahierId) {
        vm.retrieveCahier(to.params.cahierId);
      }
    });
  }

  public retrieveCahier(cahierId) {
    this.cahierService()
      .find(cahierId)
      .then(res => {
        this.cahier = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
