import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import CahierService from '../cahier/cahier.service';
import { ICahier } from '@/shared/model/cahier.model';

import ApplicationService from '../application/application.service';
import { IApplication } from '@/shared/model/application.model';

import AlertService from '@/shared/alert/alert.service';
import { IDomaine, Domaine } from '@/shared/model/domaine.model';
import DomaineService from './domaine.service';

const validations: any = {
  domaine: {
    nom: {}
  }
};

@Component({
  validations
})
export default class DomaineUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('domaineService') private domaineService: () => DomaineService;
  public domaine: IDomaine = new Domaine();

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];

  @Inject('cahierService') private cahierService: () => CahierService;

  public cahiers: ICahier[] = [];

  @Inject('applicationService') private applicationService: () => ApplicationService;

  public applications: IApplication[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.domaineId) {
        vm.retrieveDomaine(to.params.domaineId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.domaine.id) {
      this.domaineService()
        .update(this.domaine)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Domaine is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.domaineService()
        .create(this.domaine)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Domaine is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveDomaine(domaineId): void {
    this.domaineService()
      .find(domaineId)
      .then(res => {
        this.domaine = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.cahierService()
      .retrieve()
      .then(res => {
        this.cahiers = res.data;
      });
    this.applicationService()
      .retrieve()
      .then(res => {
        this.applications = res.data;
      });
  }
}
