import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDomaine } from '@/shared/model/domaine.model';
import DomaineService from './domaine.service';

@Component
export default class DomaineDetails extends Vue {
  @Inject('domaineService') private domaineService: () => DomaineService;
  public domaine: IDomaine = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.domaineId) {
        vm.retrieveDomaine(to.params.domaineId);
      }
    });
  }

  public retrieveDomaine(domaineId) {
    this.domaineService()
      .find(domaineId)
      .then(res => {
        this.domaine = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
