import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import EtablissementService from '../etablissement/etablissement.service';
import { IEtablissement } from '@/shared/model/etablissement.model';

import AlertService from '@/shared/alert/alert.service';
import { IAnomalieEtablissement, AnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';
import AnomalieEtablissementService from './anomalie-etablissement.service';

const validations: any = {
  anomalieEtablissement: {
    date: {}
  }
};

@Component({
  validations
})
export default class AnomalieEtablissementUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('anomalieEtablissementService') private anomalieEtablissementService: () => AnomalieEtablissementService;
  public anomalieEtablissement: IAnomalieEtablissement = new AnomalieEtablissement();

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];

  @Inject('etablissementService') private etablissementService: () => EtablissementService;

  public etablissements: IEtablissement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.anomalieEtablissementId) {
        vm.retrieveAnomalieEtablissement(to.params.anomalieEtablissementId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.anomalieEtablissement.id) {
      this.anomalieEtablissementService()
        .update(this.anomalieEtablissement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A AnomalieEtablissement is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.anomalieEtablissementService()
        .create(this.anomalieEtablissement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A AnomalieEtablissement is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.anomalieEtablissement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.anomalieEtablissement[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.anomalieEtablissement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.anomalieEtablissement[field] = null;
    }
  }

  public retrieveAnomalieEtablissement(anomalieEtablissementId): void {
    this.anomalieEtablissementService()
      .find(anomalieEtablissementId)
      .then(res => {
        res.date = new Date(res.date);
        this.anomalieEtablissement = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.etablissementService()
      .retrieve()
      .then(res => {
        this.etablissements = res.data;
      });
  }
}
