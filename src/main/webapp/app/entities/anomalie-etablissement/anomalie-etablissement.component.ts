import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import AnomalieEtablissementService from './anomalie-etablissement.service';

@Component
export default class AnomalieEtablissement extends mixins(Vue2Filters.mixin, AlertMixin) {
  @Inject('anomalieEtablissementService') private anomalieEtablissementService: () => AnomalieEtablissementService;
  private removeId: number = null;
  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;

  public anomalieEtablissements: IAnomalieEtablissement[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllAnomalieEtablissements();
  }

  public clear(): void {
    this.page = 1;
    this.retrieveAllAnomalieEtablissements();
  }

  public retrieveAllAnomalieEtablissements(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    this.anomalieEtablissementService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.anomalieEtablissements = res.data;
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IAnomalieEtablissement): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeAnomalieEtablissement(): void {
    this.anomalieEtablissementService()
      .delete(this.removeId)
      .then(() => {
        const message = 'A AnomalieEtablissement is deleted with identifier ' + this.removeId;
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllAnomalieEtablissements();
        this.closeDialog();
      });
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  public loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  public transition(): void {
    this.retrieveAllAnomalieEtablissements();
  }

  public changeOrder(propOrder): void {
    this.propOrder = propOrder;
    this.reverse = !this.reverse;
    this.transition();
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
