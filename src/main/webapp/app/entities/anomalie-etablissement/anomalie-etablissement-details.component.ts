import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAnomalieEtablissement } from '@/shared/model/anomalie-etablissement.model';
import AnomalieEtablissementService from './anomalie-etablissement.service';

@Component
export default class AnomalieEtablissementDetails extends Vue {
  @Inject('anomalieEtablissementService') private anomalieEtablissementService: () => AnomalieEtablissementService;
  public anomalieEtablissement: IAnomalieEtablissement = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.anomalieEtablissementId) {
        vm.retrieveAnomalieEtablissement(to.params.anomalieEtablissementId);
      }
    });
  }

  public retrieveAnomalieEtablissement(anomalieEtablissementId) {
    this.anomalieEtablissementService()
      .find(anomalieEtablissementId)
      .then(res => {
        this.anomalieEtablissement = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
