import { Component, Vue, Inject } from 'vue-property-decorator';

import { IUtilisateur } from '@/shared/model/utilisateur.model';
import UtilisateurService from './utilisateur.service';

@Component
export default class UtilisateurDetails extends Vue {
  @Inject('utilisateurService') private utilisateurService: () => UtilisateurService;
  public utilisateur: IUtilisateur = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.utilisateurId) {
        vm.retrieveUtilisateur(to.params.utilisateurId);
      }
    });
  }

  public retrieveUtilisateur(utilisateurId) {
    this.utilisateurService()
      .find(utilisateurId)
      .then(res => {
        this.utilisateur = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
