import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AnomalieService from '../anomalie/anomalie.service';
import { IAnomalie } from '@/shared/model/anomalie.model';

import ResponsabiliteCahierService from '../responsabilite-cahier/responsabilite-cahier.service';
import { IResponsabiliteCahier } from '@/shared/model/responsabilite-cahier.model';

import ApplicationService from '../application/application.service';
import { IApplication } from '@/shared/model/application.model';

import AlertService from '@/shared/alert/alert.service';
import { IUtilisateur, Utilisateur } from '@/shared/model/utilisateur.model';
import UtilisateurService from './utilisateur.service';

const validations: any = {
  utilisateur: {
    nom: {},
    prenom: {},
    email: {}
  }
};

@Component({
  validations
})
export default class UtilisateurUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('utilisateurService') private utilisateurService: () => UtilisateurService;
  public utilisateur: IUtilisateur = new Utilisateur();

  @Inject('anomalieService') private anomalieService: () => AnomalieService;

  public anomalies: IAnomalie[] = [];

  @Inject('responsabiliteCahierService') private responsabiliteCahierService: () => ResponsabiliteCahierService;

  public responsabiliteCahiers: IResponsabiliteCahier[] = [];

  @Inject('applicationService') private applicationService: () => ApplicationService;

  public applications: IApplication[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.utilisateurId) {
        vm.retrieveUtilisateur(to.params.utilisateurId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.utilisateur.id) {
      this.utilisateurService()
        .update(this.utilisateur)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Utilisateur is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.utilisateurService()
        .create(this.utilisateur)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Utilisateur is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveUtilisateur(utilisateurId): void {
    this.utilisateurService()
      .find(utilisateurId)
      .then(res => {
        this.utilisateur = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.anomalieService()
      .retrieve()
      .then(res => {
        this.anomalies = res.data;
      });
    this.responsabiliteCahierService()
      .retrieve()
      .then(res => {
        this.responsabiliteCahiers = res.data;
      });
    this.applicationService()
      .retrieve()
      .then(res => {
        this.applications = res.data;
      });
  }
}
