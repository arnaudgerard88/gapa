import { Component, Vue, Inject } from 'vue-property-decorator';

import { IEnvironnement } from '@/shared/model/environnement.model';
import EnvironnementService from './environnement.service';

@Component
export default class EnvironnementDetails extends Vue {
  @Inject('environnementService') private environnementService: () => EnvironnementService;
  public environnement: IEnvironnement = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.environnementId) {
        vm.retrieveEnvironnement(to.params.environnementId);
      }
    });
  }

  public retrieveEnvironnement(environnementId) {
    this.environnementService()
      .find(environnementId)
      .then(res => {
        this.environnement = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
