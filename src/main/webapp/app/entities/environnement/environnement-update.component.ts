import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import CampagneService from '../campagne/campagne.service';
import { ICampagne } from '@/shared/model/campagne.model';

import DeploiementService from '../deploiement/deploiement.service';
import { IDeploiement } from '@/shared/model/deploiement.model';

import ApplicationService from '../application/application.service';
import { IApplication } from '@/shared/model/application.model';

import TypeEnvService from '../type-env/type-env.service';
import { ITypeEnv } from '@/shared/model/type-env.model';

import AlertService from '@/shared/alert/alert.service';
import { IEnvironnement, Environnement } from '@/shared/model/environnement.model';
import EnvironnementService from './environnement.service';

const validations: any = {
  environnement: {
    dateAjout: {},
    estActif: {}
  }
};

@Component({
  validations
})
export default class EnvironnementUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('environnementService') private environnementService: () => EnvironnementService;
  public environnement: IEnvironnement = new Environnement();

  @Inject('campagneService') private campagneService: () => CampagneService;

  public campagnes: ICampagne[] = [];

  @Inject('deploiementService') private deploiementService: () => DeploiementService;

  public deploiements: IDeploiement[] = [];

  @Inject('applicationService') private applicationService: () => ApplicationService;

  public applications: IApplication[] = [];

  @Inject('typeEnvService') private typeEnvService: () => TypeEnvService;

  public typeEnvs: ITypeEnv[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.environnementId) {
        vm.retrieveEnvironnement(to.params.environnementId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.environnement.id) {
      this.environnementService()
        .update(this.environnement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Environnement is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.environnementService()
        .create(this.environnement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Environnement is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.environnement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.environnement[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.environnement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.environnement[field] = null;
    }
  }

  public retrieveEnvironnement(environnementId): void {
    this.environnementService()
      .find(environnementId)
      .then(res => {
        res.dateAjout = new Date(res.dateAjout);
        this.environnement = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.campagneService()
      .retrieve()
      .then(res => {
        this.campagnes = res.data;
      });
    this.deploiementService()
      .retrieve()
      .then(res => {
        this.deploiements = res.data;
      });
    this.applicationService()
      .retrieve()
      .then(res => {
        this.applications = res.data;
      });
    this.typeEnvService()
      .retrieve()
      .then(res => {
        this.typeEnvs = res.data;
      });
  }
}
