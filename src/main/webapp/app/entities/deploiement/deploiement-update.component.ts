import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import VersionService from '../version/version.service';
import { IVersion } from '@/shared/model/version.model';

import EnvironnementService from '../environnement/environnement.service';
import { IEnvironnement } from '@/shared/model/environnement.model';

import AlertService from '@/shared/alert/alert.service';
import { IDeploiement, Deploiement } from '@/shared/model/deploiement.model';
import DeploiementService from './deploiement.service';

const validations: any = {
  deploiement: {
    estActif: {},
    date: {}
  }
};

@Component({
  validations
})
export default class DeploiementUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('deploiementService') private deploiementService: () => DeploiementService;
  public deploiement: IDeploiement = new Deploiement();

  @Inject('versionService') private versionService: () => VersionService;

  public versions: IVersion[] = [];

  @Inject('environnementService') private environnementService: () => EnvironnementService;

  public environnements: IEnvironnement[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deploiementId) {
        vm.retrieveDeploiement(to.params.deploiementId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.deploiement.id) {
      this.deploiementService()
        .update(this.deploiement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Deploiement is updated with identifier ' + param.id;
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.deploiementService()
        .create(this.deploiement)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Deploiement is created with identifier ' + param.id;
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.deploiement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.deploiement[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.deploiement[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.deploiement[field] = null;
    }
  }

  public retrieveDeploiement(deploiementId): void {
    this.deploiementService()
      .find(deploiementId)
      .then(res => {
        res.date = new Date(res.date);
        this.deploiement = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.versionService()
      .retrieve()
      .then(res => {
        this.versions = res.data;
      });
    this.environnementService()
      .retrieve()
      .then(res => {
        this.environnements = res.data;
      });
  }
}
