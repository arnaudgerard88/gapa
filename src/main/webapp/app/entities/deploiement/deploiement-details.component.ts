import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDeploiement } from '@/shared/model/deploiement.model';
import DeploiementService from './deploiement.service';

@Component
export default class DeploiementDetails extends Vue {
  @Inject('deploiementService') private deploiementService: () => DeploiementService;
  public deploiement: IDeploiement = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deploiementId) {
        vm.retrieveDeploiement(to.params.deploiementId);
      }
    });
  }

  public retrieveDeploiement(deploiementId) {
    this.deploiementService()
      .find(deploiementId)
      .then(res => {
        this.deploiement = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
