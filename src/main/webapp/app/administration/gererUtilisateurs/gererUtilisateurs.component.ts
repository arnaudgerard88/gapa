import { Component, Vue } from 'vue-property-decorator';

import UserManagement from '@/admin/user-management/user-management.vue';

@Component({
  components: {
    userManagement: UserManagement
  }
})
export default class GererUtilisateurs extends Vue {
  constructor() {
    super();
  }

  public mounted(): void {}
}
