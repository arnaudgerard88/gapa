import { Vue, Component } from 'vue-property-decorator';

import GererApplications from '@/administration/gererApplications/gererApplications.vue';
import GererUtilisateurs from '@/administration/gererUtilisateurs/gererUtilisateurs.vue';
import GererCahiers from '@/administration/gererCahiers/gererCahiers.vue';
import GererCampagnes from '@/administration/gererCampagnes/gererCampagnes.vue';

@Component({
  components: {
    gererApplications: GererApplications,
    gererUtilisateurs: GererUtilisateurs,
    gererCahiers: GererCahiers,
    gererCampagnes: GererCampagnes
  }
})
export default class Administration extends Vue {
  public tabIndex: number;

  constructor() {
    super();
    this.tabIndex = 0;
  }

  public linkClass(idx) {
    if (this.tabIndex === idx) {
      return ['bg-primary', 'text-light'];
    } else {
      return ['bg-light', 'text-info'];
    }
  }
}
