import { Component, Vue } from 'vue-property-decorator';

import Campagne from '@/entities/campagne/campagne.vue';

@Component({
  components: {
    campagne: Campagne
  }
})
export default class GererCampagnes extends Vue {
  constructor() {
    super();
  }

  public mounted(): void {}
}
