import { Component, Vue } from 'vue-property-decorator';
import Application from '@/entities/application/application.vue';
import Version from '@/entities/version/version.vue';
import Environnement from '@/entities/environnement/environnement.vue';
import Domaine from '@/entities/domaine/domaine.vue';
import Deploiement from '@/entities/deploiement/deploiement.vue';

@Component({
  components: {
    application: Application,
    version: Version,
    environnement: Environnement,
    domaine: Domaine,
    deploiement: Deploiement
  }
})
export default class GererApplications extends Vue {
  public tabIndex: number;

  constructor() {
    super();
    this.tabIndex = 0;
  }

  mounted(): void {}

  public linkClass(idx) {
    if (this.tabIndex === idx) {
      return ['bg-primary', 'text-light'];
    } else {
      return ['bg-light', 'text-info'];
    }
  }
}
