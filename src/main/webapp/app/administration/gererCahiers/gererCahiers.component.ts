import { Component, Vue } from 'vue-property-decorator';
import Cahier from '@/entities/cahier/cahier.vue';

@Component({
  components: {
    cahier: Cahier
  }
})
export default class GererCahiers extends Vue {
  constructor() {
    super();
  }

  public mounted(): void {}
}
