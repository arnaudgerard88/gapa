import { Inject, Vue, Component } from 'vue-property-decorator';

import { IApplication, Application } from '@/shared/model/application.model';
import ApplicationService from '@/entities/application/application.service';

import { eventBus } from '@/main';

@Component
export default class listeAppliDashboard extends Vue {
  public modes: ['single'];
  public fields: string[];
  public items: any[];
  public selectMode: string;
  public selected: any[];

  props: ['choixAppliId'];

  @Inject('applicationService') private applicationService: () => ApplicationService;

  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;

  public isFetching = false;
  public applications: IApplication[] = [];

  constructor() {
    super();
    this.fields = ['nom'];
    this.selectMode = 'single';
    this.items = [];
    this.selected = [];
  }

  $refs!: {
    selectableTable;
  };

  public mounted(): void {
    this.retrieveAllApplications();
    console.log(this.items);
  }

  public onRowSelected(items) {
    if (items.length != 0) {
      this.selected = items;
      eventBus.$emit('choixAppliId', this.selected[0].id);
      eventBus.$emit('choixAppli', this.selected[0].nom);

      eventBus.$emit('environnementID', null);
      eventBus.$emit('environnementNom', null);
      eventBus.$emit('versionEnvSelecNom', null);
      eventBus.$emit('versionEnvSelecId', null);
    } else {
      this.selected = [];
    }
  }

  public selectAllRows() {
    this.$refs.selectableTable.selectAllRows();
  }

  public retrieveAllApplications(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    this.applicationService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.applications = res.data;
          this.items = [];
          this.applications.forEach(val => this.items.push({ nom: val.nom, id: val.id }));
          //MODIFIER ICI POUR DEFINIR L APPLICATION SELECTIONNEE PAR DEFAULT
          if (this.items.length != 0) {
            this.onRowSelected(this.items[0].nom);
            this.$refs.selectableTable.selectAllRows();
            //this.$refs.selectableTable.selectRow(0);
          }
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  /**
  @Inject('applicationService') private applicationService: () => ApplicationService;
  private removeId: number = null;
  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;
  public selected: [];

  public applications: IApplication[] = [];

  public isFetching = false;

  public create(): void {
    this.retrieveAllApplications();
  }

  public clear(): void {
    this.page = 1;
    this.retrieveAllApplications();
  }

  public retrieveAllApplications(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    this.applicationService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.applications = res.data;
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  public loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  public transition(): void {
    this.retrieveAllApplications();
  }

  public changeOrder(propOrder): void {
    this.propOrder = propOrder;
    this.reverse = !this.reverse;
    this.transition();
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
**/
}
