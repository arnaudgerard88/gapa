import { Vue, Component } from 'vue-property-decorator';

@Component
export default class tableauCampagnes extends Vue {
  public fields: ['first_name', 'last_name', 'age'];
  public items: [
    { age: 40; first_name: 'Dickerson'; last_name: 'Macdonald' },
    { age: 21; first_name: 'Larsen'; last_name: 'Shaw' },
    { age: 89; first_name: 'Geneva'; last_name: 'Wilson' }
  ];
  public tableVariants: ['primary', 'secondary', 'info', 'danger', 'warning', 'success', 'light', 'dark'];
  public striped: false;
  public bordered: false;
  public borderless: false;
  public outlined: false;
  public small: false;
  public hover: false;
  public dark: false;
  public fixed: false;
  public footClone: false;
  public headVariant: null;
  public tableVariant: '';
  public noCollapse: false;

  constructor() {
    super();
    this.fields = ['first_name', 'last_name', 'age'];
    this.items = [
      { age: 40, first_name: 'Dickerson', last_name: 'Macdonald' },
      { age: 21, first_name: 'Larsen', last_name: 'Shaw' },
      { age: 89, first_name: 'Geneva', last_name: 'Wilson' }
    ];
    this.tableVariants = ['primary', 'secondary', 'info', 'danger', 'warning', 'success', 'light', 'dark'];
    this.striped = false;
    this.bordered = false;
    this.borderless = false;
    this.outlined = false;
    this.small = false;
    this.hover = false;
    this.dark = false;
    this.fixed = false;
    this.footClone = false;
    this.headVariant = null;
    this.tableVariant = '';
    this.noCollapse = false;
  }
}
