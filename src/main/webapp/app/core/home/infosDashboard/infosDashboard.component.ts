import { Vue, Component, Inject } from 'vue-property-decorator';
import { eventBus } from '@/main';
import ApplicationService from '@/entities/application/application.service';
import { IApplication } from '@/shared/model/application.model';

@Component
export default class infosDashboard extends Vue {
  @Inject('applicationService') private applicationService: () => ApplicationService;
  public application: IApplication = null;

  public isFetching = false;

  public nomEnv: string;

  public versionEnv: string;

  constructor() {
    super();
    this.nomEnv = '';
    this.versionEnv = '';
  }

  mounted(): void {
    eventBus.$on('choixAppliId', data => {
      this.getNomApplicationFromTableauAppli(data);
    });

    eventBus.$on('environnementNom', data => {
      this.nomEnv = data;
    });

    eventBus.$on('versionEnvSelecNom', data => {
      this.versionEnv = data;
    });

    // eventBus.$on('environnementNom', data => {
    //   this.getVersionEnvironnement(data);
    // });
  }

  public getNomApplicationFromTableauAppli(idApplication: number): void {
    this.isFetching = true;
    if (idApplication != null) {
      this.applicationService()
        .find(idApplication)
        .then(
          res => {
            this.application = res;
            this.isFetching = false;
          },
          err => {
            this.isFetching = false;
            console.error(err);
          }
        );
    }
  }

  // public getVersionEnvironnement(idVersion: string){
  //   this.isFetching = true;
  //   if (idVersion != null){
  //
  //   }
  // }
}
