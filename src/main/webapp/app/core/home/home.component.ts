import Component from 'vue-class-component';
import { Inject, Vue } from 'vue-property-decorator';
import LoginService from '@/account/login.service';

import { IApplication } from '@/shared/model/application.model';
import ApplicationService from '@/entities/application/application.service';

import ListeAppliDashboard from '@/core/home/listeAppliDashboard/listeAppliDashboard.vue';
import TableauEnvironnements from '@/core/home/tableauEnvironnements/tableauEnvironnements.vue';
import ChoixEnvironnement from '@/core/home/choixEnvironnement/choixEnvironnement.vue';
import TableauCampagnes from '@/core/home/tableauCampagnes/tableauCampagnes.vue';
import TableauAnomalies from '@/core/home/tableauAnomalies/tableauAnomalies.vue';
import GraphiqueDemandesDashboard from '@/core/home/graphiqueDemandesDashboard/graphiqueDemandesDashboard.vue';
import InfosDashboard from '@/core/home/infosDashboard/infosDashboard.vue';

@Component({
  components: {
    listeAppliDashboard: ListeAppliDashboard,
    tableauEnvironnements: TableauEnvironnements,
    choixEnvironnement: ChoixEnvironnement,
    tableauCampagnes: TableauCampagnes,
    tableauAnomalies: TableauAnomalies,
    graphiqueDemandesDashboard: GraphiqueDemandesDashboard,
    infosDashboard: InfosDashboard
  }
})
export default class Home extends Vue {
  @Inject('loginService')
  private loginService: () => LoginService;

  public openLogin(): void {
    this.loginService().openLogin((<any>this).$root);
  }

  public get authenticated(): boolean {
    return this.$store.getters.authenticated;
  }

  public get username(): string {
    return this.$store.getters.account ? this.$store.getters.account.login : '';
  }

  @Inject('applicationService') private applicationService: () => ApplicationService;
  private removeId: number = null;
  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;
  public selected: [];

  public applications: IApplication[] = [];

  public isFetching = false;

  public choixAppli = null;

  public choixAppliId = null;

  public mounted(): void {
    this.retrieveAllApplications();
  }

  public clear(): void {
    this.page = 1;
    this.retrieveAllApplications();
  }

  public retrieveAllApplications(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    this.applicationService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.applications = res.data;
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  public loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  public transition(): void {
    this.retrieveAllApplications();
  }

  public changeOrder(propOrder): void {
    this.propOrder = propOrder;
    this.reverse = !this.reverse;
    this.transition();
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }

  public choisirAppli(choix) {
    this.choixAppli = choix;
  }

  public choisirAppliId(choix) {
    this.choixAppliId = choix;
  }
}
