import { Inject, Vue, Component } from 'vue-property-decorator';
import ApplicationService from '@/entities/application/application.service';
import { IApplication } from '@/shared/model/application.model';

import { eventBus } from '@/main';

@Component
export default class choixEnvironnement extends Vue {
  //acceptation des messages venant des autres composant

  // @Prop({ required: true })
  // public choixAppliId: string;
  //
  // @Prop({ required: true })
  // public choixAppli: string;

  public environnementSelectionne: string;
  public options: any[];

  @Inject('applicationService') private applicationService: () => ApplicationService;
  public application: IApplication = null;

  public isFetching = false;

  public versionActiveId: number;

  public versionActiveNom: string;

  $refs!: {
    btn_env;
  };

  constructor() {
    super();
    this.environnementSelectionne = 'radio1';
    this.options = [
      { text: 'Radio 1', value: 'radio1' },
      { text: 'Radio 3', value: 'radio2' },
      { text: 'Radio 3', value: 'radio3' },
      { text: 'Radio 4', value: 'radio4' }
    ];

    this.versionActiveId = null;
    this.versionActiveNom = '';
  }

  mounted(): void {
    eventBus.$on('choixAppliId', data => {
      this.getEnvironnementsOfApplication(data);
    });
  }

  // computed(): void {
  //   this.choixAppli = '';
  //   this.choixAppliId = '';
  // }

  public notifierChoixEnv(envSelec: string) {
    if (this.options === []) {
      eventBus.$emit('environnementID', null);
      eventBus.$emit('environnementNom', null);
      eventBus.$emit('versionEnvSelecNom', null);
      eventBus.$emit('versionEnvSelecId', null);
    } else {
      eventBus.$emit('environnementID', envSelec);
      eventBus.$emit('environnementNom', this.options.find(element => element.value == envSelec).text);
      eventBus.$emit('versionEnvSelecNom', this.options.find(element => element.value == envSelec).versionNom);
      eventBus.$emit('versionEnvSelecId', this.options.find(element => element.value == envSelec).versionId);
    }
  }

  public getEnvironnementsOfApplication(idApplication: string): void {
    this.isFetching = true;
    if (idApplication != null) {
      this.applicationService()
        .find(Number(idApplication))
        .then(
          res => {
            this.application = res;
            this.options = [];

            this.options = [];
            this.application.environnements.forEach(val => {
              let envId = val.id;
              this.versionActiveNom = null;
              this.versionActiveId = null;
              val.deploiements.forEach(val => {
                if (val.estActif == 1 && val.environnement.id == envId) {
                  this.versionActiveNom = val.version.nom;
                  this.versionActiveId = val.version.id;
                }
              });
              this.options.push({
                text: val.typeEnv.nom,
                value: val.id,
                versionNom: this.versionActiveNom,
                versionId: this.versionActiveId
              });
            });
            if (this.options.length != 0) {
              this.environnementSelectionne = this.options[0].value;
              this.notifierChoixEnv(this.environnementSelectionne);
            }

            this.isFetching = false;
          },
          err => {
            this.isFetching = false;
            console.error(err);
          }
        );
    }
  }

  //public getVersionActive()
}
