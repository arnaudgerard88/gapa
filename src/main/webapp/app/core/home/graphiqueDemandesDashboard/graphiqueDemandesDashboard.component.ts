import PieChart from '@/core/chartjs/pieChart.vue';

import { Vue, Component } from 'vue-property-decorator';

@Component({
  components: {
    pieChart: PieChart
  }
})
export default class graphiqueDemandesDashboard extends Vue {
  public donnesExemple: number[];

  public labelsExemple: string[];

  constructor() {
    super();
    this.donnesExemple = [15, 23, 26, 39, 9];
    this.labelsExemple = ['IMS', 'SMS', 'DxPlanning', 'Prescription', 'Bloc'];
  }

  public mounted() {}

  $refs!: {
    barChart;
  };
}
