import { Pie } from 'vue-chartjs';

export default {
  extends: Pie,

  methods: {
    loadData(texts, values) {
      let i;
      let colorList = [
        '#4a8fff',
        '#ffe97d',
        '#ff8847',
        '#a5ff85',
        '#ff8c9c',
        '#75ff33',
        '#9d00cf',
        '#cf9d00',
        '#cf3500',
        '#00ffc9',
        '#b9ff00'
      ];

      if (values.length > 11) {
        for (i = 0; i < values.lengh - 11; i++) {
          colorList.push(
            '#' +
              Math.random()
                .toString(16)
                .slice(2, 8)
                .toUpperCase()
          );
        }
      }

      this.renderChart(
        {
          labels: texts,
          datasets: [
            {
              backgroundColor: colorList,
              data: values
            }
          ]
        },
        {
          responsive: true,
          maintainAspectRatio: false
          // tooltips: {
          //   callbacks: {
          //     label: function (tooltipItem, data) {
          //       const dataset = data.datasets[tooltipItem.dataSetIndex];
          //       const total = dataset.data.reduce(function (previousValue, currentValue) {
          //         return previousValue + currentValue
          //       });
          //       const currentValue = dataset.data[tooltipItem.index];
          //
          //       const percentage = Math.floor(((currentValue / total) * 100) + 0.5);
          //       return currentValue + ' soit ' + percentage + '%';
          //     }
          //   }
          // }
        }
      );
    }
  }
};
