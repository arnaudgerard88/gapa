// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.common with an alias.
import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import App from './app.vue';
import Vue2Filters from 'vue2-filters';
import router from './router';
import * as config from './shared/config/config';
import * as bootstrapVueConfig from './shared/config/config-bootstrap-vue';
import JhiItemCountComponent from './shared/jhi-item-count.vue';
import JhiSortIndicatorComponent from './shared/sort/jhi-sort-indicator.vue';
import InfiniteLoading from 'vue-infinite-loading';
import AuditsService from './admin/audits/audits.service';

import HealthService from './admin/health/health.service';
import MetricsService from './admin/metrics/metrics.service';
import LogsService from './admin/logs/logs.service';
import ActivateService from './account/activate/activate.service';
import RegisterService from './account/register/register.service';
import UserManagementService from '@/admin/user-management/user-management.service';

import LoginService from './account/login.service';
import AccountService from './account/account.service';

import '../content/scss/vendor.scss';
import AlertService from '@/shared/alert/alert.service';
import ConfigurationService from '@/admin/configuration/configuration.service';

/* tslint:disable */

import ApplicationService from '@/entities/application/application.service';
import DomaineService from '@/entities/domaine/domaine.service';
import EnvironnementService from '@/entities/environnement/environnement.service';
import VersionService from '@/entities/version/version.service';
import AnomalieService from '@/entities/anomalie/anomalie.service';
import UtilisateurService from '@/entities/utilisateur/utilisateur.service';
import CampagneService from '@/entities/campagne/campagne.service';
import CahierService from '@/entities/cahier/cahier.service';
import ChapitreService from '@/entities/chapitre/chapitre.service';
import FonctionnaliteService from '@/entities/fonctionnalite/fonctionnalite.service';
import EtablissementService from '@/entities/etablissement/etablissement.service';
import TypeEnvService from '@/entities/type-env/type-env.service';
import DeploiementService from '@/entities/deploiement/deploiement.service';
import CorrectionPlannifieeService from '@/entities/correction-plannifiee/correction-plannifiee.service';
import SuiviCampagneService from '@/entities/suivi-campagne/suivi-campagne.service';
import ResponsabiliteCahierService from '@/entities/responsabilite-cahier/responsabilite-cahier.service';
import AnomalieEtablissementService from '@/entities/anomalie-etablissement/anomalie-etablissement.service';
// jhipster-needle-add-entity-service-to-main-import - JHipster will import entities services here

/* tslint:enable */
Vue.config.productionTip = false;
config.initVueApp(Vue);
config.initFortAwesome(Vue);
bootstrapVueConfig.initBootstrapVue(Vue);
Vue.use(Vue2Filters);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('jhi-item-count', JhiItemCountComponent);
Vue.component('jhi-sort-indicator', JhiSortIndicatorComponent);
Vue.component('infinite-loading', InfiniteLoading);

const store = config.initVueXStore(Vue);

export const eventBus = new Vue();

const alertService = new AlertService(store);
const loginService = new LoginService();
const accountService = new AccountService(store, router);

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next('/not-found');
  }

  if (to.meta && to.meta.authorities && to.meta.authorities.length > 0) {
    if (!accountService.hasAnyAuthority(to.meta.authorities)) {
      sessionStorage.setItem('requested-url', to.fullPath);
      next('/forbidden');
    } else {
      next();
    }
  } else {
    // no authorities, so just proceed
    next();
  }
});

/* tslint:disable */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  provide: {
    loginService: () => loginService,
    activateService: () => new ActivateService(),
    registerService: () => new RegisterService(),
    userService: () => new UserManagementService(),

    auditsService: () => new AuditsService(),

    healthService: () => new HealthService(),

    configurationService: () => new ConfigurationService(),
    logsService: () => new LogsService(),
    metricsService: () => new MetricsService(),
    alertService: () => alertService,
    applicationService: () => new ApplicationService(),
    domaineService: () => new DomaineService(),
    environnementService: () => new EnvironnementService(),
    versionService: () => new VersionService(),
    anomalieService: () => new AnomalieService(),
    utilisateurService: () => new UtilisateurService(),
    campagneService: () => new CampagneService(),
    cahierService: () => new CahierService(),
    chapitreService: () => new ChapitreService(),
    fonctionnaliteService: () => new FonctionnaliteService(),
    etablissementService: () => new EtablissementService(),
    typeEnvService: () => new TypeEnvService(),
    deploiementService: () => new DeploiementService(),
    correctionPlannifieeService: () => new CorrectionPlannifieeService(),
    suiviCampagneService: () => new SuiviCampagneService(),
    responsabiliteCahierService: () => new ResponsabiliteCahierService(),
    anomalieEtablissementService: () => new AnomalieEtablissementService(),
    // jhipster-needle-add-entity-service-to-main - JHipster will import entities services here
    accountService: () => accountService
  },
  store
});
