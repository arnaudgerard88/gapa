import Vue from 'vue';
import Component from 'vue-class-component';
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
]);
import Router from 'vue-router';
import { Authority } from '@/shared/security/authority';
const Home = () => import('../core/home/home.vue');
const Error = () => import('../core/error/error.vue');
const Register = () => import('../account/register/register.vue');
const Activate = () => import('../account/activate/activate.vue');
const ResetPasswordInit = () => import('../account/reset-password/init/reset-password-init.vue');
const ResetPasswordFinish = () => import('../account/reset-password/finish/reset-password-finish.vue');
const ChangePassword = () => import('../account/change-password/change-password.vue');
const Settings = () => import('../account/settings/settings.vue');
const JhiUserManagementComponent = () => import('../admin/user-management/user-management.vue');
const JhiUserManagementViewComponent = () => import('../admin/user-management/user-management-view.vue');
const JhiUserManagementEditComponent = () => import('../admin/user-management/user-management-edit.vue');
const JhiConfigurationComponent = () => import('../admin/configuration/configuration.vue');
const JhiDocsComponent = () => import('../admin/docs/docs.vue');
const JhiHealthComponent = () => import('../admin/health/health.vue');
const JhiLogsComponent = () => import('../admin/logs/logs.vue');
const JhiAuditsComponent = () => import('../admin/audits/audits.vue');
const JhiMetricsComponent = () => import('../admin/metrics/metrics.vue');
/* tslint:disable */
// prettier-ignore
const Application = () => import('../entities/application/application.vue');
// prettier-ignore
const ApplicationUpdate = () => import('../entities/application/application-update.vue');
// prettier-ignore
const ApplicationDetails = () => import('../entities/application/application-details.vue');
// prettier-ignore
const Domaine = () => import('../entities/domaine/domaine.vue');
// prettier-ignore
const DomaineUpdate = () => import('../entities/domaine/domaine-update.vue');
// prettier-ignore
const DomaineDetails = () => import('../entities/domaine/domaine-details.vue');
// prettier-ignore
const Environnement = () => import('../entities/environnement/environnement.vue');
// prettier-ignore
const EnvironnementUpdate = () => import('../entities/environnement/environnement-update.vue');
// prettier-ignore
const EnvironnementDetails = () => import('../entities/environnement/environnement-details.vue');
// prettier-ignore
const Version = () => import('../entities/version/version.vue');
// prettier-ignore
const VersionUpdate = () => import('../entities/version/version-update.vue');
// prettier-ignore
const VersionDetails = () => import('../entities/version/version-details.vue');
// prettier-ignore
const Anomalie = () => import('../entities/anomalie/anomalie.vue');
// prettier-ignore
const AnomalieUpdate = () => import('../entities/anomalie/anomalie-update.vue');
// prettier-ignore
const AnomalieDetails = () => import('../entities/anomalie/anomalie-details.vue');
// prettier-ignore
const Utilisateur = () => import('../entities/utilisateur/utilisateur.vue');
// prettier-ignore
const UtilisateurUpdate = () => import('../entities/utilisateur/utilisateur-update.vue');
// prettier-ignore
const UtilisateurDetails = () => import('../entities/utilisateur/utilisateur-details.vue');
// prettier-ignore
const Campagne = () => import('../entities/campagne/campagne.vue');
// prettier-ignore
const CampagneUpdate = () => import('../entities/campagne/campagne-update.vue');
// prettier-ignore
const CampagneDetails = () => import('../entities/campagne/campagne-details.vue');
// prettier-ignore
const Cahier = () => import('../entities/cahier/cahier.vue');
// prettier-ignore
const CahierUpdate = () => import('../entities/cahier/cahier-update.vue');
// prettier-ignore
const CahierDetails = () => import('../entities/cahier/cahier-details.vue');
// prettier-ignore
const Chapitre = () => import('../entities/chapitre/chapitre.vue');
// prettier-ignore
const ChapitreUpdate = () => import('../entities/chapitre/chapitre-update.vue');
// prettier-ignore
const ChapitreDetails = () => import('../entities/chapitre/chapitre-details.vue');
// prettier-ignore
const Fonctionnalite = () => import('../entities/fonctionnalite/fonctionnalite.vue');
// prettier-ignore
const FonctionnaliteUpdate = () => import('../entities/fonctionnalite/fonctionnalite-update.vue');
// prettier-ignore
const FonctionnaliteDetails = () => import('../entities/fonctionnalite/fonctionnalite-details.vue');
// prettier-ignore
const Etablissement = () => import('../entities/etablissement/etablissement.vue');
// prettier-ignore
const EtablissementUpdate = () => import('../entities/etablissement/etablissement-update.vue');
// prettier-ignore
const EtablissementDetails = () => import('../entities/etablissement/etablissement-details.vue');
// prettier-ignore
const TypeEnv = () => import('../entities/type-env/type-env.vue');
// prettier-ignore
const TypeEnvUpdate = () => import('../entities/type-env/type-env-update.vue');
// prettier-ignore
const TypeEnvDetails = () => import('../entities/type-env/type-env-details.vue');
// prettier-ignore
const Deploiement = () => import('../entities/deploiement/deploiement.vue');
// prettier-ignore
const DeploiementUpdate = () => import('../entities/deploiement/deploiement-update.vue');
// prettier-ignore
const DeploiementDetails = () => import('../entities/deploiement/deploiement-details.vue');
// prettier-ignore
const CorrectionPlannifiee = () => import('../entities/correction-plannifiee/correction-plannifiee.vue');
// prettier-ignore
const CorrectionPlannifieeUpdate = () => import('../entities/correction-plannifiee/correction-plannifiee-update.vue');
// prettier-ignore
const CorrectionPlannifieeDetails = () => import('../entities/correction-plannifiee/correction-plannifiee-details.vue');
// prettier-ignore
const SuiviCampagne = () => import('../entities/suivi-campagne/suivi-campagne.vue');
// prettier-ignore
const SuiviCampagneUpdate = () => import('../entities/suivi-campagne/suivi-campagne-update.vue');
// prettier-ignore
const SuiviCampagneDetails = () => import('../entities/suivi-campagne/suivi-campagne-details.vue');
// prettier-ignore
const ResponsabiliteCahier = () => import('../entities/responsabilite-cahier/responsabilite-cahier.vue');
// prettier-ignore
const ResponsabiliteCahierUpdate = () => import('../entities/responsabilite-cahier/responsabilite-cahier-update.vue');
// prettier-ignore
const ResponsabiliteCahierDetails = () => import('../entities/responsabilite-cahier/responsabilite-cahier-details.vue');
// prettier-ignore
const AnomalieEtablissement = () => import('../entities/anomalie-etablissement/anomalie-etablissement.vue');
// prettier-ignore
const AnomalieEtablissementUpdate = () => import('../entities/anomalie-etablissement/anomalie-etablissement-update.vue');
// prettier-ignore
const AnomalieEtablissementDetails = () => import('../entities/anomalie-etablissement/anomalie-etablissement-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

//route vers la page administration
const Administration = () => import('../administration/administration.vue');

Vue.use(Router);

// prettier-ignore
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forbidden',
      name: 'Forbidden',
      component: Error,
      meta: { error403: true }
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: Error,
      meta: { error404: true }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/account/activate',
      name: 'Activate',
      component: Activate
    },
    {
      path: '/account/reset/request',
      name: 'ResetPasswordInit',
      component: ResetPasswordInit
    },
    {
      path: '/account/reset/finish',
      name: 'ResetPasswordFinish',
      component: ResetPasswordFinish
    },
    {
      path: '/account/password',
      name: 'ChangePassword',
      component: ChangePassword,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/account/settings',
      name: 'Settings',
      component: Settings,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/admin/user-management',
      name: 'JhiUser',
      component: JhiUserManagementComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/user-management/new',
      name: 'JhiUserCreate',
      component: JhiUserManagementEditComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/user-management/:userId/edit',
      name: 'JhiUserEdit',
      component: JhiUserManagementEditComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/user-management/:userId/view',
      name: 'JhiUserView',
      component: JhiUserManagementViewComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/docs',
      name: 'JhiDocsComponent',
      component: JhiDocsComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/audits',
      name: 'JhiAuditsComponent',
      component: JhiAuditsComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/jhi-health',
      name: 'JhiHealthComponent',
      component: JhiHealthComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/logs',
      name: 'JhiLogsComponent',
      component: JhiLogsComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/jhi-metrics',
      name: 'JhiMetricsComponent',
      component: JhiMetricsComponent,
      meta: { authorities: [Authority.ADMIN] }
    },
    {
      path: '/admin/jhi-configuration',
      name: 'JhiConfigurationComponent',
      component: JhiConfigurationComponent,
      meta: { authorities: [Authority.ADMIN] }
    }
    ,
    {
      path: '/application',
      name: 'Application',
      component: Application,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/application/new',
      name: 'ApplicationCreate',
      component: ApplicationUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/application/:applicationId/edit',
      name: 'ApplicationEdit',
      component: ApplicationUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/application/:applicationId/view',
      name: 'ApplicationView',
      component: ApplicationDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/domaine',
      name: 'Domaine',
      component: Domaine,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/domaine/new',
      name: 'DomaineCreate',
      component: DomaineUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/domaine/:domaineId/edit',
      name: 'DomaineEdit',
      component: DomaineUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/domaine/:domaineId/view',
      name: 'DomaineView',
      component: DomaineDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/environnement',
      name: 'Environnement',
      component: Environnement,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/environnement/new',
      name: 'EnvironnementCreate',
      component: EnvironnementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/environnement/:environnementId/edit',
      name: 'EnvironnementEdit',
      component: EnvironnementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/environnement/:environnementId/view',
      name: 'EnvironnementView',
      component: EnvironnementDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/version',
      name: 'Version',
      component: Version,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/version/new',
      name: 'VersionCreate',
      component: VersionUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/version/:versionId/edit',
      name: 'VersionEdit',
      component: VersionUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/version/:versionId/view',
      name: 'VersionView',
      component: VersionDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/anomalie',
      name: 'Anomalie',
      component: Anomalie,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie/new',
      name: 'AnomalieCreate',
      component: AnomalieUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie/:anomalieId/edit',
      name: 'AnomalieEdit',
      component: AnomalieUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie/:anomalieId/view',
      name: 'AnomalieView',
      component: AnomalieDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/utilisateur',
      name: 'Utilisateur',
      component: Utilisateur,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/utilisateur/new',
      name: 'UtilisateurCreate',
      component: UtilisateurUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/utilisateur/:utilisateurId/edit',
      name: 'UtilisateurEdit',
      component: UtilisateurUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/utilisateur/:utilisateurId/view',
      name: 'UtilisateurView',
      component: UtilisateurDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/campagne',
      name: 'Campagne',
      component: Campagne,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/campagne/new',
      name: 'CampagneCreate',
      component: CampagneUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/campagne/:campagneId/edit',
      name: 'CampagneEdit',
      component: CampagneUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/campagne/:campagneId/view',
      name: 'CampagneView',
      component: CampagneDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/cahier',
      name: 'Cahier',
      component: Cahier,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/cahier/new',
      name: 'CahierCreate',
      component: CahierUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/cahier/:cahierId/edit',
      name: 'CahierEdit',
      component: CahierUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/cahier/:cahierId/view',
      name: 'CahierView',
      component: CahierDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/chapitre',
      name: 'Chapitre',
      component: Chapitre,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/chapitre/new',
      name: 'ChapitreCreate',
      component: ChapitreUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/chapitre/:chapitreId/edit',
      name: 'ChapitreEdit',
      component: ChapitreUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/chapitre/:chapitreId/view',
      name: 'ChapitreView',
      component: ChapitreDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/fonctionnalite',
      name: 'Fonctionnalite',
      component: Fonctionnalite,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/fonctionnalite/new',
      name: 'FonctionnaliteCreate',
      component: FonctionnaliteUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/fonctionnalite/:fonctionnaliteId/edit',
      name: 'FonctionnaliteEdit',
      component: FonctionnaliteUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/fonctionnalite/:fonctionnaliteId/view',
      name: 'FonctionnaliteView',
      component: FonctionnaliteDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/etablissement',
      name: 'Etablissement',
      component: Etablissement,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/etablissement/new',
      name: 'EtablissementCreate',
      component: EtablissementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/etablissement/:etablissementId/edit',
      name: 'EtablissementEdit',
      component: EtablissementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/etablissement/:etablissementId/view',
      name: 'EtablissementView',
      component: EtablissementDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/type-env',
      name: 'TypeEnv',
      component: TypeEnv,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/type-env/new',
      name: 'TypeEnvCreate',
      component: TypeEnvUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/type-env/:typeEnvId/edit',
      name: 'TypeEnvEdit',
      component: TypeEnvUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/type-env/:typeEnvId/view',
      name: 'TypeEnvView',
      component: TypeEnvDetails,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/administration',
      name: 'Administration',
      component: Administration,
      meta: { authorities: [Authority.ADMIN]}
    }
    ,
    {
      path: '/deploiement',
      name: 'Deploiement',
      component: Deploiement,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/deploiement/new',
      name: 'DeploiementCreate',
      component: DeploiementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/deploiement/:deploiementId/edit',
      name: 'DeploiementEdit',
      component: DeploiementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/deploiement/:deploiementId/view',
      name: 'DeploiementView',
      component: DeploiementDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/correction-plannifiee',
      name: 'CorrectionPlannifiee',
      component: CorrectionPlannifiee,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/correction-plannifiee/new',
      name: 'CorrectionPlannifieeCreate',
      component: CorrectionPlannifieeUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/correction-plannifiee/:correctionPlannifieeId/edit',
      name: 'CorrectionPlannifieeEdit',
      component: CorrectionPlannifieeUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/correction-plannifiee/:correctionPlannifieeId/view',
      name: 'CorrectionPlannifieeView',
      component: CorrectionPlannifieeDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/suivi-campagne',
      name: 'SuiviCampagne',
      component: SuiviCampagne,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/suivi-campagne/new',
      name: 'SuiviCampagneCreate',
      component: SuiviCampagneUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/suivi-campagne/:suiviCampagneId/edit',
      name: 'SuiviCampagneEdit',
      component: SuiviCampagneUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/suivi-campagne/:suiviCampagneId/view',
      name: 'SuiviCampagneView',
      component: SuiviCampagneDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/responsabilite-cahier',
      name: 'ResponsabiliteCahier',
      component: ResponsabiliteCahier,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/responsabilite-cahier/new',
      name: 'ResponsabiliteCahierCreate',
      component: ResponsabiliteCahierUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/responsabilite-cahier/:responsabiliteCahierId/edit',
      name: 'ResponsabiliteCahierEdit',
      component: ResponsabiliteCahierUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/responsabilite-cahier/:responsabiliteCahierId/view',
      name: 'ResponsabiliteCahierView',
      component: ResponsabiliteCahierDetails,
      meta: { authorities: [Authority.USER] }
    }
    ,
    {
      path: '/anomalie-etablissement',
      name: 'AnomalieEtablissement',
      component: AnomalieEtablissement,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie-etablissement/new',
      name: 'AnomalieEtablissementCreate',
      component: AnomalieEtablissementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie-etablissement/:anomalieEtablissementId/edit',
      name: 'AnomalieEtablissementEdit',
      component: AnomalieEtablissementUpdate,
      meta: { authorities: [Authority.USER] }
    },
    {
      path: '/anomalie-etablissement/:anomalieEtablissementId/view',
      name: 'AnomalieEtablissementView',
      component: AnomalieEtablissementDetails,
      meta: { authorities: [Authority.USER] }
    }
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ]
});
