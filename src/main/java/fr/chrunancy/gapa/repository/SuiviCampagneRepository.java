package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.SuiviCampagne;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SuiviCampagne entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuiviCampagneRepository extends JpaRepository<SuiviCampagne, Long> {
}
