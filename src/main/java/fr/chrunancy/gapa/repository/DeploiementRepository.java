package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.Deploiement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Deploiement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeploiementRepository extends JpaRepository<Deploiement, Long> {
}
