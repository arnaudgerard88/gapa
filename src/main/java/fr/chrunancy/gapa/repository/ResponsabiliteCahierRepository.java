package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.ResponsabiliteCahier;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ResponsabiliteCahier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResponsabiliteCahierRepository extends JpaRepository<ResponsabiliteCahier, Long> {
}
