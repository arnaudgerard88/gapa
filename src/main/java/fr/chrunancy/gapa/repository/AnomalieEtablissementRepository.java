package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.AnomalieEtablissement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AnomalieEtablissement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnomalieEtablissementRepository extends JpaRepository<AnomalieEtablissement, Long> {
}
