package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.Chapitre;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Chapitre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChapitreRepository extends JpaRepository<Chapitre, Long> {
}
