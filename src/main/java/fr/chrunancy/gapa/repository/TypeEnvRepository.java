package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.TypeEnv;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TypeEnv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeEnvRepository extends JpaRepository<TypeEnv, Long> {
}
