package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.Cahier;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Cahier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CahierRepository extends JpaRepository<Cahier, Long> {
}
