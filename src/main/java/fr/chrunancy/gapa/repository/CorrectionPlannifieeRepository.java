package fr.chrunancy.gapa.repository;

import fr.chrunancy.gapa.domain.CorrectionPlannifiee;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CorrectionPlannifiee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CorrectionPlannifieeRepository extends JpaRepository<CorrectionPlannifiee, Long> {
}
