package fr.chrunancy.gapa.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, fr.chrunancy.gapa.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, fr.chrunancy.gapa.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, fr.chrunancy.gapa.domain.User.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Authority.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.User.class.getName() + ".authorities");
            createCache(cm, fr.chrunancy.gapa.domain.Application.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Application.class.getName() + ".domaines");
            createCache(cm, fr.chrunancy.gapa.domain.Application.class.getName() + ".versions");
            createCache(cm, fr.chrunancy.gapa.domain.Application.class.getName() + ".utilisateurs");
            createCache(cm, fr.chrunancy.gapa.domain.Domaine.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Environnement.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Environnement.class.getName() + ".campagnes");
            createCache(cm, fr.chrunancy.gapa.domain.Environnement.class.getName() + ".versionEnvs");
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName() + ".anomalies");
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName() + ".versionEnvs");
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName() + ".correctionPlanifiees");
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName() + ".anomalies");
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName() + ".correctionPlanifiees");
            createCache(cm, fr.chrunancy.gapa.domain.Utilisateur.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Utilisateur.class.getName() + ".anomalies");
            createCache(cm, fr.chrunancy.gapa.domain.Utilisateur.class.getName() + ".estResponsables");
            createCache(cm, fr.chrunancy.gapa.domain.Campagne.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Campagne.class.getName() + ".suiviCampagnes");
            createCache(cm, fr.chrunancy.gapa.domain.Cahier.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Cahier.class.getName() + ".chapitres");
            createCache(cm, fr.chrunancy.gapa.domain.Cahier.class.getName() + ".estResponsables");
            createCache(cm, fr.chrunancy.gapa.domain.Chapitre.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Chapitre.class.getName() + ".fonctionnalites");
            createCache(cm, fr.chrunancy.gapa.domain.Fonctionnalite.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Fonctionnalite.class.getName() + ".suiviCampagnes");
            createCache(cm, fr.chrunancy.gapa.domain.Etablissement.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Etablissement.class.getName() + ".anomalies");
            createCache(cm, fr.chrunancy.gapa.domain.TypeEnv.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.TypeEnv.class.getName() + ".environnements");
            createCache(cm, fr.chrunancy.gapa.domain.Application.class.getName() + ".environnements");
            createCache(cm, fr.chrunancy.gapa.domain.Environnement.class.getName() + ".deploiements");
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName() + ".deploiements");
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName() + ".etablissements");
            createCache(cm, fr.chrunancy.gapa.domain.Deploiement.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Version.class.getName() + ".correctionPlannifiees");
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName() + ".correctionPlannifiees");
            createCache(cm, fr.chrunancy.gapa.domain.Anomalie.class.getName() + ".anomalieEtablissements");
            createCache(cm, fr.chrunancy.gapa.domain.Utilisateur.class.getName() + ".responsabiliteCahiers");
            createCache(cm, fr.chrunancy.gapa.domain.Cahier.class.getName() + ".responsabiliteCahiers");
            createCache(cm, fr.chrunancy.gapa.domain.Etablissement.class.getName() + ".anomalieEtablissements");
            createCache(cm, fr.chrunancy.gapa.domain.CorrectionPlannifiee.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.SuiviCampagne.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.SuiviCampagne.class.getName() + ".anomalies");
            createCache(cm, fr.chrunancy.gapa.domain.ResponsabiliteCahier.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.AnomalieEtablissement.class.getName());
            createCache(cm, fr.chrunancy.gapa.domain.Domaine.class.getName() + ".anomalies");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

}
