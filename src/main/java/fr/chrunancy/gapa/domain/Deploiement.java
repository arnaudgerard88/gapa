package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Deploiement.
 */
@Entity
@Table(name = "deploiement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Deploiement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "est_actif")
    private Integer estActif;

    @Column(name = "date")
    private Instant date;

    @ManyToOne
    @JsonIgnoreProperties("deploiements")
    private Version version;

    @ManyToOne
    @JsonIgnoreProperties("deploiements")
    private Environnement environnement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEstActif() {
        return estActif;
    }

    public Deploiement estActif(Integer estActif) {
        this.estActif = estActif;
        return this;
    }

    public void setEstActif(Integer estActif) {
        this.estActif = estActif;
    }

    public Instant getDate() {
        return date;
    }

    public Deploiement date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Version getVersion() {
        return version;
    }

    public Deploiement version(Version version) {
        this.version = version;
        return this;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Environnement getEnvironnement() {
        return environnement;
    }

    public Deploiement environnement(Environnement environnement) {
        this.environnement = environnement;
        return this;
    }

    public void setEnvironnement(Environnement environnement) {
        this.environnement = environnement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Deploiement)) {
            return false;
        }
        return id != null && id.equals(((Deploiement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Deploiement{" +
            "id=" + getId() +
            ", estActif=" + getEstActif() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
