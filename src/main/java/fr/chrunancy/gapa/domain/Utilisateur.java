package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Utilisateur.
 */
@Entity
@Table(name = "utilisateur")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Anomalie> anomalies = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ResponsabiliteCahier> responsabiliteCahiers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("utilisateurs")
    private Application application;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Utilisateur nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Utilisateur prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Utilisateur email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Anomalie> getAnomalies() {
        return anomalies;
    }

    public Utilisateur anomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
        return this;
    }

    public Utilisateur addAnomalie(Anomalie anomalie) {
        this.anomalies.add(anomalie);
        anomalie.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeAnomalie(Anomalie anomalie) {
        this.anomalies.remove(anomalie);
        anomalie.setUtilisateur(null);
        return this;
    }

    public void setAnomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
    }

    public Set<ResponsabiliteCahier> getResponsabiliteCahiers() {
        return responsabiliteCahiers;
    }

    public Utilisateur responsabiliteCahiers(Set<ResponsabiliteCahier> responsabiliteCahiers) {
        this.responsabiliteCahiers = responsabiliteCahiers;
        return this;
    }

    public Utilisateur addResponsabiliteCahier(ResponsabiliteCahier responsabiliteCahier) {
        this.responsabiliteCahiers.add(responsabiliteCahier);
        responsabiliteCahier.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeResponsabiliteCahier(ResponsabiliteCahier responsabiliteCahier) {
        this.responsabiliteCahiers.remove(responsabiliteCahier);
        responsabiliteCahier.setUtilisateur(null);
        return this;
    }

    public void setResponsabiliteCahiers(Set<ResponsabiliteCahier> responsabiliteCahiers) {
        this.responsabiliteCahiers = responsabiliteCahiers;
    }

    public Application getApplication() {
        return application;
    }

    public Utilisateur application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        return id != null && id.equals(((Utilisateur) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
