package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Version.
 */
@Entity
@Table(name = "version")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Version implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date_mise_prod")
    private Instant dateMiseProd;

    @OneToMany(mappedBy = "version")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Anomalie> anomalies = new HashSet<>();

    @OneToMany(mappedBy = "version")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Deploiement> deploiements = new HashSet<>();

    @OneToMany(mappedBy = "version")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CorrectionPlannifiee> correctionPlannifiees = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("versions")
    private Application application;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Version nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Instant getDateMiseProd() {
        return dateMiseProd;
    }

    public Version dateMiseProd(Instant dateMiseProd) {
        this.dateMiseProd = dateMiseProd;
        return this;
    }

    public void setDateMiseProd(Instant dateMiseProd) {
        this.dateMiseProd = dateMiseProd;
    }

    public Set<Anomalie> getAnomalies() {
        return anomalies;
    }

    public Version anomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
        return this;
    }

    public Version addAnomalie(Anomalie anomalie) {
        this.anomalies.add(anomalie);
        anomalie.setVersion(this);
        return this;
    }

    public Version removeAnomalie(Anomalie anomalie) {
        this.anomalies.remove(anomalie);
        anomalie.setVersion(null);
        return this;
    }

    public void setAnomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
    }

    public Set<Deploiement> getDeploiements() {
        return deploiements;
    }

    public Version deploiements(Set<Deploiement> deploiements) {
        this.deploiements = deploiements;
        return this;
    }

    public Version addDeploiement(Deploiement deploiement) {
        this.deploiements.add(deploiement);
        deploiement.setVersion(this);
        return this;
    }

    public Version removeDeploiement(Deploiement deploiement) {
        this.deploiements.remove(deploiement);
        deploiement.setVersion(null);
        return this;
    }

    public void setDeploiements(Set<Deploiement> deploiements) {
        this.deploiements = deploiements;
    }

    public Set<CorrectionPlannifiee> getCorrectionPlannifiees() {
        return correctionPlannifiees;
    }

    public Version correctionPlannifiees(Set<CorrectionPlannifiee> correctionPlannifiees) {
        this.correctionPlannifiees = correctionPlannifiees;
        return this;
    }

    public Version addCorrectionPlannifiee(CorrectionPlannifiee correctionPlannifiee) {
        this.correctionPlannifiees.add(correctionPlannifiee);
        correctionPlannifiee.setVersion(this);
        return this;
    }

    public Version removeCorrectionPlannifiee(CorrectionPlannifiee correctionPlannifiee) {
        this.correctionPlannifiees.remove(correctionPlannifiee);
        correctionPlannifiee.setVersion(null);
        return this;
    }

    public void setCorrectionPlannifiees(Set<CorrectionPlannifiee> correctionPlannifiees) {
        this.correctionPlannifiees = correctionPlannifiees;
    }

    public Application getApplication() {
        return application;
    }

    public Version application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Version)) {
            return false;
        }
        return id != null && id.equals(((Version) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Version{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", dateMiseProd='" + getDateMiseProd() + "'" +
            "}";
    }
}
