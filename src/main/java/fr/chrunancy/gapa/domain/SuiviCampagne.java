package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A SuiviCampagne.
 */
@Entity
@Table(name = "suivi_campagne")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SuiviCampagne implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "etat")
    private String etat;

    @Column(name = "date")
    private Instant date;

    @OneToMany(mappedBy = "suiviCampagne")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Anomalie> anomalies = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("suiviCampagnes")
    private Fonctionnalite fonctionnalite;

    @ManyToOne
    @JsonIgnoreProperties("suiviCampagnes")
    private Campagne campagne;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtat() {
        return etat;
    }

    public SuiviCampagne etat(String etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Instant getDate() {
        return date;
    }

    public SuiviCampagne date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Set<Anomalie> getAnomalies() {
        return anomalies;
    }

    public SuiviCampagne anomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
        return this;
    }

    public SuiviCampagne addAnomalie(Anomalie anomalie) {
        this.anomalies.add(anomalie);
        anomalie.setSuiviCampagne(this);
        return this;
    }

    public SuiviCampagne removeAnomalie(Anomalie anomalie) {
        this.anomalies.remove(anomalie);
        anomalie.setSuiviCampagne(null);
        return this;
    }

    public void setAnomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
    }

    public Fonctionnalite getFonctionnalite() {
        return fonctionnalite;
    }

    public SuiviCampagne fonctionnalite(Fonctionnalite fonctionnalite) {
        this.fonctionnalite = fonctionnalite;
        return this;
    }

    public void setFonctionnalite(Fonctionnalite fonctionnalite) {
        this.fonctionnalite = fonctionnalite;
    }

    public Campagne getCampagne() {
        return campagne;
    }

    public SuiviCampagne campagne(Campagne campagne) {
        this.campagne = campagne;
        return this;
    }

    public void setCampagne(Campagne campagne) {
        this.campagne = campagne;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SuiviCampagne)) {
            return false;
        }
        return id != null && id.equals(((SuiviCampagne) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SuiviCampagne{" +
            "id=" + getId() +
            ", etat='" + getEtat() + "'" +
            ", date='" + getDate() + "'" +
            "}";
    }
}
