package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Chapitre.
 */
@Entity
@Table(name = "chapitre")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Chapitre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date_maj")
    private Instant dateMaj;

    @OneToMany(mappedBy = "chapitre")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Fonctionnalite> fonctionnalites = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("chapitres")
    private Cahier cahier;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Chapitre nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Instant getDateMaj() {
        return dateMaj;
    }

    public Chapitre dateMaj(Instant dateMaj) {
        this.dateMaj = dateMaj;
        return this;
    }

    public void setDateMaj(Instant dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Set<Fonctionnalite> getFonctionnalites() {
        return fonctionnalites;
    }

    public Chapitre fonctionnalites(Set<Fonctionnalite> fonctionnalites) {
        this.fonctionnalites = fonctionnalites;
        return this;
    }

    public Chapitre addFonctionnalite(Fonctionnalite fonctionnalite) {
        this.fonctionnalites.add(fonctionnalite);
        fonctionnalite.setChapitre(this);
        return this;
    }

    public Chapitre removeFonctionnalite(Fonctionnalite fonctionnalite) {
        this.fonctionnalites.remove(fonctionnalite);
        fonctionnalite.setChapitre(null);
        return this;
    }

    public void setFonctionnalites(Set<Fonctionnalite> fonctionnalites) {
        this.fonctionnalites = fonctionnalites;
    }

    public Cahier getCahier() {
        return cahier;
    }

    public Chapitre cahier(Cahier cahier) {
        this.cahier = cahier;
        return this;
    }

    public void setCahier(Cahier cahier) {
        this.cahier = cahier;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Chapitre)) {
            return false;
        }
        return id != null && id.equals(((Chapitre) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Chapitre{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", dateMaj='" + getDateMaj() + "'" +
            "}";
    }
}
