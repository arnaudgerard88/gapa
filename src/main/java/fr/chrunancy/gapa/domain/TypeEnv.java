package fr.chrunancy.gapa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A TypeEnv.
 */
@Entity
@Table(name = "type_env")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TypeEnv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @OneToMany(mappedBy = "typeEnv")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Environnement> environnements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public TypeEnv nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Environnement> getEnvironnements() {
        return environnements;
    }

    public TypeEnv environnements(Set<Environnement> environnements) {
        this.environnements = environnements;
        return this;
    }

    public TypeEnv addEnvironnement(Environnement environnement) {
        this.environnements.add(environnement);
        environnement.setTypeEnv(this);
        return this;
    }

    public TypeEnv removeEnvironnement(Environnement environnement) {
        this.environnements.remove(environnement);
        environnement.setTypeEnv(null);
        return this;
    }

    public void setEnvironnements(Set<Environnement> environnements) {
        this.environnements = environnements;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeEnv)) {
            return false;
        }
        return id != null && id.equals(((TypeEnv) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TypeEnv{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
