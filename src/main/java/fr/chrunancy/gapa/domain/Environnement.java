package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Environnement.
 */
@Entity
@Table(name = "environnement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Environnement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_ajout")
    private Instant dateAjout;

    @Column(name = "est_actif")
    private Boolean estActif;

    @OneToMany(mappedBy = "environnement")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Campagne> campagnes = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER
        ,mappedBy = "environnement")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Deploiement> deploiements = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties("environnements")
    private Application application;

    @ManyToOne
    @JsonIgnoreProperties("environnements")
    private TypeEnv typeEnv;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateAjout() {
        return dateAjout;
    }

    public Environnement dateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
        return this;
    }

    public void setDateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Boolean isEstActif() {
        return estActif;
    }

    public Environnement estActif(Boolean estActif) {
        this.estActif = estActif;
        return this;
    }

    public void setEstActif(Boolean estActif) {
        this.estActif = estActif;
    }

    public Set<Campagne> getCampagnes() {
        return campagnes;
    }

    public Environnement campagnes(Set<Campagne> campagnes) {
        this.campagnes = campagnes;
        return this;
    }

    public Environnement addCampagne(Campagne campagne) {
        this.campagnes.add(campagne);
        campagne.setEnvironnement(this);
        return this;
    }

    public Environnement removeCampagne(Campagne campagne) {
        this.campagnes.remove(campagne);
        campagne.setEnvironnement(null);
        return this;
    }

    public void setCampagnes(Set<Campagne> campagnes) {
        this.campagnes = campagnes;
    }

    public Set<Deploiement> getDeploiements() {
        return deploiements;
    }

    public Environnement deploiements(Set<Deploiement> deploiements) {
        this.deploiements = deploiements;
        return this;
    }

    public Environnement addDeploiement(Deploiement deploiement) {
        this.deploiements.add(deploiement);
        deploiement.setEnvironnement(this);
        return this;
    }

    public Environnement removeDeploiement(Deploiement deploiement) {
        this.deploiements.remove(deploiement);
        deploiement.setEnvironnement(null);
        return this;
    }

    public void setDeploiements(Set<Deploiement> deploiements) {
        this.deploiements = deploiements;
    }

    public Application getApplication() {
        return application;
    }

    public Environnement application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public TypeEnv getTypeEnv() {
        return typeEnv;
    }

    public Environnement typeEnv(TypeEnv typeEnv) {
        this.typeEnv = typeEnv;
        return this;
    }

    public void setTypeEnv(TypeEnv typeEnv) {
        this.typeEnv = typeEnv;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Environnement)) {
            return false;
        }
        return id != null && id.equals(((Environnement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Environnement{" +
            "id=" + getId() +
            ", dateAjout='" + getDateAjout() + "'" +
            ", estActif='" + isEstActif() + "'" +
            "}";
    }
}
