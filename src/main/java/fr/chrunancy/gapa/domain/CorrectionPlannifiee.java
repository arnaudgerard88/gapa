package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A CorrectionPlannifiee.
 */
@Entity
@Table(name = "correction_plannifiee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CorrectionPlannifiee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Instant date;

    @ManyToOne
    @JsonIgnoreProperties("correctionPlannifiees")
    private Version version;

    @ManyToOne
    @JsonIgnoreProperties("correctionPlannifiees")
    private Anomalie anomalie;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public CorrectionPlannifiee date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Version getVersion() {
        return version;
    }

    public CorrectionPlannifiee version(Version version) {
        this.version = version;
        return this;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Anomalie getAnomalie() {
        return anomalie;
    }

    public CorrectionPlannifiee anomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
        return this;
    }

    public void setAnomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CorrectionPlannifiee)) {
            return false;
        }
        return id != null && id.equals(((CorrectionPlannifiee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CorrectionPlannifiee{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
