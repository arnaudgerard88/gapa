package fr.chrunancy.gapa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cahier.
 */
@Entity
@Table(name = "cahier")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cahier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date_maj")
    private Instant dateMaj;

    @OneToOne
    @JoinColumn(unique = true)
    private Domaine domaine;

    @OneToMany(mappedBy = "cahier")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Chapitre> chapitres = new HashSet<>();

    @OneToMany(mappedBy = "cahier")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ResponsabiliteCahier> responsabiliteCahiers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Cahier nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Instant getDateMaj() {
        return dateMaj;
    }

    public Cahier dateMaj(Instant dateMaj) {
        this.dateMaj = dateMaj;
        return this;
    }

    public void setDateMaj(Instant dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Domaine getDomaine() {
        return domaine;
    }

    public Cahier domaine(Domaine domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(Domaine domaine) {
        this.domaine = domaine;
    }

    public Set<Chapitre> getChapitres() {
        return chapitres;
    }

    public Cahier chapitres(Set<Chapitre> chapitres) {
        this.chapitres = chapitres;
        return this;
    }

    public Cahier addChapitre(Chapitre chapitre) {
        this.chapitres.add(chapitre);
        chapitre.setCahier(this);
        return this;
    }

    public Cahier removeChapitre(Chapitre chapitre) {
        this.chapitres.remove(chapitre);
        chapitre.setCahier(null);
        return this;
    }

    public void setChapitres(Set<Chapitre> chapitres) {
        this.chapitres = chapitres;
    }

    public Set<ResponsabiliteCahier> getResponsabiliteCahiers() {
        return responsabiliteCahiers;
    }

    public Cahier responsabiliteCahiers(Set<ResponsabiliteCahier> responsabiliteCahiers) {
        this.responsabiliteCahiers = responsabiliteCahiers;
        return this;
    }

    public Cahier addResponsabiliteCahier(ResponsabiliteCahier responsabiliteCahier) {
        this.responsabiliteCahiers.add(responsabiliteCahier);
        responsabiliteCahier.setCahier(this);
        return this;
    }

    public Cahier removeResponsabiliteCahier(ResponsabiliteCahier responsabiliteCahier) {
        this.responsabiliteCahiers.remove(responsabiliteCahier);
        responsabiliteCahier.setCahier(null);
        return this;
    }

    public void setResponsabiliteCahiers(Set<ResponsabiliteCahier> responsabiliteCahiers) {
        this.responsabiliteCahiers = responsabiliteCahiers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cahier)) {
            return false;
        }
        return id != null && id.equals(((Cahier) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cahier{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", dateMaj='" + getDateMaj() + "'" +
            "}";
    }
}
