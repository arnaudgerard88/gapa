package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A ResponsabiliteCahier.
 */
@Entity
@Table(name = "responsabilite_cahier")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ResponsabiliteCahier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Instant date;

    @ManyToOne
    @JsonIgnoreProperties("responsabiliteCahiers")
    private Utilisateur utilisateur;

    @ManyToOne
    @JsonIgnoreProperties("responsabiliteCahiers")
    private Cahier cahier;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public ResponsabiliteCahier date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public ResponsabiliteCahier utilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
        return this;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Cahier getCahier() {
        return cahier;
    }

    public ResponsabiliteCahier cahier(Cahier cahier) {
        this.cahier = cahier;
        return this;
    }

    public void setCahier(Cahier cahier) {
        this.cahier = cahier;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResponsabiliteCahier)) {
            return false;
        }
        return id != null && id.equals(((ResponsabiliteCahier) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ResponsabiliteCahier{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
