package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Fonctionnalite.
 */
@Entity
@Table(name = "fonctionnalite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Fonctionnalite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "description")
    private String description;

    @Column(name = "est_active")
    private Integer estActive;

    @OneToMany(mappedBy = "fonctionnalite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SuiviCampagne> suiviCampagnes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("fonctionnalites")
    private Chapitre chapitre;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Fonctionnalite nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public Fonctionnalite description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEstActive() {
        return estActive;
    }

    public Fonctionnalite estActive(Integer estActive) {
        this.estActive = estActive;
        return this;
    }

    public void setEstActive(Integer estActive) {
        this.estActive = estActive;
    }

    public Set<SuiviCampagne> getSuiviCampagnes() {
        return suiviCampagnes;
    }

    public Fonctionnalite suiviCampagnes(Set<SuiviCampagne> suiviCampagnes) {
        this.suiviCampagnes = suiviCampagnes;
        return this;
    }

    public Fonctionnalite addSuiviCampagne(SuiviCampagne suiviCampagne) {
        this.suiviCampagnes.add(suiviCampagne);
        suiviCampagne.setFonctionnalite(this);
        return this;
    }

    public Fonctionnalite removeSuiviCampagne(SuiviCampagne suiviCampagne) {
        this.suiviCampagnes.remove(suiviCampagne);
        suiviCampagne.setFonctionnalite(null);
        return this;
    }

    public void setSuiviCampagnes(Set<SuiviCampagne> suiviCampagnes) {
        this.suiviCampagnes = suiviCampagnes;
    }

    public Chapitre getChapitre() {
        return chapitre;
    }

    public Fonctionnalite chapitre(Chapitre chapitre) {
        this.chapitre = chapitre;
        return this;
    }

    public void setChapitre(Chapitre chapitre) {
        this.chapitre = chapitre;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fonctionnalite)) {
            return false;
        }
        return id != null && id.equals(((Fonctionnalite) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Fonctionnalite{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            ", estActive=" + getEstActive() +
            "}";
    }
}
