package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Anomalie.
 */
@Entity
@Table(name = "anomalie")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Anomalie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "num_interne")
    private String numInterne;

    @Column(name = "num_externe")
    private String numExterne;

    @Column(name = "titre")
    private String titre;

    @Column(name = "description")
    private String description;

    @Column(name = "statut")
    private String statut;

    @Column(name = "criticite")
    private String criticite;

    @Column(name = "date_declaration")
    private Instant dateDeclaration;

    @Column(name = "date_cloture")
    private Instant dateCloture;

    @OneToMany(mappedBy = "anomalie")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Anomalie> anomalies = new HashSet<>();

    @OneToMany(mappedBy = "anomalie")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CorrectionPlannifiee> correctionPlannifiees = new HashSet<>();

    @OneToMany(mappedBy = "anomalie")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AnomalieEtablissement> anomalieEtablissements = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("anomalies")
    private Utilisateur utilisateur;

    @ManyToOne
    @JsonIgnoreProperties("anomalies")
    private Version version;

    @ManyToOne
    @JsonIgnoreProperties("anomalies")
    private Anomalie anomalie;

    @ManyToOne
    @JsonIgnoreProperties("anomalies")
    private SuiviCampagne suiviCampagne;

    @ManyToOne
    @JsonIgnoreProperties("anomalies")
    private Domaine domaine;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumInterne() {
        return numInterne;
    }

    public Anomalie numInterne(String numInterne) {
        this.numInterne = numInterne;
        return this;
    }

    public void setNumInterne(String numInterne) {
        this.numInterne = numInterne;
    }

    public String getNumExterne() {
        return numExterne;
    }

    public Anomalie numExterne(String numExterne) {
        this.numExterne = numExterne;
        return this;
    }

    public void setNumExterne(String numExterne) {
        this.numExterne = numExterne;
    }

    public String getTitre() {
        return titre;
    }

    public Anomalie titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public Anomalie description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatut() {
        return statut;
    }

    public Anomalie statut(String statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getCriticite() {
        return criticite;
    }

    public Anomalie criticite(String criticite) {
        this.criticite = criticite;
        return this;
    }

    public void setCriticite(String criticite) {
        this.criticite = criticite;
    }

    public Instant getDateDeclaration() {
        return dateDeclaration;
    }

    public Anomalie dateDeclaration(Instant dateDeclaration) {
        this.dateDeclaration = dateDeclaration;
        return this;
    }

    public void setDateDeclaration(Instant dateDeclaration) {
        this.dateDeclaration = dateDeclaration;
    }

    public Instant getDateCloture() {
        return dateCloture;
    }

    public Anomalie dateCloture(Instant dateCloture) {
        this.dateCloture = dateCloture;
        return this;
    }

    public void setDateCloture(Instant dateCloture) {
        this.dateCloture = dateCloture;
    }

    public Set<Anomalie> getAnomalies() {
        return anomalies;
    }

    public Anomalie anomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
        return this;
    }

    public Anomalie addAnomalie(Anomalie anomalie) {
        this.anomalies.add(anomalie);
        anomalie.setAnomalie(this);
        return this;
    }

    public Anomalie removeAnomalie(Anomalie anomalie) {
        this.anomalies.remove(anomalie);
        anomalie.setAnomalie(null);
        return this;
    }

    public void setAnomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
    }

    public Set<CorrectionPlannifiee> getCorrectionPlannifiees() {
        return correctionPlannifiees;
    }

    public Anomalie correctionPlannifiees(Set<CorrectionPlannifiee> correctionPlannifiees) {
        this.correctionPlannifiees = correctionPlannifiees;
        return this;
    }

    public Anomalie addCorrectionPlannifiee(CorrectionPlannifiee correctionPlannifiee) {
        this.correctionPlannifiees.add(correctionPlannifiee);
        correctionPlannifiee.setAnomalie(this);
        return this;
    }

    public Anomalie removeCorrectionPlannifiee(CorrectionPlannifiee correctionPlannifiee) {
        this.correctionPlannifiees.remove(correctionPlannifiee);
        correctionPlannifiee.setAnomalie(null);
        return this;
    }

    public void setCorrectionPlannifiees(Set<CorrectionPlannifiee> correctionPlannifiees) {
        this.correctionPlannifiees = correctionPlannifiees;
    }

    public Set<AnomalieEtablissement> getAnomalieEtablissements() {
        return anomalieEtablissements;
    }

    public Anomalie anomalieEtablissements(Set<AnomalieEtablissement> anomalieEtablissements) {
        this.anomalieEtablissements = anomalieEtablissements;
        return this;
    }

    public Anomalie addAnomalieEtablissement(AnomalieEtablissement anomalieEtablissement) {
        this.anomalieEtablissements.add(anomalieEtablissement);
        anomalieEtablissement.setAnomalie(this);
        return this;
    }

    public Anomalie removeAnomalieEtablissement(AnomalieEtablissement anomalieEtablissement) {
        this.anomalieEtablissements.remove(anomalieEtablissement);
        anomalieEtablissement.setAnomalie(null);
        return this;
    }

    public void setAnomalieEtablissements(Set<AnomalieEtablissement> anomalieEtablissements) {
        this.anomalieEtablissements = anomalieEtablissements;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public Anomalie utilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
        return this;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Version getVersion() {
        return version;
    }

    public Anomalie version(Version version) {
        this.version = version;
        return this;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Anomalie getAnomalie() {
        return anomalie;
    }

    public Anomalie anomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
        return this;
    }

    public void setAnomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
    }

    public SuiviCampagne getSuiviCampagne() {
        return suiviCampagne;
    }

    public Anomalie suiviCampagne(SuiviCampagne suiviCampagne) {
        this.suiviCampagne = suiviCampagne;
        return this;
    }

    public void setSuiviCampagne(SuiviCampagne suiviCampagne) {
        this.suiviCampagne = suiviCampagne;
    }

    public Domaine getDomaine() {
        return domaine;
    }

    public Anomalie domaine(Domaine domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(Domaine domaine) {
        this.domaine = domaine;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Anomalie)) {
            return false;
        }
        return id != null && id.equals(((Anomalie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Anomalie{" +
            "id=" + getId() +
            ", numInterne='" + getNumInterne() + "'" +
            ", numExterne='" + getNumExterne() + "'" +
            ", titre='" + getTitre() + "'" +
            ", description='" + getDescription() + "'" +
            ", statut='" + getStatut() + "'" +
            ", criticite='" + getCriticite() + "'" +
            ", dateDeclaration='" + getDateDeclaration() + "'" +
            ", dateCloture='" + getDateCloture() + "'" +
            "}";
    }
}
