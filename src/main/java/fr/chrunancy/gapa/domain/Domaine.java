package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Domaine.
 */
@Entity
@Table(name = "domaine")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Domaine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @OneToMany(mappedBy = "domaine")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Anomalie> anomalies = new HashSet<>();

    @OneToOne(mappedBy = "domaine")
    @JsonIgnore
    private Cahier cahier;

    @ManyToOne
    @JsonIgnoreProperties("domaines")
    private Application application;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Domaine nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Anomalie> getAnomalies() {
        return anomalies;
    }

    public Domaine anomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
        return this;
    }

    public Domaine addAnomalie(Anomalie anomalie) {
        this.anomalies.add(anomalie);
        anomalie.setDomaine(this);
        return this;
    }

    public Domaine removeAnomalie(Anomalie anomalie) {
        this.anomalies.remove(anomalie);
        anomalie.setDomaine(null);
        return this;
    }

    public void setAnomalies(Set<Anomalie> anomalies) {
        this.anomalies = anomalies;
    }

    public Cahier getCahier() {
        return cahier;
    }

    public Domaine cahier(Cahier cahier) {
        this.cahier = cahier;
        return this;
    }

    public void setCahier(Cahier cahier) {
        this.cahier = cahier;
    }

    public Application getApplication() {
        return application;
    }

    public Domaine application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Domaine)) {
            return false;
        }
        return id != null && id.equals(((Domaine) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Domaine{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
