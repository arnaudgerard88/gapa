package fr.chrunancy.gapa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A AnomalieEtablissement.
 */
@Entity
@Table(name = "anomalie_etablissement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AnomalieEtablissement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Instant date;

    @ManyToOne
    @JsonIgnoreProperties("anomalieEtablissements")
    private Anomalie anomalie;

    @ManyToOne
    @JsonIgnoreProperties("anomalieEtablissements")
    private Etablissement etablissement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public AnomalieEtablissement date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Anomalie getAnomalie() {
        return anomalie;
    }

    public AnomalieEtablissement anomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
        return this;
    }

    public void setAnomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public AnomalieEtablissement etablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
        return this;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnomalieEtablissement)) {
            return false;
        }
        return id != null && id.equals(((AnomalieEtablissement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AnomalieEtablissement{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
