package fr.chrunancy.gapa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Application.
 */
@Entity
@Table(name = "application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "email")
    private String email;

    @Column(name = "date_ajout")
    private Instant dateAjout;

    @OneToMany(
        mappedBy = "application"
    )
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Domaine> domaines = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY,
        mappedBy = "application",
        orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Version> versions = new HashSet<>();

    @OneToMany(mappedBy = "application")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Utilisateur> utilisateurs = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER,
        mappedBy = "application",
        orphanRemoval = true
    )
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Environnement> environnements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Application nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public Application email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getDateAjout() {
        return dateAjout;
    }

    public Application dateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
        return this;
    }

    public void setDateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Set<Domaine> getDomaines() {
        return domaines;
    }

    public Application domaines(Set<Domaine> domaines) {
        this.domaines = domaines;
        return this;
    }

    public Application addDomaine(Domaine domaine) {
        this.domaines.add(domaine);
        domaine.setApplication(this);
        return this;
    }

    public Application removeDomaine(Domaine domaine) {
        this.domaines.remove(domaine);
        domaine.setApplication(null);
        return this;
    }

    public void setDomaines(Set<Domaine> domaines) {
        this.domaines = domaines;
    }

    public Set<Version> getVersions() {
        return versions;
    }

    public Application versions(Set<Version> versions) {
        this.versions = versions;
        return this;
    }

    public Application addVersion(Version version) {
        this.versions.add(version);
        version.setApplication(this);
        return this;
    }

    public Application removeVersion(Version version) {
        this.versions.remove(version);
        version.setApplication(null);
        return this;
    }

    public void setVersions(Set<Version> versions) {
        this.versions = versions;
    }

    public Set<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public Application utilisateurs(Set<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
        return this;
    }

    public Application addUtilisateur(Utilisateur utilisateur) {
        this.utilisateurs.add(utilisateur);
        utilisateur.setApplication(this);
        return this;
    }

    public Application removeUtilisateur(Utilisateur utilisateur) {
        this.utilisateurs.remove(utilisateur);
        utilisateur.setApplication(null);
        return this;
    }

    public void setUtilisateurs(Set<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public Set<Environnement> getEnvironnements() {
        return environnements;
    }

    public Application environnements(Set<Environnement> environnements) {
        this.environnements = environnements;
        return this;
    }

    public Application addEnvironnement(Environnement environnement) {
        this.environnements.add(environnement);
        environnement.setApplication(this);
        return this;
    }

    public Application removeEnvironnement(Environnement environnement) {
        this.environnements.remove(environnement);
        environnement.setApplication(null);
        return this;
    }

    public void setEnvironnements(Set<Environnement> environnements) {
        this.environnements = environnements;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Application)) {
            return false;
        }
        return id != null && id.equals(((Application) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Application{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", email='" + getEmail() + "'" +
            ", dateAjout='" + getDateAjout() + "'" +
            "}";
    }
}
