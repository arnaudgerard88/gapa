package fr.chrunancy.gapa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Etablissement.
 */
@Entity
@Table(name = "etablissement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Etablissement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom_etab")
    private String nomEtab;

    @OneToMany(mappedBy = "etablissement")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AnomalieEtablissement> anomalieEtablissements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomEtab() {
        return nomEtab;
    }

    public Etablissement nomEtab(String nomEtab) {
        this.nomEtab = nomEtab;
        return this;
    }

    public void setNomEtab(String nomEtab) {
        this.nomEtab = nomEtab;
    }

    public Set<AnomalieEtablissement> getAnomalieEtablissements() {
        return anomalieEtablissements;
    }

    public Etablissement anomalieEtablissements(Set<AnomalieEtablissement> anomalieEtablissements) {
        this.anomalieEtablissements = anomalieEtablissements;
        return this;
    }

    public Etablissement addAnomalieEtablissement(AnomalieEtablissement anomalieEtablissement) {
        this.anomalieEtablissements.add(anomalieEtablissement);
        anomalieEtablissement.setEtablissement(this);
        return this;
    }

    public Etablissement removeAnomalieEtablissement(AnomalieEtablissement anomalieEtablissement) {
        this.anomalieEtablissements.remove(anomalieEtablissement);
        anomalieEtablissement.setEtablissement(null);
        return this;
    }

    public void setAnomalieEtablissements(Set<AnomalieEtablissement> anomalieEtablissements) {
        this.anomalieEtablissements = anomalieEtablissements;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Etablissement)) {
            return false;
        }
        return id != null && id.equals(((Etablissement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Etablissement{" +
            "id=" + getId() +
            ", nomEtab='" + getNomEtab() + "'" +
            "}";
    }
}
