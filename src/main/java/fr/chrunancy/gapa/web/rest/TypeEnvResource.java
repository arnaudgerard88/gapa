package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.TypeEnv;
import fr.chrunancy.gapa.repository.TypeEnvRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.TypeEnv}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TypeEnvResource {

    private final Logger log = LoggerFactory.getLogger(TypeEnvResource.class);

    private static final String ENTITY_NAME = "typeEnv";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeEnvRepository typeEnvRepository;

    public TypeEnvResource(TypeEnvRepository typeEnvRepository) {
        this.typeEnvRepository = typeEnvRepository;
    }

    /**
     * {@code POST  /type-envs} : Create a new typeEnv.
     *
     * @param typeEnv the typeEnv to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeEnv, or with status {@code 400 (Bad Request)} if the typeEnv has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-envs")
    public ResponseEntity<TypeEnv> createTypeEnv(@RequestBody TypeEnv typeEnv) throws URISyntaxException {
        log.debug("REST request to save TypeEnv : {}", typeEnv);
        if (typeEnv.getId() != null) {
            throw new BadRequestAlertException("A new typeEnv cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeEnv result = typeEnvRepository.save(typeEnv);
        return ResponseEntity.created(new URI("/api/type-envs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-envs} : Updates an existing typeEnv.
     *
     * @param typeEnv the typeEnv to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeEnv,
     * or with status {@code 400 (Bad Request)} if the typeEnv is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeEnv couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-envs")
    public ResponseEntity<TypeEnv> updateTypeEnv(@RequestBody TypeEnv typeEnv) throws URISyntaxException {
        log.debug("REST request to update TypeEnv : {}", typeEnv);
        if (typeEnv.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeEnv result = typeEnvRepository.save(typeEnv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, typeEnv.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-envs} : get all the typeEnvs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeEnvs in body.
     */
    @GetMapping("/type-envs")
    public ResponseEntity<List<TypeEnv>> getAllTypeEnvs(Pageable pageable) {
        log.debug("REST request to get a page of TypeEnvs");
        Page<TypeEnv> page = typeEnvRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /type-envs/:id} : get the "id" typeEnv.
     *
     * @param id the id of the typeEnv to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeEnv, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-envs/{id}")
    public ResponseEntity<TypeEnv> getTypeEnv(@PathVariable Long id) {
        log.debug("REST request to get TypeEnv : {}", id);
        Optional<TypeEnv> typeEnv = typeEnvRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(typeEnv);
    }

    /**
     * {@code DELETE  /type-envs/:id} : delete the "id" typeEnv.
     *
     * @param id the id of the typeEnv to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-envs/{id}")
    public ResponseEntity<Void> deleteTypeEnv(@PathVariable Long id) {
        log.debug("REST request to delete TypeEnv : {}", id);
        typeEnvRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
