package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.CorrectionPlannifiee;
import fr.chrunancy.gapa.repository.CorrectionPlannifieeRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.CorrectionPlannifiee}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CorrectionPlannifieeResource {

    private final Logger log = LoggerFactory.getLogger(CorrectionPlannifieeResource.class);

    private static final String ENTITY_NAME = "correctionPlannifiee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CorrectionPlannifieeRepository correctionPlannifieeRepository;

    public CorrectionPlannifieeResource(CorrectionPlannifieeRepository correctionPlannifieeRepository) {
        this.correctionPlannifieeRepository = correctionPlannifieeRepository;
    }

    /**
     * {@code POST  /correction-plannifiees} : Create a new correctionPlannifiee.
     *
     * @param correctionPlannifiee the correctionPlannifiee to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new correctionPlannifiee, or with status {@code 400 (Bad Request)} if the correctionPlannifiee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/correction-plannifiees")
    public ResponseEntity<CorrectionPlannifiee> createCorrectionPlannifiee(@RequestBody CorrectionPlannifiee correctionPlannifiee) throws URISyntaxException {
        log.debug("REST request to save CorrectionPlannifiee : {}", correctionPlannifiee);
        if (correctionPlannifiee.getId() != null) {
            throw new BadRequestAlertException("A new correctionPlannifiee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CorrectionPlannifiee result = correctionPlannifieeRepository.save(correctionPlannifiee);
        return ResponseEntity.created(new URI("/api/correction-plannifiees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /correction-plannifiees} : Updates an existing correctionPlannifiee.
     *
     * @param correctionPlannifiee the correctionPlannifiee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated correctionPlannifiee,
     * or with status {@code 400 (Bad Request)} if the correctionPlannifiee is not valid,
     * or with status {@code 500 (Internal Server Error)} if the correctionPlannifiee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/correction-plannifiees")
    public ResponseEntity<CorrectionPlannifiee> updateCorrectionPlannifiee(@RequestBody CorrectionPlannifiee correctionPlannifiee) throws URISyntaxException {
        log.debug("REST request to update CorrectionPlannifiee : {}", correctionPlannifiee);
        if (correctionPlannifiee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CorrectionPlannifiee result = correctionPlannifieeRepository.save(correctionPlannifiee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, correctionPlannifiee.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /correction-plannifiees} : get all the correctionPlannifiees.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of correctionPlannifiees in body.
     */
    @GetMapping("/correction-plannifiees")
    public ResponseEntity<List<CorrectionPlannifiee>> getAllCorrectionPlannifiees(Pageable pageable) {
        log.debug("REST request to get a page of CorrectionPlannifiees");
        Page<CorrectionPlannifiee> page = correctionPlannifieeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /correction-plannifiees/:id} : get the "id" correctionPlannifiee.
     *
     * @param id the id of the correctionPlannifiee to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the correctionPlannifiee, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/correction-plannifiees/{id}")
    public ResponseEntity<CorrectionPlannifiee> getCorrectionPlannifiee(@PathVariable Long id) {
        log.debug("REST request to get CorrectionPlannifiee : {}", id);
        Optional<CorrectionPlannifiee> correctionPlannifiee = correctionPlannifieeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(correctionPlannifiee);
    }

    /**
     * {@code DELETE  /correction-plannifiees/:id} : delete the "id" correctionPlannifiee.
     *
     * @param id the id of the correctionPlannifiee to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/correction-plannifiees/{id}")
    public ResponseEntity<Void> deleteCorrectionPlannifiee(@PathVariable Long id) {
        log.debug("REST request to delete CorrectionPlannifiee : {}", id);
        correctionPlannifieeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
