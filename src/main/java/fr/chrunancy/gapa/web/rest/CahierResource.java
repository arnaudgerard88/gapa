package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.Cahier;
import fr.chrunancy.gapa.repository.CahierRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.Cahier}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CahierResource {

    private final Logger log = LoggerFactory.getLogger(CahierResource.class);

    private static final String ENTITY_NAME = "cahier";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CahierRepository cahierRepository;

    public CahierResource(CahierRepository cahierRepository) {
        this.cahierRepository = cahierRepository;
    }

    /**
     * {@code POST  /cahiers} : Create a new cahier.
     *
     * @param cahier the cahier to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cahier, or with status {@code 400 (Bad Request)} if the cahier has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cahiers")
    public ResponseEntity<Cahier> createCahier(@RequestBody Cahier cahier) throws URISyntaxException {
        log.debug("REST request to save Cahier : {}", cahier);
        if (cahier.getId() != null) {
            throw new BadRequestAlertException("A new cahier cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cahier result = cahierRepository.save(cahier);
        return ResponseEntity.created(new URI("/api/cahiers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cahiers} : Updates an existing cahier.
     *
     * @param cahier the cahier to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cahier,
     * or with status {@code 400 (Bad Request)} if the cahier is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cahier couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cahiers")
    public ResponseEntity<Cahier> updateCahier(@RequestBody Cahier cahier) throws URISyntaxException {
        log.debug("REST request to update Cahier : {}", cahier);
        if (cahier.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Cahier result = cahierRepository.save(cahier);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cahier.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cahiers} : get all the cahiers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cahiers in body.
     */
    @GetMapping("/cahiers")
    public ResponseEntity<List<Cahier>> getAllCahiers(Pageable pageable) {
        log.debug("REST request to get a page of Cahiers");
        Page<Cahier> page = cahierRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cahiers/:id} : get the "id" cahier.
     *
     * @param id the id of the cahier to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cahier, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cahiers/{id}")
    public ResponseEntity<Cahier> getCahier(@PathVariable Long id) {
        log.debug("REST request to get Cahier : {}", id);
        Optional<Cahier> cahier = cahierRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cahier);
    }

    /**
     * {@code DELETE  /cahiers/:id} : delete the "id" cahier.
     *
     * @param id the id of the cahier to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cahiers/{id}")
    public ResponseEntity<Void> deleteCahier(@PathVariable Long id) {
        log.debug("REST request to delete Cahier : {}", id);
        cahierRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
