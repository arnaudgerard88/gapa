package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.Chapitre;
import fr.chrunancy.gapa.repository.ChapitreRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.Chapitre}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ChapitreResource {

    private final Logger log = LoggerFactory.getLogger(ChapitreResource.class);

    private static final String ENTITY_NAME = "chapitre";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChapitreRepository chapitreRepository;

    public ChapitreResource(ChapitreRepository chapitreRepository) {
        this.chapitreRepository = chapitreRepository;
    }

    /**
     * {@code POST  /chapitres} : Create a new chapitre.
     *
     * @param chapitre the chapitre to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chapitre, or with status {@code 400 (Bad Request)} if the chapitre has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chapitres")
    public ResponseEntity<Chapitre> createChapitre(@RequestBody Chapitre chapitre) throws URISyntaxException {
        log.debug("REST request to save Chapitre : {}", chapitre);
        if (chapitre.getId() != null) {
            throw new BadRequestAlertException("A new chapitre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Chapitre result = chapitreRepository.save(chapitre);
        return ResponseEntity.created(new URI("/api/chapitres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chapitres} : Updates an existing chapitre.
     *
     * @param chapitre the chapitre to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chapitre,
     * or with status {@code 400 (Bad Request)} if the chapitre is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chapitre couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chapitres")
    public ResponseEntity<Chapitre> updateChapitre(@RequestBody Chapitre chapitre) throws URISyntaxException {
        log.debug("REST request to update Chapitre : {}", chapitre);
        if (chapitre.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Chapitre result = chapitreRepository.save(chapitre);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chapitre.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chapitres} : get all the chapitres.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chapitres in body.
     */
    @GetMapping("/chapitres")
    public ResponseEntity<List<Chapitre>> getAllChapitres(Pageable pageable) {
        log.debug("REST request to get a page of Chapitres");
        Page<Chapitre> page = chapitreRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chapitres/:id} : get the "id" chapitre.
     *
     * @param id the id of the chapitre to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chapitre, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chapitres/{id}")
    public ResponseEntity<Chapitre> getChapitre(@PathVariable Long id) {
        log.debug("REST request to get Chapitre : {}", id);
        Optional<Chapitre> chapitre = chapitreRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chapitre);
    }

    /**
     * {@code DELETE  /chapitres/:id} : delete the "id" chapitre.
     *
     * @param id the id of the chapitre to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chapitres/{id}")
    public ResponseEntity<Void> deleteChapitre(@PathVariable Long id) {
        log.debug("REST request to delete Chapitre : {}", id);
        chapitreRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
