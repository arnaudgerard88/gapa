package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.ResponsabiliteCahier;
import fr.chrunancy.gapa.repository.ResponsabiliteCahierRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.ResponsabiliteCahier}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ResponsabiliteCahierResource {

    private final Logger log = LoggerFactory.getLogger(ResponsabiliteCahierResource.class);

    private static final String ENTITY_NAME = "responsabiliteCahier";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResponsabiliteCahierRepository responsabiliteCahierRepository;

    public ResponsabiliteCahierResource(ResponsabiliteCahierRepository responsabiliteCahierRepository) {
        this.responsabiliteCahierRepository = responsabiliteCahierRepository;
    }

    /**
     * {@code POST  /responsabilite-cahiers} : Create a new responsabiliteCahier.
     *
     * @param responsabiliteCahier the responsabiliteCahier to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new responsabiliteCahier, or with status {@code 400 (Bad Request)} if the responsabiliteCahier has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/responsabilite-cahiers")
    public ResponseEntity<ResponsabiliteCahier> createResponsabiliteCahier(@RequestBody ResponsabiliteCahier responsabiliteCahier) throws URISyntaxException {
        log.debug("REST request to save ResponsabiliteCahier : {}", responsabiliteCahier);
        if (responsabiliteCahier.getId() != null) {
            throw new BadRequestAlertException("A new responsabiliteCahier cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResponsabiliteCahier result = responsabiliteCahierRepository.save(responsabiliteCahier);
        return ResponseEntity.created(new URI("/api/responsabilite-cahiers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /responsabilite-cahiers} : Updates an existing responsabiliteCahier.
     *
     * @param responsabiliteCahier the responsabiliteCahier to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated responsabiliteCahier,
     * or with status {@code 400 (Bad Request)} if the responsabiliteCahier is not valid,
     * or with status {@code 500 (Internal Server Error)} if the responsabiliteCahier couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/responsabilite-cahiers")
    public ResponseEntity<ResponsabiliteCahier> updateResponsabiliteCahier(@RequestBody ResponsabiliteCahier responsabiliteCahier) throws URISyntaxException {
        log.debug("REST request to update ResponsabiliteCahier : {}", responsabiliteCahier);
        if (responsabiliteCahier.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResponsabiliteCahier result = responsabiliteCahierRepository.save(responsabiliteCahier);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, responsabiliteCahier.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /responsabilite-cahiers} : get all the responsabiliteCahiers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of responsabiliteCahiers in body.
     */
    @GetMapping("/responsabilite-cahiers")
    public ResponseEntity<List<ResponsabiliteCahier>> getAllResponsabiliteCahiers(Pageable pageable) {
        log.debug("REST request to get a page of ResponsabiliteCahiers");
        Page<ResponsabiliteCahier> page = responsabiliteCahierRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /responsabilite-cahiers/:id} : get the "id" responsabiliteCahier.
     *
     * @param id the id of the responsabiliteCahier to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the responsabiliteCahier, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/responsabilite-cahiers/{id}")
    public ResponseEntity<ResponsabiliteCahier> getResponsabiliteCahier(@PathVariable Long id) {
        log.debug("REST request to get ResponsabiliteCahier : {}", id);
        Optional<ResponsabiliteCahier> responsabiliteCahier = responsabiliteCahierRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(responsabiliteCahier);
    }

    /**
     * {@code DELETE  /responsabilite-cahiers/:id} : delete the "id" responsabiliteCahier.
     *
     * @param id the id of the responsabiliteCahier to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/responsabilite-cahiers/{id}")
    public ResponseEntity<Void> deleteResponsabiliteCahier(@PathVariable Long id) {
        log.debug("REST request to delete ResponsabiliteCahier : {}", id);
        responsabiliteCahierRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
