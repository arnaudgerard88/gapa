package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.SuiviCampagne;
import fr.chrunancy.gapa.repository.SuiviCampagneRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.SuiviCampagne}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SuiviCampagneResource {

    private final Logger log = LoggerFactory.getLogger(SuiviCampagneResource.class);

    private static final String ENTITY_NAME = "suiviCampagne";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuiviCampagneRepository suiviCampagneRepository;

    public SuiviCampagneResource(SuiviCampagneRepository suiviCampagneRepository) {
        this.suiviCampagneRepository = suiviCampagneRepository;
    }

    /**
     * {@code POST  /suivi-campagnes} : Create a new suiviCampagne.
     *
     * @param suiviCampagne the suiviCampagne to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new suiviCampagne, or with status {@code 400 (Bad Request)} if the suiviCampagne has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/suivi-campagnes")
    public ResponseEntity<SuiviCampagne> createSuiviCampagne(@RequestBody SuiviCampagne suiviCampagne) throws URISyntaxException {
        log.debug("REST request to save SuiviCampagne : {}", suiviCampagne);
        if (suiviCampagne.getId() != null) {
            throw new BadRequestAlertException("A new suiviCampagne cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SuiviCampagne result = suiviCampagneRepository.save(suiviCampagne);
        return ResponseEntity.created(new URI("/api/suivi-campagnes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /suivi-campagnes} : Updates an existing suiviCampagne.
     *
     * @param suiviCampagne the suiviCampagne to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated suiviCampagne,
     * or with status {@code 400 (Bad Request)} if the suiviCampagne is not valid,
     * or with status {@code 500 (Internal Server Error)} if the suiviCampagne couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/suivi-campagnes")
    public ResponseEntity<SuiviCampagne> updateSuiviCampagne(@RequestBody SuiviCampagne suiviCampagne) throws URISyntaxException {
        log.debug("REST request to update SuiviCampagne : {}", suiviCampagne);
        if (suiviCampagne.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SuiviCampagne result = suiviCampagneRepository.save(suiviCampagne);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, suiviCampagne.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /suivi-campagnes} : get all the suiviCampagnes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of suiviCampagnes in body.
     */
    @GetMapping("/suivi-campagnes")
    public ResponseEntity<List<SuiviCampagne>> getAllSuiviCampagnes(Pageable pageable) {
        log.debug("REST request to get a page of SuiviCampagnes");
        Page<SuiviCampagne> page = suiviCampagneRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /suivi-campagnes/:id} : get the "id" suiviCampagne.
     *
     * @param id the id of the suiviCampagne to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the suiviCampagne, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/suivi-campagnes/{id}")
    public ResponseEntity<SuiviCampagne> getSuiviCampagne(@PathVariable Long id) {
        log.debug("REST request to get SuiviCampagne : {}", id);
        Optional<SuiviCampagne> suiviCampagne = suiviCampagneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(suiviCampagne);
    }

    /**
     * {@code DELETE  /suivi-campagnes/:id} : delete the "id" suiviCampagne.
     *
     * @param id the id of the suiviCampagne to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/suivi-campagnes/{id}")
    public ResponseEntity<Void> deleteSuiviCampagne(@PathVariable Long id) {
        log.debug("REST request to delete SuiviCampagne : {}", id);
        suiviCampagneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
