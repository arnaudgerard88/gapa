package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.Deploiement;
import fr.chrunancy.gapa.domain.Environnement;
import fr.chrunancy.gapa.domain.Version;
import fr.chrunancy.gapa.repository.DeploiementRepository;
import fr.chrunancy.gapa.repository.EnvironnementRepository;
import fr.chrunancy.gapa.repository.VersionRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.Deploiement}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DeploiementResource {

    private final Logger log = LoggerFactory.getLogger(DeploiementResource.class);

    private static final String ENTITY_NAME = "deploiement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeploiementRepository deploiementRepository;
    private final VersionRepository versionRepository;
    private final EnvironnementRepository environnementRepository;

    public DeploiementResource(DeploiementRepository deploiementRepository, VersionRepository versionRepository,  EnvironnementRepository environnementRepository) {
        this.deploiementRepository = deploiementRepository;
        this.versionRepository = versionRepository;
        this.environnementRepository = environnementRepository;
    }

    /**
     * {@code POST  /deploiements} : Create a new deploiement.
     *
     * @param deploiement the deploiement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deploiement, or with status {@code 400 (Bad Request)} if the deploiement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/deploiements")
    public ResponseEntity<Deploiement> createDeploiement(@RequestBody Deploiement deploiement) throws URISyntaxException {
        log.debug("REST request to save Deploiement : {}", deploiement);
        if (deploiement.getId() != null) {
            throw new BadRequestAlertException("A new deploiement cannot already have an ID", ENTITY_NAME, "idexists");
        }

        deploiement.getVersion().addDeploiement(deploiement);
        this.versionRepository.save(deploiement.getVersion());

        deploiement.getEnvironnement().addDeploiement(deploiement);
        this.environnementRepository.save(deploiement.getEnvironnement());

        Deploiement result = deploiementRepository.save(deploiement);
        return ResponseEntity.created(new URI("/api/deploiements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /deploiements} : Updates an existing deploiement.
     *
     * @param deploiement the deploiement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deploiement,
     * or with status {@code 400 (Bad Request)} if the deploiement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deploiement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/deploiements")
    public ResponseEntity<Deploiement> updateDeploiement(@RequestBody Deploiement deploiement) throws URISyntaxException {
        log.debug("REST request to update Deploiement : {}", deploiement);
        if (deploiement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        //Synchro
        Deploiement oldDeploiement = deploiementRepository.getOne(deploiement.getId());
        if(oldDeploiement.getVersion().getId() != deploiement.getVersion().getId()){
            Version oldVersion = oldDeploiement.getVersion();
            oldVersion.removeDeploiement(oldDeploiement);
            this.versionRepository.save(oldVersion);

            deploiement.getVersion().addDeploiement(deploiement);
            this.versionRepository.save(deploiement.getVersion());
        }
        if(oldDeploiement.getEnvironnement().getId() != deploiement.getEnvironnement().getId()){
            Environnement oldEnvironnement = oldDeploiement.getEnvironnement();
            oldEnvironnement.removeDeploiement(oldDeploiement);
            this.environnementRepository.save(oldEnvironnement);

            deploiement.getEnvironnement().addDeploiement(deploiement);
            this.environnementRepository.save(deploiement.getEnvironnement());
        }

        Deploiement result = deploiementRepository.save(deploiement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deploiement.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /deploiements} : get all the deploiements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deploiements in body.
     */
    @GetMapping("/deploiements")
    public ResponseEntity<List<Deploiement>> getAllDeploiements(Pageable pageable) {
        log.debug("REST request to get a page of Deploiements");
        Page<Deploiement> page = deploiementRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /deploiements/:id} : get the "id" deploiement.
     *
     * @param id the id of the deploiement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deploiement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/deploiements/{id}")
    public ResponseEntity<Deploiement> getDeploiement(@PathVariable Long id) {
        log.debug("REST request to get Deploiement : {}", id);
        Optional<Deploiement> deploiement = deploiementRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(deploiement);
    }

    /**
     * {@code DELETE  /deploiements/:id} : delete the "id" deploiement.
     *
     * @param id the id of the deploiement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/deploiements/{id}")
    public ResponseEntity<Void> deleteDeploiement(@PathVariable Long id) {
        log.debug("REST request to delete Deploiement : {}", id);
        if(this.deploiementRepository.findById(id).isPresent()){
            Deploiement deploiement = this.deploiementRepository.findById(id).get();
            Version version = deploiement.getVersion();
            Environnement environnement = deploiement.getEnvironnement();
            version.removeDeploiement(deploiement);
            environnement.removeDeploiement(deploiement);
            this.versionRepository.save(version);
            this.environnementRepository.save(environnement);
        }
        deploiementRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
