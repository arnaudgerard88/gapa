/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.chrunancy.gapa.web.rest.vm;
