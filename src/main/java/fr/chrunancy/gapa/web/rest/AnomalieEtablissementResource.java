package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.AnomalieEtablissement;
import fr.chrunancy.gapa.repository.AnomalieEtablissementRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.AnomalieEtablissement}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AnomalieEtablissementResource {

    private final Logger log = LoggerFactory.getLogger(AnomalieEtablissementResource.class);

    private static final String ENTITY_NAME = "anomalieEtablissement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AnomalieEtablissementRepository anomalieEtablissementRepository;

    public AnomalieEtablissementResource(AnomalieEtablissementRepository anomalieEtablissementRepository) {
        this.anomalieEtablissementRepository = anomalieEtablissementRepository;
    }

    /**
     * {@code POST  /anomalie-etablissements} : Create a new anomalieEtablissement.
     *
     * @param anomalieEtablissement the anomalieEtablissement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new anomalieEtablissement, or with status {@code 400 (Bad Request)} if the anomalieEtablissement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/anomalie-etablissements")
    public ResponseEntity<AnomalieEtablissement> createAnomalieEtablissement(@RequestBody AnomalieEtablissement anomalieEtablissement) throws URISyntaxException {
        log.debug("REST request to save AnomalieEtablissement : {}", anomalieEtablissement);
        if (anomalieEtablissement.getId() != null) {
            throw new BadRequestAlertException("A new anomalieEtablissement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnomalieEtablissement result = anomalieEtablissementRepository.save(anomalieEtablissement);
        return ResponseEntity.created(new URI("/api/anomalie-etablissements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /anomalie-etablissements} : Updates an existing anomalieEtablissement.
     *
     * @param anomalieEtablissement the anomalieEtablissement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated anomalieEtablissement,
     * or with status {@code 400 (Bad Request)} if the anomalieEtablissement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the anomalieEtablissement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/anomalie-etablissements")
    public ResponseEntity<AnomalieEtablissement> updateAnomalieEtablissement(@RequestBody AnomalieEtablissement anomalieEtablissement) throws URISyntaxException {
        log.debug("REST request to update AnomalieEtablissement : {}", anomalieEtablissement);
        if (anomalieEtablissement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnomalieEtablissement result = anomalieEtablissementRepository.save(anomalieEtablissement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, anomalieEtablissement.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /anomalie-etablissements} : get all the anomalieEtablissements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of anomalieEtablissements in body.
     */
    @GetMapping("/anomalie-etablissements")
    public ResponseEntity<List<AnomalieEtablissement>> getAllAnomalieEtablissements(Pageable pageable) {
        log.debug("REST request to get a page of AnomalieEtablissements");
        Page<AnomalieEtablissement> page = anomalieEtablissementRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /anomalie-etablissements/:id} : get the "id" anomalieEtablissement.
     *
     * @param id the id of the anomalieEtablissement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the anomalieEtablissement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/anomalie-etablissements/{id}")
    public ResponseEntity<AnomalieEtablissement> getAnomalieEtablissement(@PathVariable Long id) {
        log.debug("REST request to get AnomalieEtablissement : {}", id);
        Optional<AnomalieEtablissement> anomalieEtablissement = anomalieEtablissementRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(anomalieEtablissement);
    }

    /**
     * {@code DELETE  /anomalie-etablissements/:id} : delete the "id" anomalieEtablissement.
     *
     * @param id the id of the anomalieEtablissement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/anomalie-etablissements/{id}")
    public ResponseEntity<Void> deleteAnomalieEtablissement(@PathVariable Long id) {
        log.debug("REST request to delete AnomalieEtablissement : {}", id);
        anomalieEtablissementRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
