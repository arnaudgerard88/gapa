package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.domain.Application;
import fr.chrunancy.gapa.domain.Environnement;
import fr.chrunancy.gapa.domain.TypeEnv;
import fr.chrunancy.gapa.repository.ApplicationRepository;
import fr.chrunancy.gapa.repository.EnvironnementRepository;
import fr.chrunancy.gapa.repository.TypeEnvRepository;
import fr.chrunancy.gapa.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.chrunancy.gapa.domain.Environnement}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EnvironnementResource {

    private final Logger log = LoggerFactory.getLogger(EnvironnementResource.class);

    private static final String ENTITY_NAME = "environnement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EnvironnementRepository environnementRepository;
    private final ApplicationRepository applicationRepository;
    private final TypeEnvRepository typeEnvRepository;

    public EnvironnementResource(EnvironnementRepository environnementRepository, ApplicationRepository applicationRepository, TypeEnvRepository typeEnvRepository) {
        this.environnementRepository = environnementRepository;
        this.applicationRepository = applicationRepository;
        this.typeEnvRepository = typeEnvRepository;
    }

    /**
     * {@code POST  /environnements} : Create a new environnement.
     *
     * @param environnement the environnement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new environnement, or with status {@code 400 (Bad Request)} if the environnement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/environnements")
    public ResponseEntity<Environnement> createEnvironnement(@RequestBody Environnement environnement) throws URISyntaxException {
        log.debug("REST request to save Environnement : {}", environnement);
        if (environnement.getId() != null) {
            throw new BadRequestAlertException("A new environnement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        //on save
        Environnement result = environnementRepository.save(environnement);

        //on synchronise
        environnement.getApplication().addEnvironnement(environnement);
        environnement.getTypeEnv().addEnvironnement(environnement);
        this.applicationRepository.save(environnement.getApplication());
        this.typeEnvRepository.save(environnement.getTypeEnv());

        return ResponseEntity.created(new URI("/api/environnements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /environnements} : Updates an existing environnement.
     *
     * @param environnement the environnement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated environnement,
     * or with status {@code 400 (Bad Request)} if the environnement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the environnement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/environnements")
    public ResponseEntity<Environnement> updateEnvironnement(@RequestBody Environnement environnement) throws URISyntaxException {
        log.debug("REST request to update Environnement : {}", environnement);
        if (environnement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        //on sycnhonise les deux objets
        Environnement oldEnvironnement = environnementRepository.getOne(environnement.getId());

        if(oldEnvironnement.getApplication().getId() != environnement.getApplication().getId()){
            oldEnvironnement.getApplication().removeEnvironnement(oldEnvironnement);
            environnement.getApplication().addEnvironnement(environnement);
            this.applicationRepository.save(environnement.getApplication());
        }
        if(oldEnvironnement.getTypeEnv().getId() != environnement.getTypeEnv().getId()){
            oldEnvironnement.getTypeEnv().removeEnvironnement(oldEnvironnement);
            environnement.getTypeEnv().addEnvironnement(environnement);
            this.typeEnvRepository.save(environnement.getTypeEnv());
        }
        //on save l objet
        Environnement result = environnementRepository.save(environnement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, environnement.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /environnements} : get all the environnements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of environnements in body.
     */
    @GetMapping("/environnements")
    public ResponseEntity<List<Environnement>> getAllEnvironnements(Pageable pageable) {
        log.debug("REST request to get a page of Environnements");
        Page<Environnement> page = environnementRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /environnements/:id} : get the "id" environnement.
     *
     * @param id the id of the environnement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the environnement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/environnements/{id}")
    public ResponseEntity<Environnement> getEnvironnement(@PathVariable Long id) {
        log.debug("REST request to get Environnement : {}", id);
        Optional<Environnement> environnement = environnementRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(environnement);
    }

    /**
     * {@code DELETE  /environnements/:id} : delete the "id" environnement.
     *
     * @param id the id of the environnement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/environnements/{id}")
    public ResponseEntity<Void> deleteEnvironnement(@PathVariable Long id) {
        log.debug("REST request to delete Environnement : {}", id);

        //synchro
        if(environnementRepository.findById(id).isPresent()){
            Environnement environnement = environnementRepository.findById(id).get();
            TypeEnv typeEnv = environnement.getTypeEnv();
            typeEnv.removeEnvironnement(environnement);
            Application application = environnement.getApplication();
            application.removeEnvironnement(environnement);
            typeEnvRepository.save(typeEnv);
            applicationRepository.save(application);
        }

        environnementRepository.deleteById(id);

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
