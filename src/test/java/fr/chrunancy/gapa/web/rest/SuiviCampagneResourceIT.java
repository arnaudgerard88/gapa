package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.SuiviCampagne;
import fr.chrunancy.gapa.repository.SuiviCampagneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SuiviCampagneResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class SuiviCampagneResourceIT {

    private static final String DEFAULT_ETAT = "AAAAAAAAAA";
    private static final String UPDATED_ETAT = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private SuiviCampagneRepository suiviCampagneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSuiviCampagneMockMvc;

    private SuiviCampagne suiviCampagne;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuiviCampagne createEntity(EntityManager em) {
        SuiviCampagne suiviCampagne = new SuiviCampagne()
            .etat(DEFAULT_ETAT)
            .date(DEFAULT_DATE);
        return suiviCampagne;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuiviCampagne createUpdatedEntity(EntityManager em) {
        SuiviCampagne suiviCampagne = new SuiviCampagne()
            .etat(UPDATED_ETAT)
            .date(UPDATED_DATE);
        return suiviCampagne;
    }

    @BeforeEach
    public void initTest() {
        suiviCampagne = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuiviCampagne() throws Exception {
        int databaseSizeBeforeCreate = suiviCampagneRepository.findAll().size();

        // Create the SuiviCampagne
        restSuiviCampagneMockMvc.perform(post("/api/suivi-campagnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suiviCampagne)))
            .andExpect(status().isCreated());

        // Validate the SuiviCampagne in the database
        List<SuiviCampagne> suiviCampagneList = suiviCampagneRepository.findAll();
        assertThat(suiviCampagneList).hasSize(databaseSizeBeforeCreate + 1);
        SuiviCampagne testSuiviCampagne = suiviCampagneList.get(suiviCampagneList.size() - 1);
        assertThat(testSuiviCampagne.getEtat()).isEqualTo(DEFAULT_ETAT);
        assertThat(testSuiviCampagne.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createSuiviCampagneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = suiviCampagneRepository.findAll().size();

        // Create the SuiviCampagne with an existing ID
        suiviCampagne.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuiviCampagneMockMvc.perform(post("/api/suivi-campagnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suiviCampagne)))
            .andExpect(status().isBadRequest());

        // Validate the SuiviCampagne in the database
        List<SuiviCampagne> suiviCampagneList = suiviCampagneRepository.findAll();
        assertThat(suiviCampagneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSuiviCampagnes() throws Exception {
        // Initialize the database
        suiviCampagneRepository.saveAndFlush(suiviCampagne);

        // Get all the suiviCampagneList
        restSuiviCampagneMockMvc.perform(get("/api/suivi-campagnes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(suiviCampagne.getId().intValue())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSuiviCampagne() throws Exception {
        // Initialize the database
        suiviCampagneRepository.saveAndFlush(suiviCampagne);

        // Get the suiviCampagne
        restSuiviCampagneMockMvc.perform(get("/api/suivi-campagnes/{id}", suiviCampagne.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(suiviCampagne.getId().intValue()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSuiviCampagne() throws Exception {
        // Get the suiviCampagne
        restSuiviCampagneMockMvc.perform(get("/api/suivi-campagnes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuiviCampagne() throws Exception {
        // Initialize the database
        suiviCampagneRepository.saveAndFlush(suiviCampagne);

        int databaseSizeBeforeUpdate = suiviCampagneRepository.findAll().size();

        // Update the suiviCampagne
        SuiviCampagne updatedSuiviCampagne = suiviCampagneRepository.findById(suiviCampagne.getId()).get();
        // Disconnect from session so that the updates on updatedSuiviCampagne are not directly saved in db
        em.detach(updatedSuiviCampagne);
        updatedSuiviCampagne
            .etat(UPDATED_ETAT)
            .date(UPDATED_DATE);

        restSuiviCampagneMockMvc.perform(put("/api/suivi-campagnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSuiviCampagne)))
            .andExpect(status().isOk());

        // Validate the SuiviCampagne in the database
        List<SuiviCampagne> suiviCampagneList = suiviCampagneRepository.findAll();
        assertThat(suiviCampagneList).hasSize(databaseSizeBeforeUpdate);
        SuiviCampagne testSuiviCampagne = suiviCampagneList.get(suiviCampagneList.size() - 1);
        assertThat(testSuiviCampagne.getEtat()).isEqualTo(UPDATED_ETAT);
        assertThat(testSuiviCampagne.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSuiviCampagne() throws Exception {
        int databaseSizeBeforeUpdate = suiviCampagneRepository.findAll().size();

        // Create the SuiviCampagne

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuiviCampagneMockMvc.perform(put("/api/suivi-campagnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(suiviCampagne)))
            .andExpect(status().isBadRequest());

        // Validate the SuiviCampagne in the database
        List<SuiviCampagne> suiviCampagneList = suiviCampagneRepository.findAll();
        assertThat(suiviCampagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuiviCampagne() throws Exception {
        // Initialize the database
        suiviCampagneRepository.saveAndFlush(suiviCampagne);

        int databaseSizeBeforeDelete = suiviCampagneRepository.findAll().size();

        // Delete the suiviCampagne
        restSuiviCampagneMockMvc.perform(delete("/api/suivi-campagnes/{id}", suiviCampagne.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SuiviCampagne> suiviCampagneList = suiviCampagneRepository.findAll();
        assertThat(suiviCampagneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
