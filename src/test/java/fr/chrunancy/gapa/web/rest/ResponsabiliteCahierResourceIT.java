package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.ResponsabiliteCahier;
import fr.chrunancy.gapa.repository.ResponsabiliteCahierRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResponsabiliteCahierResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ResponsabiliteCahierResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ResponsabiliteCahierRepository responsabiliteCahierRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restResponsabiliteCahierMockMvc;

    private ResponsabiliteCahier responsabiliteCahier;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResponsabiliteCahier createEntity(EntityManager em) {
        ResponsabiliteCahier responsabiliteCahier = new ResponsabiliteCahier()
            .date(DEFAULT_DATE);
        return responsabiliteCahier;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResponsabiliteCahier createUpdatedEntity(EntityManager em) {
        ResponsabiliteCahier responsabiliteCahier = new ResponsabiliteCahier()
            .date(UPDATED_DATE);
        return responsabiliteCahier;
    }

    @BeforeEach
    public void initTest() {
        responsabiliteCahier = createEntity(em);
    }

    @Test
    @Transactional
    public void createResponsabiliteCahier() throws Exception {
        int databaseSizeBeforeCreate = responsabiliteCahierRepository.findAll().size();

        // Create the ResponsabiliteCahier
        restResponsabiliteCahierMockMvc.perform(post("/api/responsabilite-cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(responsabiliteCahier)))
            .andExpect(status().isCreated());

        // Validate the ResponsabiliteCahier in the database
        List<ResponsabiliteCahier> responsabiliteCahierList = responsabiliteCahierRepository.findAll();
        assertThat(responsabiliteCahierList).hasSize(databaseSizeBeforeCreate + 1);
        ResponsabiliteCahier testResponsabiliteCahier = responsabiliteCahierList.get(responsabiliteCahierList.size() - 1);
        assertThat(testResponsabiliteCahier.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createResponsabiliteCahierWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = responsabiliteCahierRepository.findAll().size();

        // Create the ResponsabiliteCahier with an existing ID
        responsabiliteCahier.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResponsabiliteCahierMockMvc.perform(post("/api/responsabilite-cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(responsabiliteCahier)))
            .andExpect(status().isBadRequest());

        // Validate the ResponsabiliteCahier in the database
        List<ResponsabiliteCahier> responsabiliteCahierList = responsabiliteCahierRepository.findAll();
        assertThat(responsabiliteCahierList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllResponsabiliteCahiers() throws Exception {
        // Initialize the database
        responsabiliteCahierRepository.saveAndFlush(responsabiliteCahier);

        // Get all the responsabiliteCahierList
        restResponsabiliteCahierMockMvc.perform(get("/api/responsabilite-cahiers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(responsabiliteCahier.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getResponsabiliteCahier() throws Exception {
        // Initialize the database
        responsabiliteCahierRepository.saveAndFlush(responsabiliteCahier);

        // Get the responsabiliteCahier
        restResponsabiliteCahierMockMvc.perform(get("/api/responsabilite-cahiers/{id}", responsabiliteCahier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(responsabiliteCahier.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResponsabiliteCahier() throws Exception {
        // Get the responsabiliteCahier
        restResponsabiliteCahierMockMvc.perform(get("/api/responsabilite-cahiers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResponsabiliteCahier() throws Exception {
        // Initialize the database
        responsabiliteCahierRepository.saveAndFlush(responsabiliteCahier);

        int databaseSizeBeforeUpdate = responsabiliteCahierRepository.findAll().size();

        // Update the responsabiliteCahier
        ResponsabiliteCahier updatedResponsabiliteCahier = responsabiliteCahierRepository.findById(responsabiliteCahier.getId()).get();
        // Disconnect from session so that the updates on updatedResponsabiliteCahier are not directly saved in db
        em.detach(updatedResponsabiliteCahier);
        updatedResponsabiliteCahier
            .date(UPDATED_DATE);

        restResponsabiliteCahierMockMvc.perform(put("/api/responsabilite-cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedResponsabiliteCahier)))
            .andExpect(status().isOk());

        // Validate the ResponsabiliteCahier in the database
        List<ResponsabiliteCahier> responsabiliteCahierList = responsabiliteCahierRepository.findAll();
        assertThat(responsabiliteCahierList).hasSize(databaseSizeBeforeUpdate);
        ResponsabiliteCahier testResponsabiliteCahier = responsabiliteCahierList.get(responsabiliteCahierList.size() - 1);
        assertThat(testResponsabiliteCahier.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingResponsabiliteCahier() throws Exception {
        int databaseSizeBeforeUpdate = responsabiliteCahierRepository.findAll().size();

        // Create the ResponsabiliteCahier

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResponsabiliteCahierMockMvc.perform(put("/api/responsabilite-cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(responsabiliteCahier)))
            .andExpect(status().isBadRequest());

        // Validate the ResponsabiliteCahier in the database
        List<ResponsabiliteCahier> responsabiliteCahierList = responsabiliteCahierRepository.findAll();
        assertThat(responsabiliteCahierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResponsabiliteCahier() throws Exception {
        // Initialize the database
        responsabiliteCahierRepository.saveAndFlush(responsabiliteCahier);

        int databaseSizeBeforeDelete = responsabiliteCahierRepository.findAll().size();

        // Delete the responsabiliteCahier
        restResponsabiliteCahierMockMvc.perform(delete("/api/responsabilite-cahiers/{id}", responsabiliteCahier.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ResponsabiliteCahier> responsabiliteCahierList = responsabiliteCahierRepository.findAll();
        assertThat(responsabiliteCahierList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
