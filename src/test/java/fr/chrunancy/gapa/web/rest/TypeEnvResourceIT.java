package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.TypeEnv;
import fr.chrunancy.gapa.repository.TypeEnvRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypeEnvResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class TypeEnvResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    @Autowired
    private TypeEnvRepository typeEnvRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTypeEnvMockMvc;

    private TypeEnv typeEnv;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeEnv createEntity(EntityManager em) {
        TypeEnv typeEnv = new TypeEnv()
            .nom(DEFAULT_NOM);
        return typeEnv;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeEnv createUpdatedEntity(EntityManager em) {
        TypeEnv typeEnv = new TypeEnv()
            .nom(UPDATED_NOM);
        return typeEnv;
    }

    @BeforeEach
    public void initTest() {
        typeEnv = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeEnv() throws Exception {
        int databaseSizeBeforeCreate = typeEnvRepository.findAll().size();

        // Create the TypeEnv
        restTypeEnvMockMvc.perform(post("/api/type-envs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeEnv)))
            .andExpect(status().isCreated());

        // Validate the TypeEnv in the database
        List<TypeEnv> typeEnvList = typeEnvRepository.findAll();
        assertThat(typeEnvList).hasSize(databaseSizeBeforeCreate + 1);
        TypeEnv testTypeEnv = typeEnvList.get(typeEnvList.size() - 1);
        assertThat(testTypeEnv.getNom()).isEqualTo(DEFAULT_NOM);
    }

    @Test
    @Transactional
    public void createTypeEnvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeEnvRepository.findAll().size();

        // Create the TypeEnv with an existing ID
        typeEnv.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeEnvMockMvc.perform(post("/api/type-envs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeEnv)))
            .andExpect(status().isBadRequest());

        // Validate the TypeEnv in the database
        List<TypeEnv> typeEnvList = typeEnvRepository.findAll();
        assertThat(typeEnvList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTypeEnvs() throws Exception {
        // Initialize the database
        typeEnvRepository.saveAndFlush(typeEnv);

        // Get all the typeEnvList
        restTypeEnvMockMvc.perform(get("/api/type-envs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeEnv.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)));
    }
    
    @Test
    @Transactional
    public void getTypeEnv() throws Exception {
        // Initialize the database
        typeEnvRepository.saveAndFlush(typeEnv);

        // Get the typeEnv
        restTypeEnvMockMvc.perform(get("/api/type-envs/{id}", typeEnv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(typeEnv.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM));
    }

    @Test
    @Transactional
    public void getNonExistingTypeEnv() throws Exception {
        // Get the typeEnv
        restTypeEnvMockMvc.perform(get("/api/type-envs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeEnv() throws Exception {
        // Initialize the database
        typeEnvRepository.saveAndFlush(typeEnv);

        int databaseSizeBeforeUpdate = typeEnvRepository.findAll().size();

        // Update the typeEnv
        TypeEnv updatedTypeEnv = typeEnvRepository.findById(typeEnv.getId()).get();
        // Disconnect from session so that the updates on updatedTypeEnv are not directly saved in db
        em.detach(updatedTypeEnv);
        updatedTypeEnv
            .nom(UPDATED_NOM);

        restTypeEnvMockMvc.perform(put("/api/type-envs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypeEnv)))
            .andExpect(status().isOk());

        // Validate the TypeEnv in the database
        List<TypeEnv> typeEnvList = typeEnvRepository.findAll();
        assertThat(typeEnvList).hasSize(databaseSizeBeforeUpdate);
        TypeEnv testTypeEnv = typeEnvList.get(typeEnvList.size() - 1);
        assertThat(testTypeEnv.getNom()).isEqualTo(UPDATED_NOM);
    }

    @Test
    @Transactional
    public void updateNonExistingTypeEnv() throws Exception {
        int databaseSizeBeforeUpdate = typeEnvRepository.findAll().size();

        // Create the TypeEnv

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypeEnvMockMvc.perform(put("/api/type-envs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeEnv)))
            .andExpect(status().isBadRequest());

        // Validate the TypeEnv in the database
        List<TypeEnv> typeEnvList = typeEnvRepository.findAll();
        assertThat(typeEnvList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypeEnv() throws Exception {
        // Initialize the database
        typeEnvRepository.saveAndFlush(typeEnv);

        int databaseSizeBeforeDelete = typeEnvRepository.findAll().size();

        // Delete the typeEnv
        restTypeEnvMockMvc.perform(delete("/api/type-envs/{id}", typeEnv.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TypeEnv> typeEnvList = typeEnvRepository.findAll();
        assertThat(typeEnvList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
