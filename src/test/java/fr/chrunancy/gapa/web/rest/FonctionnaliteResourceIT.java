package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.Fonctionnalite;
import fr.chrunancy.gapa.repository.FonctionnaliteRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FonctionnaliteResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class FonctionnaliteResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_EST_ACTIVE = 1;
    private static final Integer UPDATED_EST_ACTIVE = 2;

    @Autowired
    private FonctionnaliteRepository fonctionnaliteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFonctionnaliteMockMvc;

    private Fonctionnalite fonctionnalite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fonctionnalite createEntity(EntityManager em) {
        Fonctionnalite fonctionnalite = new Fonctionnalite()
            .nom(DEFAULT_NOM)
            .description(DEFAULT_DESCRIPTION)
            .estActive(DEFAULT_EST_ACTIVE);
        return fonctionnalite;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fonctionnalite createUpdatedEntity(EntityManager em) {
        Fonctionnalite fonctionnalite = new Fonctionnalite()
            .nom(UPDATED_NOM)
            .description(UPDATED_DESCRIPTION)
            .estActive(UPDATED_EST_ACTIVE);
        return fonctionnalite;
    }

    @BeforeEach
    public void initTest() {
        fonctionnalite = createEntity(em);
    }

    @Test
    @Transactional
    public void createFonctionnalite() throws Exception {
        int databaseSizeBeforeCreate = fonctionnaliteRepository.findAll().size();

        // Create the Fonctionnalite
        restFonctionnaliteMockMvc.perform(post("/api/fonctionnalites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fonctionnalite)))
            .andExpect(status().isCreated());

        // Validate the Fonctionnalite in the database
        List<Fonctionnalite> fonctionnaliteList = fonctionnaliteRepository.findAll();
        assertThat(fonctionnaliteList).hasSize(databaseSizeBeforeCreate + 1);
        Fonctionnalite testFonctionnalite = fonctionnaliteList.get(fonctionnaliteList.size() - 1);
        assertThat(testFonctionnalite.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testFonctionnalite.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFonctionnalite.getEstActive()).isEqualTo(DEFAULT_EST_ACTIVE);
    }

    @Test
    @Transactional
    public void createFonctionnaliteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fonctionnaliteRepository.findAll().size();

        // Create the Fonctionnalite with an existing ID
        fonctionnalite.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFonctionnaliteMockMvc.perform(post("/api/fonctionnalites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fonctionnalite)))
            .andExpect(status().isBadRequest());

        // Validate the Fonctionnalite in the database
        List<Fonctionnalite> fonctionnaliteList = fonctionnaliteRepository.findAll();
        assertThat(fonctionnaliteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFonctionnalites() throws Exception {
        // Initialize the database
        fonctionnaliteRepository.saveAndFlush(fonctionnalite);

        // Get all the fonctionnaliteList
        restFonctionnaliteMockMvc.perform(get("/api/fonctionnalites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fonctionnalite.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].estActive").value(hasItem(DEFAULT_EST_ACTIVE)));
    }
    
    @Test
    @Transactional
    public void getFonctionnalite() throws Exception {
        // Initialize the database
        fonctionnaliteRepository.saveAndFlush(fonctionnalite);

        // Get the fonctionnalite
        restFonctionnaliteMockMvc.perform(get("/api/fonctionnalites/{id}", fonctionnalite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fonctionnalite.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.estActive").value(DEFAULT_EST_ACTIVE));
    }

    @Test
    @Transactional
    public void getNonExistingFonctionnalite() throws Exception {
        // Get the fonctionnalite
        restFonctionnaliteMockMvc.perform(get("/api/fonctionnalites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFonctionnalite() throws Exception {
        // Initialize the database
        fonctionnaliteRepository.saveAndFlush(fonctionnalite);

        int databaseSizeBeforeUpdate = fonctionnaliteRepository.findAll().size();

        // Update the fonctionnalite
        Fonctionnalite updatedFonctionnalite = fonctionnaliteRepository.findById(fonctionnalite.getId()).get();
        // Disconnect from session so that the updates on updatedFonctionnalite are not directly saved in db
        em.detach(updatedFonctionnalite);
        updatedFonctionnalite
            .nom(UPDATED_NOM)
            .description(UPDATED_DESCRIPTION)
            .estActive(UPDATED_EST_ACTIVE);

        restFonctionnaliteMockMvc.perform(put("/api/fonctionnalites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFonctionnalite)))
            .andExpect(status().isOk());

        // Validate the Fonctionnalite in the database
        List<Fonctionnalite> fonctionnaliteList = fonctionnaliteRepository.findAll();
        assertThat(fonctionnaliteList).hasSize(databaseSizeBeforeUpdate);
        Fonctionnalite testFonctionnalite = fonctionnaliteList.get(fonctionnaliteList.size() - 1);
        assertThat(testFonctionnalite.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testFonctionnalite.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFonctionnalite.getEstActive()).isEqualTo(UPDATED_EST_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingFonctionnalite() throws Exception {
        int databaseSizeBeforeUpdate = fonctionnaliteRepository.findAll().size();

        // Create the Fonctionnalite

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFonctionnaliteMockMvc.perform(put("/api/fonctionnalites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fonctionnalite)))
            .andExpect(status().isBadRequest());

        // Validate the Fonctionnalite in the database
        List<Fonctionnalite> fonctionnaliteList = fonctionnaliteRepository.findAll();
        assertThat(fonctionnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFonctionnalite() throws Exception {
        // Initialize the database
        fonctionnaliteRepository.saveAndFlush(fonctionnalite);

        int databaseSizeBeforeDelete = fonctionnaliteRepository.findAll().size();

        // Delete the fonctionnalite
        restFonctionnaliteMockMvc.perform(delete("/api/fonctionnalites/{id}", fonctionnalite.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fonctionnalite> fonctionnaliteList = fonctionnaliteRepository.findAll();
        assertThat(fonctionnaliteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
