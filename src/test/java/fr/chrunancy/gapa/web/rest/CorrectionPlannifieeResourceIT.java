package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.CorrectionPlannifiee;
import fr.chrunancy.gapa.repository.CorrectionPlannifieeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CorrectionPlannifieeResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CorrectionPlannifieeResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private CorrectionPlannifieeRepository correctionPlannifieeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCorrectionPlannifieeMockMvc;

    private CorrectionPlannifiee correctionPlannifiee;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CorrectionPlannifiee createEntity(EntityManager em) {
        CorrectionPlannifiee correctionPlannifiee = new CorrectionPlannifiee()
            .date(DEFAULT_DATE);
        return correctionPlannifiee;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CorrectionPlannifiee createUpdatedEntity(EntityManager em) {
        CorrectionPlannifiee correctionPlannifiee = new CorrectionPlannifiee()
            .date(UPDATED_DATE);
        return correctionPlannifiee;
    }

    @BeforeEach
    public void initTest() {
        correctionPlannifiee = createEntity(em);
    }

    @Test
    @Transactional
    public void createCorrectionPlannifiee() throws Exception {
        int databaseSizeBeforeCreate = correctionPlannifieeRepository.findAll().size();

        // Create the CorrectionPlannifiee
        restCorrectionPlannifieeMockMvc.perform(post("/api/correction-plannifiees")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(correctionPlannifiee)))
            .andExpect(status().isCreated());

        // Validate the CorrectionPlannifiee in the database
        List<CorrectionPlannifiee> correctionPlannifieeList = correctionPlannifieeRepository.findAll();
        assertThat(correctionPlannifieeList).hasSize(databaseSizeBeforeCreate + 1);
        CorrectionPlannifiee testCorrectionPlannifiee = correctionPlannifieeList.get(correctionPlannifieeList.size() - 1);
        assertThat(testCorrectionPlannifiee.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createCorrectionPlannifieeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = correctionPlannifieeRepository.findAll().size();

        // Create the CorrectionPlannifiee with an existing ID
        correctionPlannifiee.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCorrectionPlannifieeMockMvc.perform(post("/api/correction-plannifiees")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(correctionPlannifiee)))
            .andExpect(status().isBadRequest());

        // Validate the CorrectionPlannifiee in the database
        List<CorrectionPlannifiee> correctionPlannifieeList = correctionPlannifieeRepository.findAll();
        assertThat(correctionPlannifieeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCorrectionPlannifiees() throws Exception {
        // Initialize the database
        correctionPlannifieeRepository.saveAndFlush(correctionPlannifiee);

        // Get all the correctionPlannifieeList
        restCorrectionPlannifieeMockMvc.perform(get("/api/correction-plannifiees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(correctionPlannifiee.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getCorrectionPlannifiee() throws Exception {
        // Initialize the database
        correctionPlannifieeRepository.saveAndFlush(correctionPlannifiee);

        // Get the correctionPlannifiee
        restCorrectionPlannifieeMockMvc.perform(get("/api/correction-plannifiees/{id}", correctionPlannifiee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(correctionPlannifiee.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCorrectionPlannifiee() throws Exception {
        // Get the correctionPlannifiee
        restCorrectionPlannifieeMockMvc.perform(get("/api/correction-plannifiees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCorrectionPlannifiee() throws Exception {
        // Initialize the database
        correctionPlannifieeRepository.saveAndFlush(correctionPlannifiee);

        int databaseSizeBeforeUpdate = correctionPlannifieeRepository.findAll().size();

        // Update the correctionPlannifiee
        CorrectionPlannifiee updatedCorrectionPlannifiee = correctionPlannifieeRepository.findById(correctionPlannifiee.getId()).get();
        // Disconnect from session so that the updates on updatedCorrectionPlannifiee are not directly saved in db
        em.detach(updatedCorrectionPlannifiee);
        updatedCorrectionPlannifiee
            .date(UPDATED_DATE);

        restCorrectionPlannifieeMockMvc.perform(put("/api/correction-plannifiees")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCorrectionPlannifiee)))
            .andExpect(status().isOk());

        // Validate the CorrectionPlannifiee in the database
        List<CorrectionPlannifiee> correctionPlannifieeList = correctionPlannifieeRepository.findAll();
        assertThat(correctionPlannifieeList).hasSize(databaseSizeBeforeUpdate);
        CorrectionPlannifiee testCorrectionPlannifiee = correctionPlannifieeList.get(correctionPlannifieeList.size() - 1);
        assertThat(testCorrectionPlannifiee.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingCorrectionPlannifiee() throws Exception {
        int databaseSizeBeforeUpdate = correctionPlannifieeRepository.findAll().size();

        // Create the CorrectionPlannifiee

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCorrectionPlannifieeMockMvc.perform(put("/api/correction-plannifiees")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(correctionPlannifiee)))
            .andExpect(status().isBadRequest());

        // Validate the CorrectionPlannifiee in the database
        List<CorrectionPlannifiee> correctionPlannifieeList = correctionPlannifieeRepository.findAll();
        assertThat(correctionPlannifieeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCorrectionPlannifiee() throws Exception {
        // Initialize the database
        correctionPlannifieeRepository.saveAndFlush(correctionPlannifiee);

        int databaseSizeBeforeDelete = correctionPlannifieeRepository.findAll().size();

        // Delete the correctionPlannifiee
        restCorrectionPlannifieeMockMvc.perform(delete("/api/correction-plannifiees/{id}", correctionPlannifiee.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CorrectionPlannifiee> correctionPlannifieeList = correctionPlannifieeRepository.findAll();
        assertThat(correctionPlannifieeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
