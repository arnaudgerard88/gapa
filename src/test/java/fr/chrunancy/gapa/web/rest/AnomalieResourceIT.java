package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.Anomalie;
import fr.chrunancy.gapa.repository.AnomalieRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AnomalieResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AnomalieResourceIT {

    private static final String DEFAULT_NUM_INTERNE = "AAAAAAAAAA";
    private static final String UPDATED_NUM_INTERNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_EXTERNE = "AAAAAAAAAA";
    private static final String UPDATED_NUM_EXTERNE = "BBBBBBBBBB";

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_STATUT = "AAAAAAAAAA";
    private static final String UPDATED_STATUT = "BBBBBBBBBB";

    private static final String DEFAULT_CRITICITE = "AAAAAAAAAA";
    private static final String UPDATED_CRITICITE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_DECLARATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_DECLARATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_CLOTURE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CLOTURE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private AnomalieRepository anomalieRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAnomalieMockMvc;

    private Anomalie anomalie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Anomalie createEntity(EntityManager em) {
        Anomalie anomalie = new Anomalie()
            .numInterne(DEFAULT_NUM_INTERNE)
            .numExterne(DEFAULT_NUM_EXTERNE)
            .titre(DEFAULT_TITRE)
            .description(DEFAULT_DESCRIPTION)
            .statut(DEFAULT_STATUT)
            .criticite(DEFAULT_CRITICITE)
            .dateDeclaration(DEFAULT_DATE_DECLARATION)
            .dateCloture(DEFAULT_DATE_CLOTURE);
        return anomalie;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Anomalie createUpdatedEntity(EntityManager em) {
        Anomalie anomalie = new Anomalie()
            .numInterne(UPDATED_NUM_INTERNE)
            .numExterne(UPDATED_NUM_EXTERNE)
            .titre(UPDATED_TITRE)
            .description(UPDATED_DESCRIPTION)
            .statut(UPDATED_STATUT)
            .criticite(UPDATED_CRITICITE)
            .dateDeclaration(UPDATED_DATE_DECLARATION)
            .dateCloture(UPDATED_DATE_CLOTURE);
        return anomalie;
    }

    @BeforeEach
    public void initTest() {
        anomalie = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnomalie() throws Exception {
        int databaseSizeBeforeCreate = anomalieRepository.findAll().size();

        // Create the Anomalie
        restAnomalieMockMvc.perform(post("/api/anomalies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalie)))
            .andExpect(status().isCreated());

        // Validate the Anomalie in the database
        List<Anomalie> anomalieList = anomalieRepository.findAll();
        assertThat(anomalieList).hasSize(databaseSizeBeforeCreate + 1);
        Anomalie testAnomalie = anomalieList.get(anomalieList.size() - 1);
        assertThat(testAnomalie.getNumInterne()).isEqualTo(DEFAULT_NUM_INTERNE);
        assertThat(testAnomalie.getNumExterne()).isEqualTo(DEFAULT_NUM_EXTERNE);
        assertThat(testAnomalie.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testAnomalie.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAnomalie.getStatut()).isEqualTo(DEFAULT_STATUT);
        assertThat(testAnomalie.getCriticite()).isEqualTo(DEFAULT_CRITICITE);
        assertThat(testAnomalie.getDateDeclaration()).isEqualTo(DEFAULT_DATE_DECLARATION);
        assertThat(testAnomalie.getDateCloture()).isEqualTo(DEFAULT_DATE_CLOTURE);
    }

    @Test
    @Transactional
    public void createAnomalieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anomalieRepository.findAll().size();

        // Create the Anomalie with an existing ID
        anomalie.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnomalieMockMvc.perform(post("/api/anomalies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalie)))
            .andExpect(status().isBadRequest());

        // Validate the Anomalie in the database
        List<Anomalie> anomalieList = anomalieRepository.findAll();
        assertThat(anomalieList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAnomalies() throws Exception {
        // Initialize the database
        anomalieRepository.saveAndFlush(anomalie);

        // Get all the anomalieList
        restAnomalieMockMvc.perform(get("/api/anomalies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anomalie.getId().intValue())))
            .andExpect(jsonPath("$.[*].numInterne").value(hasItem(DEFAULT_NUM_INTERNE)))
            .andExpect(jsonPath("$.[*].numExterne").value(hasItem(DEFAULT_NUM_EXTERNE)))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT)))
            .andExpect(jsonPath("$.[*].criticite").value(hasItem(DEFAULT_CRITICITE)))
            .andExpect(jsonPath("$.[*].dateDeclaration").value(hasItem(DEFAULT_DATE_DECLARATION.toString())))
            .andExpect(jsonPath("$.[*].dateCloture").value(hasItem(DEFAULT_DATE_CLOTURE.toString())));
    }
    
    @Test
    @Transactional
    public void getAnomalie() throws Exception {
        // Initialize the database
        anomalieRepository.saveAndFlush(anomalie);

        // Get the anomalie
        restAnomalieMockMvc.perform(get("/api/anomalies/{id}", anomalie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(anomalie.getId().intValue()))
            .andExpect(jsonPath("$.numInterne").value(DEFAULT_NUM_INTERNE))
            .andExpect(jsonPath("$.numExterne").value(DEFAULT_NUM_EXTERNE))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT))
            .andExpect(jsonPath("$.criticite").value(DEFAULT_CRITICITE))
            .andExpect(jsonPath("$.dateDeclaration").value(DEFAULT_DATE_DECLARATION.toString()))
            .andExpect(jsonPath("$.dateCloture").value(DEFAULT_DATE_CLOTURE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnomalie() throws Exception {
        // Get the anomalie
        restAnomalieMockMvc.perform(get("/api/anomalies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnomalie() throws Exception {
        // Initialize the database
        anomalieRepository.saveAndFlush(anomalie);

        int databaseSizeBeforeUpdate = anomalieRepository.findAll().size();

        // Update the anomalie
        Anomalie updatedAnomalie = anomalieRepository.findById(anomalie.getId()).get();
        // Disconnect from session so that the updates on updatedAnomalie are not directly saved in db
        em.detach(updatedAnomalie);
        updatedAnomalie
            .numInterne(UPDATED_NUM_INTERNE)
            .numExterne(UPDATED_NUM_EXTERNE)
            .titre(UPDATED_TITRE)
            .description(UPDATED_DESCRIPTION)
            .statut(UPDATED_STATUT)
            .criticite(UPDATED_CRITICITE)
            .dateDeclaration(UPDATED_DATE_DECLARATION)
            .dateCloture(UPDATED_DATE_CLOTURE);

        restAnomalieMockMvc.perform(put("/api/anomalies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnomalie)))
            .andExpect(status().isOk());

        // Validate the Anomalie in the database
        List<Anomalie> anomalieList = anomalieRepository.findAll();
        assertThat(anomalieList).hasSize(databaseSizeBeforeUpdate);
        Anomalie testAnomalie = anomalieList.get(anomalieList.size() - 1);
        assertThat(testAnomalie.getNumInterne()).isEqualTo(UPDATED_NUM_INTERNE);
        assertThat(testAnomalie.getNumExterne()).isEqualTo(UPDATED_NUM_EXTERNE);
        assertThat(testAnomalie.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testAnomalie.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAnomalie.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testAnomalie.getCriticite()).isEqualTo(UPDATED_CRITICITE);
        assertThat(testAnomalie.getDateDeclaration()).isEqualTo(UPDATED_DATE_DECLARATION);
        assertThat(testAnomalie.getDateCloture()).isEqualTo(UPDATED_DATE_CLOTURE);
    }

    @Test
    @Transactional
    public void updateNonExistingAnomalie() throws Exception {
        int databaseSizeBeforeUpdate = anomalieRepository.findAll().size();

        // Create the Anomalie

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnomalieMockMvc.perform(put("/api/anomalies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalie)))
            .andExpect(status().isBadRequest());

        // Validate the Anomalie in the database
        List<Anomalie> anomalieList = anomalieRepository.findAll();
        assertThat(anomalieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnomalie() throws Exception {
        // Initialize the database
        anomalieRepository.saveAndFlush(anomalie);

        int databaseSizeBeforeDelete = anomalieRepository.findAll().size();

        // Delete the anomalie
        restAnomalieMockMvc.perform(delete("/api/anomalies/{id}", anomalie.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Anomalie> anomalieList = anomalieRepository.findAll();
        assertThat(anomalieList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
