package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.Cahier;
import fr.chrunancy.gapa.repository.CahierRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CahierResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CahierResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_MAJ = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MAJ = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private CahierRepository cahierRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCahierMockMvc;

    private Cahier cahier;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cahier createEntity(EntityManager em) {
        Cahier cahier = new Cahier()
            .nom(DEFAULT_NOM)
            .dateMaj(DEFAULT_DATE_MAJ);
        return cahier;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cahier createUpdatedEntity(EntityManager em) {
        Cahier cahier = new Cahier()
            .nom(UPDATED_NOM)
            .dateMaj(UPDATED_DATE_MAJ);
        return cahier;
    }

    @BeforeEach
    public void initTest() {
        cahier = createEntity(em);
    }

    @Test
    @Transactional
    public void createCahier() throws Exception {
        int databaseSizeBeforeCreate = cahierRepository.findAll().size();

        // Create the Cahier
        restCahierMockMvc.perform(post("/api/cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cahier)))
            .andExpect(status().isCreated());

        // Validate the Cahier in the database
        List<Cahier> cahierList = cahierRepository.findAll();
        assertThat(cahierList).hasSize(databaseSizeBeforeCreate + 1);
        Cahier testCahier = cahierList.get(cahierList.size() - 1);
        assertThat(testCahier.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCahier.getDateMaj()).isEqualTo(DEFAULT_DATE_MAJ);
    }

    @Test
    @Transactional
    public void createCahierWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cahierRepository.findAll().size();

        // Create the Cahier with an existing ID
        cahier.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCahierMockMvc.perform(post("/api/cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cahier)))
            .andExpect(status().isBadRequest());

        // Validate the Cahier in the database
        List<Cahier> cahierList = cahierRepository.findAll();
        assertThat(cahierList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCahiers() throws Exception {
        // Initialize the database
        cahierRepository.saveAndFlush(cahier);

        // Get all the cahierList
        restCahierMockMvc.perform(get("/api/cahiers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cahier.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].dateMaj").value(hasItem(DEFAULT_DATE_MAJ.toString())));
    }
    
    @Test
    @Transactional
    public void getCahier() throws Exception {
        // Initialize the database
        cahierRepository.saveAndFlush(cahier);

        // Get the cahier
        restCahierMockMvc.perform(get("/api/cahiers/{id}", cahier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cahier.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.dateMaj").value(DEFAULT_DATE_MAJ.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCahier() throws Exception {
        // Get the cahier
        restCahierMockMvc.perform(get("/api/cahiers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCahier() throws Exception {
        // Initialize the database
        cahierRepository.saveAndFlush(cahier);

        int databaseSizeBeforeUpdate = cahierRepository.findAll().size();

        // Update the cahier
        Cahier updatedCahier = cahierRepository.findById(cahier.getId()).get();
        // Disconnect from session so that the updates on updatedCahier are not directly saved in db
        em.detach(updatedCahier);
        updatedCahier
            .nom(UPDATED_NOM)
            .dateMaj(UPDATED_DATE_MAJ);

        restCahierMockMvc.perform(put("/api/cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCahier)))
            .andExpect(status().isOk());

        // Validate the Cahier in the database
        List<Cahier> cahierList = cahierRepository.findAll();
        assertThat(cahierList).hasSize(databaseSizeBeforeUpdate);
        Cahier testCahier = cahierList.get(cahierList.size() - 1);
        assertThat(testCahier.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCahier.getDateMaj()).isEqualTo(UPDATED_DATE_MAJ);
    }

    @Test
    @Transactional
    public void updateNonExistingCahier() throws Exception {
        int databaseSizeBeforeUpdate = cahierRepository.findAll().size();

        // Create the Cahier

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCahierMockMvc.perform(put("/api/cahiers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cahier)))
            .andExpect(status().isBadRequest());

        // Validate the Cahier in the database
        List<Cahier> cahierList = cahierRepository.findAll();
        assertThat(cahierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCahier() throws Exception {
        // Initialize the database
        cahierRepository.saveAndFlush(cahier);

        int databaseSizeBeforeDelete = cahierRepository.findAll().size();

        // Delete the cahier
        restCahierMockMvc.perform(delete("/api/cahiers/{id}", cahier.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cahier> cahierList = cahierRepository.findAll();
        assertThat(cahierList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
