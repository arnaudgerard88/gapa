package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.AnomalieEtablissement;
import fr.chrunancy.gapa.repository.AnomalieEtablissementRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AnomalieEtablissementResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AnomalieEtablissementResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private AnomalieEtablissementRepository anomalieEtablissementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAnomalieEtablissementMockMvc;

    private AnomalieEtablissement anomalieEtablissement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnomalieEtablissement createEntity(EntityManager em) {
        AnomalieEtablissement anomalieEtablissement = new AnomalieEtablissement()
            .date(DEFAULT_DATE);
        return anomalieEtablissement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnomalieEtablissement createUpdatedEntity(EntityManager em) {
        AnomalieEtablissement anomalieEtablissement = new AnomalieEtablissement()
            .date(UPDATED_DATE);
        return anomalieEtablissement;
    }

    @BeforeEach
    public void initTest() {
        anomalieEtablissement = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnomalieEtablissement() throws Exception {
        int databaseSizeBeforeCreate = anomalieEtablissementRepository.findAll().size();

        // Create the AnomalieEtablissement
        restAnomalieEtablissementMockMvc.perform(post("/api/anomalie-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalieEtablissement)))
            .andExpect(status().isCreated());

        // Validate the AnomalieEtablissement in the database
        List<AnomalieEtablissement> anomalieEtablissementList = anomalieEtablissementRepository.findAll();
        assertThat(anomalieEtablissementList).hasSize(databaseSizeBeforeCreate + 1);
        AnomalieEtablissement testAnomalieEtablissement = anomalieEtablissementList.get(anomalieEtablissementList.size() - 1);
        assertThat(testAnomalieEtablissement.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createAnomalieEtablissementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anomalieEtablissementRepository.findAll().size();

        // Create the AnomalieEtablissement with an existing ID
        anomalieEtablissement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnomalieEtablissementMockMvc.perform(post("/api/anomalie-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalieEtablissement)))
            .andExpect(status().isBadRequest());

        // Validate the AnomalieEtablissement in the database
        List<AnomalieEtablissement> anomalieEtablissementList = anomalieEtablissementRepository.findAll();
        assertThat(anomalieEtablissementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAnomalieEtablissements() throws Exception {
        // Initialize the database
        anomalieEtablissementRepository.saveAndFlush(anomalieEtablissement);

        // Get all the anomalieEtablissementList
        restAnomalieEtablissementMockMvc.perform(get("/api/anomalie-etablissements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anomalieEtablissement.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getAnomalieEtablissement() throws Exception {
        // Initialize the database
        anomalieEtablissementRepository.saveAndFlush(anomalieEtablissement);

        // Get the anomalieEtablissement
        restAnomalieEtablissementMockMvc.perform(get("/api/anomalie-etablissements/{id}", anomalieEtablissement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(anomalieEtablissement.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnomalieEtablissement() throws Exception {
        // Get the anomalieEtablissement
        restAnomalieEtablissementMockMvc.perform(get("/api/anomalie-etablissements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnomalieEtablissement() throws Exception {
        // Initialize the database
        anomalieEtablissementRepository.saveAndFlush(anomalieEtablissement);

        int databaseSizeBeforeUpdate = anomalieEtablissementRepository.findAll().size();

        // Update the anomalieEtablissement
        AnomalieEtablissement updatedAnomalieEtablissement = anomalieEtablissementRepository.findById(anomalieEtablissement.getId()).get();
        // Disconnect from session so that the updates on updatedAnomalieEtablissement are not directly saved in db
        em.detach(updatedAnomalieEtablissement);
        updatedAnomalieEtablissement
            .date(UPDATED_DATE);

        restAnomalieEtablissementMockMvc.perform(put("/api/anomalie-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnomalieEtablissement)))
            .andExpect(status().isOk());

        // Validate the AnomalieEtablissement in the database
        List<AnomalieEtablissement> anomalieEtablissementList = anomalieEtablissementRepository.findAll();
        assertThat(anomalieEtablissementList).hasSize(databaseSizeBeforeUpdate);
        AnomalieEtablissement testAnomalieEtablissement = anomalieEtablissementList.get(anomalieEtablissementList.size() - 1);
        assertThat(testAnomalieEtablissement.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingAnomalieEtablissement() throws Exception {
        int databaseSizeBeforeUpdate = anomalieEtablissementRepository.findAll().size();

        // Create the AnomalieEtablissement

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnomalieEtablissementMockMvc.perform(put("/api/anomalie-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anomalieEtablissement)))
            .andExpect(status().isBadRequest());

        // Validate the AnomalieEtablissement in the database
        List<AnomalieEtablissement> anomalieEtablissementList = anomalieEtablissementRepository.findAll();
        assertThat(anomalieEtablissementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnomalieEtablissement() throws Exception {
        // Initialize the database
        anomalieEtablissementRepository.saveAndFlush(anomalieEtablissement);

        int databaseSizeBeforeDelete = anomalieEtablissementRepository.findAll().size();

        // Delete the anomalieEtablissement
        restAnomalieEtablissementMockMvc.perform(delete("/api/anomalie-etablissements/{id}", anomalieEtablissement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AnomalieEtablissement> anomalieEtablissementList = anomalieEtablissementRepository.findAll();
        assertThat(anomalieEtablissementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
