package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.Deploiement;
import fr.chrunancy.gapa.repository.DeploiementRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DeploiementResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DeploiementResourceIT {

    private static final Integer DEFAULT_EST_ACTIF = 1;
    private static final Integer UPDATED_EST_ACTIF = 2;

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DeploiementRepository deploiementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeploiementMockMvc;

    private Deploiement deploiement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deploiement createEntity(EntityManager em) {
        Deploiement deploiement = new Deploiement()
            .estActif(DEFAULT_EST_ACTIF)
            .date(DEFAULT_DATE);
        return deploiement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deploiement createUpdatedEntity(EntityManager em) {
        Deploiement deploiement = new Deploiement()
            .estActif(UPDATED_EST_ACTIF)
            .date(UPDATED_DATE);
        return deploiement;
    }

    @BeforeEach
    public void initTest() {
        deploiement = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeploiement() throws Exception {
        int databaseSizeBeforeCreate = deploiementRepository.findAll().size();

        // Create the Deploiement
        restDeploiementMockMvc.perform(post("/api/deploiements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deploiement)))
            .andExpect(status().isCreated());

        // Validate the Deploiement in the database
        List<Deploiement> deploiementList = deploiementRepository.findAll();
        assertThat(deploiementList).hasSize(databaseSizeBeforeCreate + 1);
        Deploiement testDeploiement = deploiementList.get(deploiementList.size() - 1);
        assertThat(testDeploiement.getEstActif()).isEqualTo(DEFAULT_EST_ACTIF);
        assertThat(testDeploiement.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createDeploiementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deploiementRepository.findAll().size();

        // Create the Deploiement with an existing ID
        deploiement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeploiementMockMvc.perform(post("/api/deploiements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deploiement)))
            .andExpect(status().isBadRequest());

        // Validate the Deploiement in the database
        List<Deploiement> deploiementList = deploiementRepository.findAll();
        assertThat(deploiementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDeploiements() throws Exception {
        // Initialize the database
        deploiementRepository.saveAndFlush(deploiement);

        // Get all the deploiementList
        restDeploiementMockMvc.perform(get("/api/deploiements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deploiement.getId().intValue())))
            .andExpect(jsonPath("$.[*].estActif").value(hasItem(DEFAULT_EST_ACTIF)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getDeploiement() throws Exception {
        // Initialize the database
        deploiementRepository.saveAndFlush(deploiement);

        // Get the deploiement
        restDeploiementMockMvc.perform(get("/api/deploiements/{id}", deploiement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deploiement.getId().intValue()))
            .andExpect(jsonPath("$.estActif").value(DEFAULT_EST_ACTIF))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDeploiement() throws Exception {
        // Get the deploiement
        restDeploiementMockMvc.perform(get("/api/deploiements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeploiement() throws Exception {
        // Initialize the database
        deploiementRepository.saveAndFlush(deploiement);

        int databaseSizeBeforeUpdate = deploiementRepository.findAll().size();

        // Update the deploiement
        Deploiement updatedDeploiement = deploiementRepository.findById(deploiement.getId()).get();
        // Disconnect from session so that the updates on updatedDeploiement are not directly saved in db
        em.detach(updatedDeploiement);
        updatedDeploiement
            .estActif(UPDATED_EST_ACTIF)
            .date(UPDATED_DATE);

        restDeploiementMockMvc.perform(put("/api/deploiements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeploiement)))
            .andExpect(status().isOk());

        // Validate the Deploiement in the database
        List<Deploiement> deploiementList = deploiementRepository.findAll();
        assertThat(deploiementList).hasSize(databaseSizeBeforeUpdate);
        Deploiement testDeploiement = deploiementList.get(deploiementList.size() - 1);
        assertThat(testDeploiement.getEstActif()).isEqualTo(UPDATED_EST_ACTIF);
        assertThat(testDeploiement.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingDeploiement() throws Exception {
        int databaseSizeBeforeUpdate = deploiementRepository.findAll().size();

        // Create the Deploiement

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeploiementMockMvc.perform(put("/api/deploiements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deploiement)))
            .andExpect(status().isBadRequest());

        // Validate the Deploiement in the database
        List<Deploiement> deploiementList = deploiementRepository.findAll();
        assertThat(deploiementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDeploiement() throws Exception {
        // Initialize the database
        deploiementRepository.saveAndFlush(deploiement);

        int databaseSizeBeforeDelete = deploiementRepository.findAll().size();

        // Delete the deploiement
        restDeploiementMockMvc.perform(delete("/api/deploiements/{id}", deploiement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Deploiement> deploiementList = deploiementRepository.findAll();
        assertThat(deploiementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
