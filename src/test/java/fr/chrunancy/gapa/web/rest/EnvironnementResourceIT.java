package fr.chrunancy.gapa.web.rest;

import fr.chrunancy.gapa.GapaApp;
import fr.chrunancy.gapa.domain.Environnement;
import fr.chrunancy.gapa.repository.EnvironnementRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EnvironnementResource} REST controller.
 */
@SpringBootTest(classes = GapaApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class EnvironnementResourceIT {

    private static final Instant DEFAULT_DATE_AJOUT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_AJOUT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_EST_ACTIF = false;
    private static final Boolean UPDATED_EST_ACTIF = true;

    @Autowired
    private EnvironnementRepository environnementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEnvironnementMockMvc;

    private Environnement environnement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Environnement createEntity(EntityManager em) {
        Environnement environnement = new Environnement()
            .dateAjout(DEFAULT_DATE_AJOUT)
            .estActif(DEFAULT_EST_ACTIF);
        return environnement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Environnement createUpdatedEntity(EntityManager em) {
        Environnement environnement = new Environnement()
            .dateAjout(UPDATED_DATE_AJOUT)
            .estActif(UPDATED_EST_ACTIF);
        return environnement;
    }

    @BeforeEach
    public void initTest() {
        environnement = createEntity(em);
    }

    @Test
    @Transactional
    public void createEnvironnement() throws Exception {
        int databaseSizeBeforeCreate = environnementRepository.findAll().size();

        // Create the Environnement
        restEnvironnementMockMvc.perform(post("/api/environnements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environnement)))
            .andExpect(status().isCreated());

        // Validate the Environnement in the database
        List<Environnement> environnementList = environnementRepository.findAll();
        assertThat(environnementList).hasSize(databaseSizeBeforeCreate + 1);
        Environnement testEnvironnement = environnementList.get(environnementList.size() - 1);
        assertThat(testEnvironnement.getDateAjout()).isEqualTo(DEFAULT_DATE_AJOUT);
        assertThat(testEnvironnement.isEstActif()).isEqualTo(DEFAULT_EST_ACTIF);
    }

    @Test
    @Transactional
    public void createEnvironnementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = environnementRepository.findAll().size();

        // Create the Environnement with an existing ID
        environnement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnvironnementMockMvc.perform(post("/api/environnements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environnement)))
            .andExpect(status().isBadRequest());

        // Validate the Environnement in the database
        List<Environnement> environnementList = environnementRepository.findAll();
        assertThat(environnementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEnvironnements() throws Exception {
        // Initialize the database
        environnementRepository.saveAndFlush(environnement);

        // Get all the environnementList
        restEnvironnementMockMvc.perform(get("/api/environnements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(environnement.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateAjout").value(hasItem(DEFAULT_DATE_AJOUT.toString())))
            .andExpect(jsonPath("$.[*].estActif").value(hasItem(DEFAULT_EST_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getEnvironnement() throws Exception {
        // Initialize the database
        environnementRepository.saveAndFlush(environnement);

        // Get the environnement
        restEnvironnementMockMvc.perform(get("/api/environnements/{id}", environnement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(environnement.getId().intValue()))
            .andExpect(jsonPath("$.dateAjout").value(DEFAULT_DATE_AJOUT.toString()))
            .andExpect(jsonPath("$.estActif").value(DEFAULT_EST_ACTIF.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEnvironnement() throws Exception {
        // Get the environnement
        restEnvironnementMockMvc.perform(get("/api/environnements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEnvironnement() throws Exception {
        // Initialize the database
        environnementRepository.saveAndFlush(environnement);

        int databaseSizeBeforeUpdate = environnementRepository.findAll().size();

        // Update the environnement
        Environnement updatedEnvironnement = environnementRepository.findById(environnement.getId()).get();
        // Disconnect from session so that the updates on updatedEnvironnement are not directly saved in db
        em.detach(updatedEnvironnement);
        updatedEnvironnement
            .dateAjout(UPDATED_DATE_AJOUT)
            .estActif(UPDATED_EST_ACTIF);

        restEnvironnementMockMvc.perform(put("/api/environnements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEnvironnement)))
            .andExpect(status().isOk());

        // Validate the Environnement in the database
        List<Environnement> environnementList = environnementRepository.findAll();
        assertThat(environnementList).hasSize(databaseSizeBeforeUpdate);
        Environnement testEnvironnement = environnementList.get(environnementList.size() - 1);
        assertThat(testEnvironnement.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testEnvironnement.isEstActif()).isEqualTo(UPDATED_EST_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingEnvironnement() throws Exception {
        int databaseSizeBeforeUpdate = environnementRepository.findAll().size();

        // Create the Environnement

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnvironnementMockMvc.perform(put("/api/environnements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environnement)))
            .andExpect(status().isBadRequest());

        // Validate the Environnement in the database
        List<Environnement> environnementList = environnementRepository.findAll();
        assertThat(environnementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEnvironnement() throws Exception {
        // Initialize the database
        environnementRepository.saveAndFlush(environnement);

        int databaseSizeBeforeDelete = environnementRepository.findAll().size();

        // Delete the environnement
        restEnvironnementMockMvc.perform(delete("/api/environnements/{id}", environnement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Environnement> environnementList = environnementRepository.findAll();
        assertThat(environnementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
