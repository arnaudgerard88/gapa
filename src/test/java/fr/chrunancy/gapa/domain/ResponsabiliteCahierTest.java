package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class ResponsabiliteCahierTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResponsabiliteCahier.class);
        ResponsabiliteCahier responsabiliteCahier1 = new ResponsabiliteCahier();
        responsabiliteCahier1.setId(1L);
        ResponsabiliteCahier responsabiliteCahier2 = new ResponsabiliteCahier();
        responsabiliteCahier2.setId(responsabiliteCahier1.getId());
        assertThat(responsabiliteCahier1).isEqualTo(responsabiliteCahier2);
        responsabiliteCahier2.setId(2L);
        assertThat(responsabiliteCahier1).isNotEqualTo(responsabiliteCahier2);
        responsabiliteCahier1.setId(null);
        assertThat(responsabiliteCahier1).isNotEqualTo(responsabiliteCahier2);
    }
}
