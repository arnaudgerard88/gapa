package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class DeploiementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Deploiement.class);
        Deploiement deploiement1 = new Deploiement();
        deploiement1.setId(1L);
        Deploiement deploiement2 = new Deploiement();
        deploiement2.setId(deploiement1.getId());
        assertThat(deploiement1).isEqualTo(deploiement2);
        deploiement2.setId(2L);
        assertThat(deploiement1).isNotEqualTo(deploiement2);
        deploiement1.setId(null);
        assertThat(deploiement1).isNotEqualTo(deploiement2);
    }
}
