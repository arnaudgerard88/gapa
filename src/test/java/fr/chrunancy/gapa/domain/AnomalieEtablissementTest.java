package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class AnomalieEtablissementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnomalieEtablissement.class);
        AnomalieEtablissement anomalieEtablissement1 = new AnomalieEtablissement();
        anomalieEtablissement1.setId(1L);
        AnomalieEtablissement anomalieEtablissement2 = new AnomalieEtablissement();
        anomalieEtablissement2.setId(anomalieEtablissement1.getId());
        assertThat(anomalieEtablissement1).isEqualTo(anomalieEtablissement2);
        anomalieEtablissement2.setId(2L);
        assertThat(anomalieEtablissement1).isNotEqualTo(anomalieEtablissement2);
        anomalieEtablissement1.setId(null);
        assertThat(anomalieEtablissement1).isNotEqualTo(anomalieEtablissement2);
    }
}
