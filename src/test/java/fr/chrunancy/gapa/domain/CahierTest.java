package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class CahierTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cahier.class);
        Cahier cahier1 = new Cahier();
        cahier1.setId(1L);
        Cahier cahier2 = new Cahier();
        cahier2.setId(cahier1.getId());
        assertThat(cahier1).isEqualTo(cahier2);
        cahier2.setId(2L);
        assertThat(cahier1).isNotEqualTo(cahier2);
        cahier1.setId(null);
        assertThat(cahier1).isNotEqualTo(cahier2);
    }
}
