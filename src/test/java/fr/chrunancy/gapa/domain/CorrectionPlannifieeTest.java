package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class CorrectionPlannifieeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CorrectionPlannifiee.class);
        CorrectionPlannifiee correctionPlannifiee1 = new CorrectionPlannifiee();
        correctionPlannifiee1.setId(1L);
        CorrectionPlannifiee correctionPlannifiee2 = new CorrectionPlannifiee();
        correctionPlannifiee2.setId(correctionPlannifiee1.getId());
        assertThat(correctionPlannifiee1).isEqualTo(correctionPlannifiee2);
        correctionPlannifiee2.setId(2L);
        assertThat(correctionPlannifiee1).isNotEqualTo(correctionPlannifiee2);
        correctionPlannifiee1.setId(null);
        assertThat(correctionPlannifiee1).isNotEqualTo(correctionPlannifiee2);
    }
}
