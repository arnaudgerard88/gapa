package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class TypeEnvTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeEnv.class);
        TypeEnv typeEnv1 = new TypeEnv();
        typeEnv1.setId(1L);
        TypeEnv typeEnv2 = new TypeEnv();
        typeEnv2.setId(typeEnv1.getId());
        assertThat(typeEnv1).isEqualTo(typeEnv2);
        typeEnv2.setId(2L);
        assertThat(typeEnv1).isNotEqualTo(typeEnv2);
        typeEnv1.setId(null);
        assertThat(typeEnv1).isNotEqualTo(typeEnv2);
    }
}
