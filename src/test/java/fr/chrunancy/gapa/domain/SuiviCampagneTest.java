package fr.chrunancy.gapa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.chrunancy.gapa.web.rest.TestUtil;

public class SuiviCampagneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuiviCampagne.class);
        SuiviCampagne suiviCampagne1 = new SuiviCampagne();
        suiviCampagne1.setId(1L);
        SuiviCampagne suiviCampagne2 = new SuiviCampagne();
        suiviCampagne2.setId(suiviCampagne1.getId());
        assertThat(suiviCampagne1).isEqualTo(suiviCampagne2);
        suiviCampagne2.setId(2L);
        assertThat(suiviCampagne1).isNotEqualTo(suiviCampagne2);
        suiviCampagne1.setId(null);
        assertThat(suiviCampagne1).isNotEqualTo(suiviCampagne2);
    }
}
