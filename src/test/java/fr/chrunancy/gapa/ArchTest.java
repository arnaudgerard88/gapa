package fr.chrunancy.gapa;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("fr.chrunancy.gapa");

        noClasses()
            .that()
                .resideInAnyPackage("fr.chrunancy.gapa.service..")
            .or()
                .resideInAnyPackage("fr.chrunancy.gapa.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..fr.chrunancy.gapa.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
