/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ApplicationDetailComponent from '@/entities/application/application-details.vue';
import ApplicationClass from '@/entities/application/application-details.component';
import ApplicationService from '@/entities/application/application.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Application Management Detail Component', () => {
    let wrapper: Wrapper<ApplicationClass>;
    let comp: ApplicationClass;
    let applicationServiceStub: SinonStubbedInstance<ApplicationService>;

    beforeEach(() => {
      applicationServiceStub = sinon.createStubInstance<ApplicationService>(ApplicationService);

      wrapper = shallowMount<ApplicationClass>(ApplicationDetailComponent, {
        store,
        localVue,
        provide: { applicationService: () => applicationServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundApplication = { id: 123 };
        applicationServiceStub.find.resolves(foundApplication);

        // WHEN
        comp.retrieveApplication(123);
        await comp.$nextTick();

        // THEN
        expect(comp.application).toBe(foundApplication);
      });
    });
  });
});
