/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ChapitreDetailComponent from '@/entities/chapitre/chapitre-details.vue';
import ChapitreClass from '@/entities/chapitre/chapitre-details.component';
import ChapitreService from '@/entities/chapitre/chapitre.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Chapitre Management Detail Component', () => {
    let wrapper: Wrapper<ChapitreClass>;
    let comp: ChapitreClass;
    let chapitreServiceStub: SinonStubbedInstance<ChapitreService>;

    beforeEach(() => {
      chapitreServiceStub = sinon.createStubInstance<ChapitreService>(ChapitreService);

      wrapper = shallowMount<ChapitreClass>(ChapitreDetailComponent, {
        store,
        localVue,
        provide: { chapitreService: () => chapitreServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundChapitre = { id: 123 };
        chapitreServiceStub.find.resolves(foundChapitre);

        // WHEN
        comp.retrieveChapitre(123);
        await comp.$nextTick();

        // THEN
        expect(comp.chapitre).toBe(foundChapitre);
      });
    });
  });
});
