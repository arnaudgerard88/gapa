/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import ChapitreUpdateComponent from '@/entities/chapitre/chapitre-update.vue';
import ChapitreClass from '@/entities/chapitre/chapitre-update.component';
import ChapitreService from '@/entities/chapitre/chapitre.service';

import FonctionnaliteService from '@/entities/fonctionnalite/fonctionnalite.service';

import CahierService from '@/entities/cahier/cahier.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Chapitre Management Update Component', () => {
    let wrapper: Wrapper<ChapitreClass>;
    let comp: ChapitreClass;
    let chapitreServiceStub: SinonStubbedInstance<ChapitreService>;

    beforeEach(() => {
      chapitreServiceStub = sinon.createStubInstance<ChapitreService>(ChapitreService);

      wrapper = shallowMount<ChapitreClass>(ChapitreUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          chapitreService: () => chapitreServiceStub,

          fonctionnaliteService: () => new FonctionnaliteService(),

          cahierService: () => new CahierService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.chapitre = entity;
        chapitreServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(chapitreServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.chapitre = entity;
        chapitreServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(chapitreServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
