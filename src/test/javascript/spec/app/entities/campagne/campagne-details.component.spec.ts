/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CampagneDetailComponent from '@/entities/campagne/campagne-details.vue';
import CampagneClass from '@/entities/campagne/campagne-details.component';
import CampagneService from '@/entities/campagne/campagne.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Campagne Management Detail Component', () => {
    let wrapper: Wrapper<CampagneClass>;
    let comp: CampagneClass;
    let campagneServiceStub: SinonStubbedInstance<CampagneService>;

    beforeEach(() => {
      campagneServiceStub = sinon.createStubInstance<CampagneService>(CampagneService);

      wrapper = shallowMount<CampagneClass>(CampagneDetailComponent, {
        store,
        localVue,
        provide: { campagneService: () => campagneServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCampagne = { id: 123 };
        campagneServiceStub.find.resolves(foundCampagne);

        // WHEN
        comp.retrieveCampagne(123);
        await comp.$nextTick();

        // THEN
        expect(comp.campagne).toBe(foundCampagne);
      });
    });
  });
});
