/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CahierDetailComponent from '@/entities/cahier/cahier-details.vue';
import CahierClass from '@/entities/cahier/cahier-details.component';
import CahierService from '@/entities/cahier/cahier.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Cahier Management Detail Component', () => {
    let wrapper: Wrapper<CahierClass>;
    let comp: CahierClass;
    let cahierServiceStub: SinonStubbedInstance<CahierService>;

    beforeEach(() => {
      cahierServiceStub = sinon.createStubInstance<CahierService>(CahierService);

      wrapper = shallowMount<CahierClass>(CahierDetailComponent, { store, localVue, provide: { cahierService: () => cahierServiceStub } });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCahier = { id: 123 };
        cahierServiceStub.find.resolves(foundCahier);

        // WHEN
        comp.retrieveCahier(123);
        await comp.$nextTick();

        // THEN
        expect(comp.cahier).toBe(foundCahier);
      });
    });
  });
});
