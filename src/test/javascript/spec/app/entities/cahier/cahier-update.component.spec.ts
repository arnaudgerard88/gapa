/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import CahierUpdateComponent from '@/entities/cahier/cahier-update.vue';
import CahierClass from '@/entities/cahier/cahier-update.component';
import CahierService from '@/entities/cahier/cahier.service';

import DomaineService from '@/entities/domaine/domaine.service';

import ChapitreService from '@/entities/chapitre/chapitre.service';

import ResponsabiliteCahierService from '@/entities/responsabilite-cahier/responsabilite-cahier.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Cahier Management Update Component', () => {
    let wrapper: Wrapper<CahierClass>;
    let comp: CahierClass;
    let cahierServiceStub: SinonStubbedInstance<CahierService>;

    beforeEach(() => {
      cahierServiceStub = sinon.createStubInstance<CahierService>(CahierService);

      wrapper = shallowMount<CahierClass>(CahierUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          cahierService: () => cahierServiceStub,

          domaineService: () => new DomaineService(),

          chapitreService: () => new ChapitreService(),

          responsabiliteCahierService: () => new ResponsabiliteCahierService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.cahier = entity;
        cahierServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cahierServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.cahier = entity;
        cahierServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cahierServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
