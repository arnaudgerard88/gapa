/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import TypeEnvUpdateComponent from '@/entities/type-env/type-env-update.vue';
import TypeEnvClass from '@/entities/type-env/type-env-update.component';
import TypeEnvService from '@/entities/type-env/type-env.service';

import EnvironnementService from '@/entities/environnement/environnement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('TypeEnv Management Update Component', () => {
    let wrapper: Wrapper<TypeEnvClass>;
    let comp: TypeEnvClass;
    let typeEnvServiceStub: SinonStubbedInstance<TypeEnvService>;

    beforeEach(() => {
      typeEnvServiceStub = sinon.createStubInstance<TypeEnvService>(TypeEnvService);

      wrapper = shallowMount<TypeEnvClass>(TypeEnvUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          typeEnvService: () => typeEnvServiceStub,

          environnementService: () => new EnvironnementService()
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.typeEnv = entity;
        typeEnvServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(typeEnvServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.typeEnv = entity;
        typeEnvServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(typeEnvServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
