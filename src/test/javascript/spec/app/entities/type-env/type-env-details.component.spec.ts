/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import TypeEnvDetailComponent from '@/entities/type-env/type-env-details.vue';
import TypeEnvClass from '@/entities/type-env/type-env-details.component';
import TypeEnvService from '@/entities/type-env/type-env.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('TypeEnv Management Detail Component', () => {
    let wrapper: Wrapper<TypeEnvClass>;
    let comp: TypeEnvClass;
    let typeEnvServiceStub: SinonStubbedInstance<TypeEnvService>;

    beforeEach(() => {
      typeEnvServiceStub = sinon.createStubInstance<TypeEnvService>(TypeEnvService);

      wrapper = shallowMount<TypeEnvClass>(TypeEnvDetailComponent, {
        store,
        localVue,
        provide: { typeEnvService: () => typeEnvServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundTypeEnv = { id: 123 };
        typeEnvServiceStub.find.resolves(foundTypeEnv);

        // WHEN
        comp.retrieveTypeEnv(123);
        await comp.$nextTick();

        // THEN
        expect(comp.typeEnv).toBe(foundTypeEnv);
      });
    });
  });
});
