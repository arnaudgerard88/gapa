/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import AnomalieUpdateComponent from '@/entities/anomalie/anomalie-update.vue';
import AnomalieClass from '@/entities/anomalie/anomalie-update.component';
import AnomalieService from '@/entities/anomalie/anomalie.service';

import CorrectionPlannifieeService from '@/entities/correction-plannifiee/correction-plannifiee.service';

import AnomalieEtablissementService from '@/entities/anomalie-etablissement/anomalie-etablissement.service';

import UtilisateurService from '@/entities/utilisateur/utilisateur.service';

import VersionService from '@/entities/version/version.service';

import SuiviCampagneService from '@/entities/suivi-campagne/suivi-campagne.service';

import DomaineService from '@/entities/domaine/domaine.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Anomalie Management Update Component', () => {
    let wrapper: Wrapper<AnomalieClass>;
    let comp: AnomalieClass;
    let anomalieServiceStub: SinonStubbedInstance<AnomalieService>;

    beforeEach(() => {
      anomalieServiceStub = sinon.createStubInstance<AnomalieService>(AnomalieService);

      wrapper = shallowMount<AnomalieClass>(AnomalieUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          anomalieService: () => anomalieServiceStub,

          correctionPlannifieeService: () => new CorrectionPlannifieeService(),

          anomalieEtablissementService: () => new AnomalieEtablissementService(),

          utilisateurService: () => new UtilisateurService(),

          versionService: () => new VersionService(),

          suiviCampagneService: () => new SuiviCampagneService(),

          domaineService: () => new DomaineService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.anomalie = entity;
        anomalieServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(anomalieServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.anomalie = entity;
        anomalieServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(anomalieServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
