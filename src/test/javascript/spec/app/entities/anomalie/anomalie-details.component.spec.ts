/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import AnomalieDetailComponent from '@/entities/anomalie/anomalie-details.vue';
import AnomalieClass from '@/entities/anomalie/anomalie-details.component';
import AnomalieService from '@/entities/anomalie/anomalie.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Anomalie Management Detail Component', () => {
    let wrapper: Wrapper<AnomalieClass>;
    let comp: AnomalieClass;
    let anomalieServiceStub: SinonStubbedInstance<AnomalieService>;

    beforeEach(() => {
      anomalieServiceStub = sinon.createStubInstance<AnomalieService>(AnomalieService);

      wrapper = shallowMount<AnomalieClass>(AnomalieDetailComponent, {
        store,
        localVue,
        provide: { anomalieService: () => anomalieServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundAnomalie = { id: 123 };
        anomalieServiceStub.find.resolves(foundAnomalie);

        // WHEN
        comp.retrieveAnomalie(123);
        await comp.$nextTick();

        // THEN
        expect(comp.anomalie).toBe(foundAnomalie);
      });
    });
  });
});
