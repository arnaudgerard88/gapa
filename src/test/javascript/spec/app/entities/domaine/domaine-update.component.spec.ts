/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import DomaineUpdateComponent from '@/entities/domaine/domaine-update.vue';
import DomaineClass from '@/entities/domaine/domaine-update.component';
import DomaineService from '@/entities/domaine/domaine.service';

import AnomalieService from '@/entities/anomalie/anomalie.service';

import CahierService from '@/entities/cahier/cahier.service';

import ApplicationService from '@/entities/application/application.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Domaine Management Update Component', () => {
    let wrapper: Wrapper<DomaineClass>;
    let comp: DomaineClass;
    let domaineServiceStub: SinonStubbedInstance<DomaineService>;

    beforeEach(() => {
      domaineServiceStub = sinon.createStubInstance<DomaineService>(DomaineService);

      wrapper = shallowMount<DomaineClass>(DomaineUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          domaineService: () => domaineServiceStub,

          anomalieService: () => new AnomalieService(),

          cahierService: () => new CahierService(),

          applicationService: () => new ApplicationService()
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.domaine = entity;
        domaineServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(domaineServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.domaine = entity;
        domaineServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(domaineServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
