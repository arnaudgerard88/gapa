/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import VersionDetailComponent from '@/entities/version/version-details.vue';
import VersionClass from '@/entities/version/version-details.component';
import VersionService from '@/entities/version/version.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Version Management Detail Component', () => {
    let wrapper: Wrapper<VersionClass>;
    let comp: VersionClass;
    let versionServiceStub: SinonStubbedInstance<VersionService>;

    beforeEach(() => {
      versionServiceStub = sinon.createStubInstance<VersionService>(VersionService);

      wrapper = shallowMount<VersionClass>(VersionDetailComponent, {
        store,
        localVue,
        provide: { versionService: () => versionServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVersion = { id: 123 };
        versionServiceStub.find.resolves(foundVersion);

        // WHEN
        comp.retrieveVersion(123);
        await comp.$nextTick();

        // THEN
        expect(comp.version).toBe(foundVersion);
      });
    });
  });
});
