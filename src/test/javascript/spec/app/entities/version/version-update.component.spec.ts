/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import VersionUpdateComponent from '@/entities/version/version-update.vue';
import VersionClass from '@/entities/version/version-update.component';
import VersionService from '@/entities/version/version.service';

import AnomalieService from '@/entities/anomalie/anomalie.service';

import DeploiementService from '@/entities/deploiement/deploiement.service';

import CorrectionPlannifieeService from '@/entities/correction-plannifiee/correction-plannifiee.service';

import ApplicationService from '@/entities/application/application.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Version Management Update Component', () => {
    let wrapper: Wrapper<VersionClass>;
    let comp: VersionClass;
    let versionServiceStub: SinonStubbedInstance<VersionService>;

    beforeEach(() => {
      versionServiceStub = sinon.createStubInstance<VersionService>(VersionService);

      wrapper = shallowMount<VersionClass>(VersionUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          versionService: () => versionServiceStub,

          anomalieService: () => new AnomalieService(),

          deploiementService: () => new DeploiementService(),

          correctionPlannifieeService: () => new CorrectionPlannifieeService(),

          applicationService: () => new ApplicationService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.version = entity;
        versionServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(versionServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.version = entity;
        versionServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(versionServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
