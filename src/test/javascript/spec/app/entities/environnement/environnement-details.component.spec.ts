/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import EnvironnementDetailComponent from '@/entities/environnement/environnement-details.vue';
import EnvironnementClass from '@/entities/environnement/environnement-details.component';
import EnvironnementService from '@/entities/environnement/environnement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Environnement Management Detail Component', () => {
    let wrapper: Wrapper<EnvironnementClass>;
    let comp: EnvironnementClass;
    let environnementServiceStub: SinonStubbedInstance<EnvironnementService>;

    beforeEach(() => {
      environnementServiceStub = sinon.createStubInstance<EnvironnementService>(EnvironnementService);

      wrapper = shallowMount<EnvironnementClass>(EnvironnementDetailComponent, {
        store,
        localVue,
        provide: { environnementService: () => environnementServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundEnvironnement = { id: 123 };
        environnementServiceStub.find.resolves(foundEnvironnement);

        // WHEN
        comp.retrieveEnvironnement(123);
        await comp.$nextTick();

        // THEN
        expect(comp.environnement).toBe(foundEnvironnement);
      });
    });
  });
});
