/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import EnvironnementUpdateComponent from '@/entities/environnement/environnement-update.vue';
import EnvironnementClass from '@/entities/environnement/environnement-update.component';
import EnvironnementService from '@/entities/environnement/environnement.service';

import CampagneService from '@/entities/campagne/campagne.service';

import DeploiementService from '@/entities/deploiement/deploiement.service';

import ApplicationService from '@/entities/application/application.service';

import TypeEnvService from '@/entities/type-env/type-env.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Environnement Management Update Component', () => {
    let wrapper: Wrapper<EnvironnementClass>;
    let comp: EnvironnementClass;
    let environnementServiceStub: SinonStubbedInstance<EnvironnementService>;

    beforeEach(() => {
      environnementServiceStub = sinon.createStubInstance<EnvironnementService>(EnvironnementService);

      wrapper = shallowMount<EnvironnementClass>(EnvironnementUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          environnementService: () => environnementServiceStub,

          campagneService: () => new CampagneService(),

          deploiementService: () => new DeploiementService(),

          applicationService: () => new ApplicationService(),

          typeEnvService: () => new TypeEnvService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.environnement = entity;
        environnementServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(environnementServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.environnement = entity;
        environnementServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(environnementServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
