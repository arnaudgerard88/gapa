/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CorrectionPlannifieeDetailComponent from '@/entities/correction-plannifiee/correction-plannifiee-details.vue';
import CorrectionPlannifieeClass from '@/entities/correction-plannifiee/correction-plannifiee-details.component';
import CorrectionPlannifieeService from '@/entities/correction-plannifiee/correction-plannifiee.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('CorrectionPlannifiee Management Detail Component', () => {
    let wrapper: Wrapper<CorrectionPlannifieeClass>;
    let comp: CorrectionPlannifieeClass;
    let correctionPlannifieeServiceStub: SinonStubbedInstance<CorrectionPlannifieeService>;

    beforeEach(() => {
      correctionPlannifieeServiceStub = sinon.createStubInstance<CorrectionPlannifieeService>(CorrectionPlannifieeService);

      wrapper = shallowMount<CorrectionPlannifieeClass>(CorrectionPlannifieeDetailComponent, {
        store,
        localVue,
        provide: { correctionPlannifieeService: () => correctionPlannifieeServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCorrectionPlannifiee = { id: 123 };
        correctionPlannifieeServiceStub.find.resolves(foundCorrectionPlannifiee);

        // WHEN
        comp.retrieveCorrectionPlannifiee(123);
        await comp.$nextTick();

        // THEN
        expect(comp.correctionPlannifiee).toBe(foundCorrectionPlannifiee);
      });
    });
  });
});
