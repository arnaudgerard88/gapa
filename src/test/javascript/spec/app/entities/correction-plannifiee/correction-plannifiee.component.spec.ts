/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import CorrectionPlannifieeComponent from '@/entities/correction-plannifiee/correction-plannifiee.vue';
import CorrectionPlannifieeClass from '@/entities/correction-plannifiee/correction-plannifiee.component';
import CorrectionPlannifieeService from '@/entities/correction-plannifiee/correction-plannifiee.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {}
  }
};

describe('Component Tests', () => {
  describe('CorrectionPlannifiee Management Component', () => {
    let wrapper: Wrapper<CorrectionPlannifieeClass>;
    let comp: CorrectionPlannifieeClass;
    let correctionPlannifieeServiceStub: SinonStubbedInstance<CorrectionPlannifieeService>;

    beforeEach(() => {
      correctionPlannifieeServiceStub = sinon.createStubInstance<CorrectionPlannifieeService>(CorrectionPlannifieeService);
      correctionPlannifieeServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CorrectionPlannifieeClass>(CorrectionPlannifieeComponent, {
        store,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          correctionPlannifieeService: () => correctionPlannifieeServiceStub
        }
      });
      comp = wrapper.vm;
    });

    it('should be a Vue instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('Should call load all on init', async () => {
      // GIVEN
      correctionPlannifieeServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCorrectionPlannifiees();
      await comp.$nextTick();

      // THEN
      expect(correctionPlannifieeServiceStub.retrieve.called).toBeTruthy();
      expect(comp.correctionPlannifiees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      correctionPlannifieeServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(correctionPlannifieeServiceStub.retrieve.called).toBeTruthy();
      expect(comp.correctionPlannifiees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      correctionPlannifieeServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(correctionPlannifieeServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      correctionPlannifieeServiceStub.retrieve.reset();
      correctionPlannifieeServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(correctionPlannifieeServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.correctionPlannifiees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      correctionPlannifieeServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeCorrectionPlannifiee();
      await comp.$nextTick();

      // THEN
      expect(correctionPlannifieeServiceStub.delete.called).toBeTruthy();
      expect(correctionPlannifieeServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
