/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import DeploiementUpdateComponent from '@/entities/deploiement/deploiement-update.vue';
import DeploiementClass from '@/entities/deploiement/deploiement-update.component';
import DeploiementService from '@/entities/deploiement/deploiement.service';

import VersionService from '@/entities/version/version.service';

import EnvironnementService from '@/entities/environnement/environnement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Deploiement Management Update Component', () => {
    let wrapper: Wrapper<DeploiementClass>;
    let comp: DeploiementClass;
    let deploiementServiceStub: SinonStubbedInstance<DeploiementService>;

    beforeEach(() => {
      deploiementServiceStub = sinon.createStubInstance<DeploiementService>(DeploiementService);

      wrapper = shallowMount<DeploiementClass>(DeploiementUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          deploiementService: () => deploiementServiceStub,

          versionService: () => new VersionService(),

          environnementService: () => new EnvironnementService()
        }
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(format(date, DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.deploiement = entity;
        deploiementServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(deploiementServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.deploiement = entity;
        deploiementServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(deploiementServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
