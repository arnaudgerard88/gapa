/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import DeploiementDetailComponent from '@/entities/deploiement/deploiement-details.vue';
import DeploiementClass from '@/entities/deploiement/deploiement-details.component';
import DeploiementService from '@/entities/deploiement/deploiement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Deploiement Management Detail Component', () => {
    let wrapper: Wrapper<DeploiementClass>;
    let comp: DeploiementClass;
    let deploiementServiceStub: SinonStubbedInstance<DeploiementService>;

    beforeEach(() => {
      deploiementServiceStub = sinon.createStubInstance<DeploiementService>(DeploiementService);

      wrapper = shallowMount<DeploiementClass>(DeploiementDetailComponent, {
        store,
        localVue,
        provide: { deploiementService: () => deploiementServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDeploiement = { id: 123 };
        deploiementServiceStub.find.resolves(foundDeploiement);

        // WHEN
        comp.retrieveDeploiement(123);
        await comp.$nextTick();

        // THEN
        expect(comp.deploiement).toBe(foundDeploiement);
      });
    });
  });
});
