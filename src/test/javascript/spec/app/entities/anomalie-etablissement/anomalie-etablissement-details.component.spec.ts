/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import AnomalieEtablissementDetailComponent from '@/entities/anomalie-etablissement/anomalie-etablissement-details.vue';
import AnomalieEtablissementClass from '@/entities/anomalie-etablissement/anomalie-etablissement-details.component';
import AnomalieEtablissementService from '@/entities/anomalie-etablissement/anomalie-etablissement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('AnomalieEtablissement Management Detail Component', () => {
    let wrapper: Wrapper<AnomalieEtablissementClass>;
    let comp: AnomalieEtablissementClass;
    let anomalieEtablissementServiceStub: SinonStubbedInstance<AnomalieEtablissementService>;

    beforeEach(() => {
      anomalieEtablissementServiceStub = sinon.createStubInstance<AnomalieEtablissementService>(AnomalieEtablissementService);

      wrapper = shallowMount<AnomalieEtablissementClass>(AnomalieEtablissementDetailComponent, {
        store,
        localVue,
        provide: { anomalieEtablissementService: () => anomalieEtablissementServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundAnomalieEtablissement = { id: 123 };
        anomalieEtablissementServiceStub.find.resolves(foundAnomalieEtablissement);

        // WHEN
        comp.retrieveAnomalieEtablissement(123);
        await comp.$nextTick();

        // THEN
        expect(comp.anomalieEtablissement).toBe(foundAnomalieEtablissement);
      });
    });
  });
});
