/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import FonctionnaliteDetailComponent from '@/entities/fonctionnalite/fonctionnalite-details.vue';
import FonctionnaliteClass from '@/entities/fonctionnalite/fonctionnalite-details.component';
import FonctionnaliteService from '@/entities/fonctionnalite/fonctionnalite.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Fonctionnalite Management Detail Component', () => {
    let wrapper: Wrapper<FonctionnaliteClass>;
    let comp: FonctionnaliteClass;
    let fonctionnaliteServiceStub: SinonStubbedInstance<FonctionnaliteService>;

    beforeEach(() => {
      fonctionnaliteServiceStub = sinon.createStubInstance<FonctionnaliteService>(FonctionnaliteService);

      wrapper = shallowMount<FonctionnaliteClass>(FonctionnaliteDetailComponent, {
        store,
        localVue,
        provide: { fonctionnaliteService: () => fonctionnaliteServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFonctionnalite = { id: 123 };
        fonctionnaliteServiceStub.find.resolves(foundFonctionnalite);

        // WHEN
        comp.retrieveFonctionnalite(123);
        await comp.$nextTick();

        // THEN
        expect(comp.fonctionnalite).toBe(foundFonctionnalite);
      });
    });
  });
});
