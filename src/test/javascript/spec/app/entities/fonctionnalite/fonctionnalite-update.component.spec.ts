/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import FonctionnaliteUpdateComponent from '@/entities/fonctionnalite/fonctionnalite-update.vue';
import FonctionnaliteClass from '@/entities/fonctionnalite/fonctionnalite-update.component';
import FonctionnaliteService from '@/entities/fonctionnalite/fonctionnalite.service';

import SuiviCampagneService from '@/entities/suivi-campagne/suivi-campagne.service';

import ChapitreService from '@/entities/chapitre/chapitre.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Fonctionnalite Management Update Component', () => {
    let wrapper: Wrapper<FonctionnaliteClass>;
    let comp: FonctionnaliteClass;
    let fonctionnaliteServiceStub: SinonStubbedInstance<FonctionnaliteService>;

    beforeEach(() => {
      fonctionnaliteServiceStub = sinon.createStubInstance<FonctionnaliteService>(FonctionnaliteService);

      wrapper = shallowMount<FonctionnaliteClass>(FonctionnaliteUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          fonctionnaliteService: () => fonctionnaliteServiceStub,

          suiviCampagneService: () => new SuiviCampagneService(),

          chapitreService: () => new ChapitreService()
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.fonctionnalite = entity;
        fonctionnaliteServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fonctionnaliteServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.fonctionnalite = entity;
        fonctionnaliteServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(fonctionnaliteServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
