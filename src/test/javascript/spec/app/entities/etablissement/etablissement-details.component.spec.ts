/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import EtablissementDetailComponent from '@/entities/etablissement/etablissement-details.vue';
import EtablissementClass from '@/entities/etablissement/etablissement-details.component';
import EtablissementService from '@/entities/etablissement/etablissement.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Etablissement Management Detail Component', () => {
    let wrapper: Wrapper<EtablissementClass>;
    let comp: EtablissementClass;
    let etablissementServiceStub: SinonStubbedInstance<EtablissementService>;

    beforeEach(() => {
      etablissementServiceStub = sinon.createStubInstance<EtablissementService>(EtablissementService);

      wrapper = shallowMount<EtablissementClass>(EtablissementDetailComponent, {
        store,
        localVue,
        provide: { etablissementService: () => etablissementServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundEtablissement = { id: 123 };
        etablissementServiceStub.find.resolves(foundEtablissement);

        // WHEN
        comp.retrieveEtablissement(123);
        await comp.$nextTick();

        // THEN
        expect(comp.etablissement).toBe(foundEtablissement);
      });
    });
  });
});
