/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ResponsabiliteCahierDetailComponent from '@/entities/responsabilite-cahier/responsabilite-cahier-details.vue';
import ResponsabiliteCahierClass from '@/entities/responsabilite-cahier/responsabilite-cahier-details.component';
import ResponsabiliteCahierService from '@/entities/responsabilite-cahier/responsabilite-cahier.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ResponsabiliteCahier Management Detail Component', () => {
    let wrapper: Wrapper<ResponsabiliteCahierClass>;
    let comp: ResponsabiliteCahierClass;
    let responsabiliteCahierServiceStub: SinonStubbedInstance<ResponsabiliteCahierService>;

    beforeEach(() => {
      responsabiliteCahierServiceStub = sinon.createStubInstance<ResponsabiliteCahierService>(ResponsabiliteCahierService);

      wrapper = shallowMount<ResponsabiliteCahierClass>(ResponsabiliteCahierDetailComponent, {
        store,
        localVue,
        provide: { responsabiliteCahierService: () => responsabiliteCahierServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundResponsabiliteCahier = { id: 123 };
        responsabiliteCahierServiceStub.find.resolves(foundResponsabiliteCahier);

        // WHEN
        comp.retrieveResponsabiliteCahier(123);
        await comp.$nextTick();

        // THEN
        expect(comp.responsabiliteCahier).toBe(foundResponsabiliteCahier);
      });
    });
  });
});
