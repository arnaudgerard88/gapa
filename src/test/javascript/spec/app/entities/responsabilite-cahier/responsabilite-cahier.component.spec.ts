/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import ResponsabiliteCahierComponent from '@/entities/responsabilite-cahier/responsabilite-cahier.vue';
import ResponsabiliteCahierClass from '@/entities/responsabilite-cahier/responsabilite-cahier.component';
import ResponsabiliteCahierService from '@/entities/responsabilite-cahier/responsabilite-cahier.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {}
  }
};

describe('Component Tests', () => {
  describe('ResponsabiliteCahier Management Component', () => {
    let wrapper: Wrapper<ResponsabiliteCahierClass>;
    let comp: ResponsabiliteCahierClass;
    let responsabiliteCahierServiceStub: SinonStubbedInstance<ResponsabiliteCahierService>;

    beforeEach(() => {
      responsabiliteCahierServiceStub = sinon.createStubInstance<ResponsabiliteCahierService>(ResponsabiliteCahierService);
      responsabiliteCahierServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ResponsabiliteCahierClass>(ResponsabiliteCahierComponent, {
        store,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          responsabiliteCahierService: () => responsabiliteCahierServiceStub
        }
      });
      comp = wrapper.vm;
    });

    it('should be a Vue instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('Should call load all on init', async () => {
      // GIVEN
      responsabiliteCahierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllResponsabiliteCahiers();
      await comp.$nextTick();

      // THEN
      expect(responsabiliteCahierServiceStub.retrieve.called).toBeTruthy();
      expect(comp.responsabiliteCahiers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      responsabiliteCahierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(responsabiliteCahierServiceStub.retrieve.called).toBeTruthy();
      expect(comp.responsabiliteCahiers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      responsabiliteCahierServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(responsabiliteCahierServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      responsabiliteCahierServiceStub.retrieve.reset();
      responsabiliteCahierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(responsabiliteCahierServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.responsabiliteCahiers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      responsabiliteCahierServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeResponsabiliteCahier();
      await comp.$nextTick();

      // THEN
      expect(responsabiliteCahierServiceStub.delete.called).toBeTruthy();
      expect(responsabiliteCahierServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
