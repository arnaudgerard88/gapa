/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import SuiviCampagneComponent from '@/entities/suivi-campagne/suivi-campagne.vue';
import SuiviCampagneClass from '@/entities/suivi-campagne/suivi-campagne.component';
import SuiviCampagneService from '@/entities/suivi-campagne/suivi-campagne.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {}
  }
};

describe('Component Tests', () => {
  describe('SuiviCampagne Management Component', () => {
    let wrapper: Wrapper<SuiviCampagneClass>;
    let comp: SuiviCampagneClass;
    let suiviCampagneServiceStub: SinonStubbedInstance<SuiviCampagneService>;

    beforeEach(() => {
      suiviCampagneServiceStub = sinon.createStubInstance<SuiviCampagneService>(SuiviCampagneService);
      suiviCampagneServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<SuiviCampagneClass>(SuiviCampagneComponent, {
        store,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          suiviCampagneService: () => suiviCampagneServiceStub
        }
      });
      comp = wrapper.vm;
    });

    it('should be a Vue instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('Should call load all on init', async () => {
      // GIVEN
      suiviCampagneServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllSuiviCampagnes();
      await comp.$nextTick();

      // THEN
      expect(suiviCampagneServiceStub.retrieve.called).toBeTruthy();
      expect(comp.suiviCampagnes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      suiviCampagneServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(suiviCampagneServiceStub.retrieve.called).toBeTruthy();
      expect(comp.suiviCampagnes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      suiviCampagneServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(suiviCampagneServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      suiviCampagneServiceStub.retrieve.reset();
      suiviCampagneServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(suiviCampagneServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.suiviCampagnes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      suiviCampagneServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeSuiviCampagne();
      await comp.$nextTick();

      // THEN
      expect(suiviCampagneServiceStub.delete.called).toBeTruthy();
      expect(suiviCampagneServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
