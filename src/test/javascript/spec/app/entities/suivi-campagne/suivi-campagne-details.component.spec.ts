/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import SuiviCampagneDetailComponent from '@/entities/suivi-campagne/suivi-campagne-details.vue';
import SuiviCampagneClass from '@/entities/suivi-campagne/suivi-campagne-details.component';
import SuiviCampagneService from '@/entities/suivi-campagne/suivi-campagne.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('SuiviCampagne Management Detail Component', () => {
    let wrapper: Wrapper<SuiviCampagneClass>;
    let comp: SuiviCampagneClass;
    let suiviCampagneServiceStub: SinonStubbedInstance<SuiviCampagneService>;

    beforeEach(() => {
      suiviCampagneServiceStub = sinon.createStubInstance<SuiviCampagneService>(SuiviCampagneService);

      wrapper = shallowMount<SuiviCampagneClass>(SuiviCampagneDetailComponent, {
        store,
        localVue,
        provide: { suiviCampagneService: () => suiviCampagneServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundSuiviCampagne = { id: 123 };
        suiviCampagneServiceStub.find.resolves(foundSuiviCampagne);

        // WHEN
        comp.retrieveSuiviCampagne(123);
        await comp.$nextTick();

        // THEN
        expect(comp.suiviCampagne).toBe(foundSuiviCampagne);
      });
    });
  });
});
