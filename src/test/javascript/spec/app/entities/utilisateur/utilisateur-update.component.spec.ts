/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import UtilisateurUpdateComponent from '@/entities/utilisateur/utilisateur-update.vue';
import UtilisateurClass from '@/entities/utilisateur/utilisateur-update.component';
import UtilisateurService from '@/entities/utilisateur/utilisateur.service';

import AnomalieService from '@/entities/anomalie/anomalie.service';

import ResponsabiliteCahierService from '@/entities/responsabilite-cahier/responsabilite-cahier.service';

import ApplicationService from '@/entities/application/application.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Utilisateur Management Update Component', () => {
    let wrapper: Wrapper<UtilisateurClass>;
    let comp: UtilisateurClass;
    let utilisateurServiceStub: SinonStubbedInstance<UtilisateurService>;

    beforeEach(() => {
      utilisateurServiceStub = sinon.createStubInstance<UtilisateurService>(UtilisateurService);

      wrapper = shallowMount<UtilisateurClass>(UtilisateurUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          utilisateurService: () => utilisateurServiceStub,

          anomalieService: () => new AnomalieService(),

          responsabiliteCahierService: () => new ResponsabiliteCahierService(),

          applicationService: () => new ApplicationService()
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.utilisateur = entity;
        utilisateurServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(utilisateurServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.utilisateur = entity;
        utilisateurServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(utilisateurServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
