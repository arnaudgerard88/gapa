/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import UtilisateurDetailComponent from '@/entities/utilisateur/utilisateur-details.vue';
import UtilisateurClass from '@/entities/utilisateur/utilisateur-details.component';
import UtilisateurService from '@/entities/utilisateur/utilisateur.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Utilisateur Management Detail Component', () => {
    let wrapper: Wrapper<UtilisateurClass>;
    let comp: UtilisateurClass;
    let utilisateurServiceStub: SinonStubbedInstance<UtilisateurService>;

    beforeEach(() => {
      utilisateurServiceStub = sinon.createStubInstance<UtilisateurService>(UtilisateurService);

      wrapper = shallowMount<UtilisateurClass>(UtilisateurDetailComponent, {
        store,
        localVue,
        provide: { utilisateurService: () => utilisateurServiceStub }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundUtilisateur = { id: 123 };
        utilisateurServiceStub.find.resolves(foundUtilisateur);

        // WHEN
        comp.retrieveUtilisateur(123);
        await comp.$nextTick();

        // THEN
        expect(comp.utilisateur).toBe(foundUtilisateur);
      });
    });
  });
});
